/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.math;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 *
 * @author acost
 */
public class Graph {
    
    Set<String> nodes;
    HashMap<String, Set<String>> edges;
    
    public Graph() {
        this.nodes = new HashSet<>();
        this.edges = new HashMap<>();
    }
    
    public boolean addNode(String nodeName) {
        if (this.nodes.contains(nodeName)) return false;
        
        this.nodes.add(nodeName);
        this.edges.put(nodeName, new HashSet<>());
        return true;
    }
    
    public boolean addDirectedEdge(String firstNode, String secondNode) {
        if (!this.nodes.contains(firstNode)) { addNode(firstNode); } 
        if (!this.nodes.contains(secondNode)) { addNode(secondNode); }
        
        if (edges.get(firstNode).contains(secondNode)) return false;
        
        edges.get(firstNode).add(secondNode);
        return true;
    }
    
    public boolean removeDirectedEdge(String firstNode, String secondNode) {
        if (!this.nodes.contains(firstNode) || !this.nodes.contains(secondNode)) return false;
        
        if (!edges.get(firstNode).contains(secondNode)) return false;
        
        edges.get(firstNode).remove(secondNode);
        return true;
    }
    
    
    private boolean recursiveCycle(String node, Set<String> stack, Set<String> visited) {
        
        if (!visited.contains(node)) {
            visited.add(node);
            stack.add(node);
            
            for(String adjacent : edges.get(node)) {
                if (!visited.contains(adjacent) && recursiveCycle(adjacent, stack, visited)) {
                    return true;
                } else if (stack.contains(adjacent)) {
                    return true;
                }
            }
        }
        
        stack.remove(node);
        return false;
    }
    
    public boolean hasCycle() {
    
        Set<String> stack = new HashSet<>();
        Set<String> visited = new HashSet<>();
        
        for(String node : nodes) {
            if (recursiveCycle(node, stack, visited)) {
                return true;
            }
        }
        
        return false;
    }
    
    
    private List<CycleNode> getGraphWithoutCycles() {
                
        
        List<String> newToVisit = new ArrayList<>();
        List<String> toVisit = new ArrayList<>();
        List<String> visited = new ArrayList<>();
        List<String> visit = new ArrayList<>();
        
        Map<String, String> parents = new HashMap<>();
        Map<String, CycleNode> cycleNodes = new HashMap<>();
        
        for (String str : nodes) {
            CycleNode cn = CycleNode.fromSingle(str);            
            toVisit.add(cn.name);
            cycleNodes.put(cn.name, cn);
        }
        
        for(String from : edges.keySet()) {
            for (String to : edges.get(from)) {
                cycleNodes.get(from).neighbours.add(to);
            }
        }
        
        AtomicInteger cycleNumber = new AtomicInteger(0);
        int i = 0;
        for (; i < toVisit.size(); i++) {
            CycleNode node = cycleNodes.get(toVisit.get(i));
            parents.put(node.name, null);
            dfsCycle(node, cycleNumber, parents, toVisit, visit, visited, cycleNodes, newToVisit);
            if (!newToVisit.isEmpty()) {
                toVisit.addAll(newToVisit);
                newToVisit.clear();                
                i = 0;
            }
        }
        
        
        return new ArrayList<>(cycleNodes.values());
        
    }
    
    public List<Set<String>> SCO() {
        
        List<CycleNode> acyclic = getGraphWithoutCycles();        
        Map<String, Integer> levels = new HashMap<>();        
        Map<String, CycleNode> nodesByName = new HashMap<>();
        Set<String> visited = new HashSet<>();
                
        
        for (CycleNode c : acyclic) { levels.put(c.name, 0); nodesByName.put(c.name, c); }
        
        Stack<String> topoSort = getTopoSort(nodesByName);
        while(!topoSort.isEmpty()) {
            CycleNode c = nodesByName.get(topoSort.pop());
            setLevel(c, levels, nodesByName);            
        }
        
        
        List<String> rankedNames = new ArrayList<>(levels.keySet());
        rankedNames.sort(new Comparator<String>(){
            @Override
            public int compare(String o1, String o2) {
                return levels.get(o1).compareTo(levels.get(o2));
            }
            
        });
        
        List<Set<String>> res = new ArrayList<>();
        int lastLevel = -1;        
        for (String str : rankedNames) {
            if (levels.get(str) != lastLevel) {
                res.add(new HashSet<>());
                lastLevel++;
            }            
            CycleNode c = nodesByName.get(str);
            if (c.cycleOriginalNodes.isEmpty()) { res.get(lastLevel).add(c.name); }
            else { res.get(lastLevel).addAll(c.cycleOriginalNodes); }
              
        }
                
        
        return res;
    }
    
    private void setLevel(CycleNode source, Map<String, Integer> levels, Map<String, CycleNode> nodesByName) {
        
        int nextLevel = levels.get(source.name) + 1;
        
        for (String neighbour : source.neighbours) {
            CycleNode cn = nodesByName.get(neighbour);
            int neighbourLevel = levels.get(cn.name);
            
            if (nextLevel > neighbourLevel) { levels.put(cn.name, nextLevel); }            
        }
        
        
    }
    
    private Stack<String> getTopoSort(Map<String, CycleNode> nodesByName) {

        Set<String> visited = new HashSet<>();
        Stack<String> stack = new Stack<>();                
        
        for (CycleNode cn : nodesByName.values()) {
            if (!visited.contains(cn.name)) {
                topoSortUtil(cn, nodesByName, visited, stack);
            }
        }
        
        return stack;
    }
    
    private void topoSortUtil(CycleNode source, Map<String, CycleNode> nodesByName, Set<String> visited,  Stack<String> stack) {
        
        visited.add(source.name);
        for (String neighbour : source.neighbours) {        
            CycleNode cn = nodesByName.get(neighbour);
            if (!visited.contains(cn.name)) {
                topoSortUtil(cn, nodesByName, visited, stack);
            }                    
        }
        stack.push(source.name);
        
    }
    
    private void dfsCycle(CycleNode current, AtomicInteger cycleNumber, Map<String, String> parents, List<String> toVisit, List<String>  visit, List<String> visited, Map<String, CycleNode> nodes, List<String>  newToVisit) {
        
        if (visited.contains(current.name)) { return; }
        
        
        moveFrom(current.name, toVisit, visit);
        
        
        int i = 0;
        for (; i < current.neighbours.size(); i++) {
            CycleNode neighbour = nodes.get(current.neighbours.get(i));            
            if (neighbour == null) continue;
            
            if (visited.contains(neighbour.name)) continue;
            if (!visit.contains(neighbour.name)) {
                parents.put(neighbour.name, current.name);
                dfsCycle(neighbour, cycleNumber, parents, toVisit, visit, visited, nodes, newToVisit);                                
                if (!nodes.containsKey(current.name)) { return; }                
                if (current.isRevisitNeighbours()) {
                    i = -1;                    
                }
            } else {
                
                List<CycleNode> cycleList = new ArrayList<>();
                CycleNode node = current;
                String cycleParent = parents.get(neighbour.name);
                while (node != null) {
                    cycleList.add(node);
                    if (node.name.equals(neighbour.name)) { break; }
                    node = nodes.get(parents.get(node.name));                    
                }                    
                                
                cycleNumber.set(cycleNumber.get() + 1);
                CycleNode cycleNode = CycleNode.fromGroup("Cycle " + cycleNumber.get(), cycleList);
                for(CycleNode cn : cycleList) {
                    nodes.remove(cn.name);
                    parents.remove(cn.name);
                    visit.remove(cn.name);
                    
                    for (CycleNode look : nodes.values()) {
                        look.renameNeighbour(cn.name, cycleNode.name);
                    }
                }
                
                
                nodes.put(cycleNode.name, cycleNode);
                parents.put(cycleNode.name, cycleParent);
                if (cycleParent == null) {                    
                    newToVisit.add(cycleNode.name);
                } else {
                    nodes.get(cycleParent).addSafe(cycleNode.name);
                    
                }                   
                
                if (!nodes.containsKey(current.name)) return;
            }
            
        }
        
        if (!nodes.containsKey(current.name)) return;
        moveFrom(current.name, visit, visited);
        
    }
    
    private void moveFrom(String name, List<String> from, List<String>  to) {
        from.remove(name);
        to.add(name);
    }
    
    private static class CycleNode {
        
        private String name;
        private Set<String> cycleOriginalNodes; 
        private List<String> neighbours;
        private boolean revisitNeighbours;
        
        private CycleNode() {}
        
        public static CycleNode fromSingle(String str) {            
            CycleNode cn = new CycleNode();
            cn.name = str;
            cn.cycleOriginalNodes = new HashSet<>();      
            cn.neighbours = new ArrayList<>();
            cn.revisitNeighbours = false;
            return cn;
        }
        
        public static CycleNode fromGroup(String str, List<CycleNode> nodes) {
            CycleNode cn = new CycleNode();
            cn.name = str;
            cn.cycleOriginalNodes = new HashSet<>();
            cn.neighbours = new ArrayList<>();
            for (CycleNode n : nodes) {
                if (n.cycleOriginalNodes.isEmpty()) { cn.cycleOriginalNodes.add(n.name); }
                else { cn.cycleOriginalNodes.addAll(n.cycleOriginalNodes); }
                
                cn.neighbours.addAll(n.neighbours);
            }
            cn.neighbours.remove(cn.name);
            cn.neighbours.removeAll(cn.cycleOriginalNodes);
            cn.neighbours.removeAll(nodes.stream().map(c -> c.name).collect(Collectors.toList()));
            cn.revisitNeighbours = false;
            
            cn.neighbours = new ArrayList<>(new HashSet<>(cn.neighbours));
            
            return cn;
        }
        
        public void renameNeighbour(String oldName, String newName) {
            if (this.neighbours.contains(oldName)) {
                this.neighbours.remove(oldName);
                if (!this.neighbours.contains(newName)) {
                    this.neighbours.add(newName);
                }                
                this.revisitNeighbours = true;
            }
        }
        
        public void addSafe(String name) {
            if (!this.neighbours.contains(name)) {
                this.neighbours.add(name);
            }
        }
        
        public boolean isRevisitNeighbours() {
            boolean old = this.revisitNeighbours;
            this.revisitNeighbours = false;
            return old;
        }
        
        @Override
        public String toString() {
            
            String ret;
            if (cycleOriginalNodes.isEmpty()) {
                ret = String.format("(%s -> %s)", this.name, this.neighbours);
            } else {
                ret = String.format("(%s%s -> %s)", this.name, this.cycleOriginalNodes, this.neighbours);
            }
            return ret;
            
        }
    }
    
}
