/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.math;

import java.util.ArrayList;

/**
 *
 * @author uriel
 */
public class Matrix<T> {
    
   private ArrayList<ArrayList<Object>> matrix;
   private int size;
   
   
   public Matrix(int size) {
       this.size = size;
       
       matrix = new ArrayList<>();
       for (int i = 0; i < size; i++) {
           ArrayList<Object> row = new ArrayList<>(size);           
           for (int j = 0; j < size; j++) {
               row.add(new Object());
           }
           matrix.add(row);
       }
   }
   
   public void setValue(int row, int col, T element) {
       
       if (row < 0 || row >= size || col < 0 || col >= size) return;
       
       matrix.get(row).set(col, element);       
   }
   
   public T getValue(int row, int col) {
       if (row < 0 || row >= size || col < 0 || col >= size) return null;
       
       return (T)matrix.get(row).get(col);
   }

    @Override
    public String toString() {
        return "Matrix{" + "matrix=" + matrix + ", size=" + size + '}';
    }
   
   
   
}


