/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author acost
 */
public class ImageUtils {
    
    public static final String RESOURCE_PATH = "/resources/";
    
    public static ImageIcon getImageIcon(String path) {
        
        String npath = RESOURCE_PATH + path;
        
        URL imgURL = ImageUtils.class.getResource(npath);
        if (imgURL != null) {            
            return new ImageIcon(imgURL);
        } else {            
            throw new RuntimeException("No se ha podido encontrar: " + npath);
        }
    }
    
    
    public static ImageIcon getScaledImageIcon(ImageIcon srcImg, int w, int h) {
        BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = resizedImg.createGraphics();

        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(srcImg.getImage(), 0, 0, w, h, null);
        g2.dispose();

        return new ImageIcon(resizedImg);
    }
        
}
