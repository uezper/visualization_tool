/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.filters;

import java.util.ArrayList;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.solution.Group;
import py.una.pol.setviz.utils.solution.Solution;

/**
 *
 * @author acost
 */
public class Filters {
    
    
    
    public static ArrayList<Solution> applyFilters(ArrayList<Solution> set, ArrayList<FilterEntry> filters) {
            
        ArrayList<String> objectiveNames = PlotFrame.settings.getObjectiveNames();
        
        if (filters.isEmpty()) return set;
        
        ArrayList<Solution> newSet = new ArrayList<>();
  
        for(Solution solution : set) {
     
            
            boolean firstShapeFilter = true;
            boolean groupFilter = true;            
            boolean shapeFilter = true;
            boolean objValueFilter = true;          
            boolean objPosFilter = true;
            boolean objPriorityFilter = true;
            
            for(FilterEntry filter : filters) {                
                if (null != filter.getFilterType()) switch (filter.getFilterType()) {
                    case GROUP:                                                
                        groupFilter = filterByGroup(groupFilter, solution, filter);
                        break;
                    case SHAPE:
                        if (firstShapeFilter) {
                            shapeFilter = false;
                            firstShapeFilter = false;
                        }                        
                        shapeFilter |= filterByObjShape(solution, filter);
                        break;
                    case OBJ_POSITION:
                        objPosFilter &= filterByObjPos(solution, filter);
                        break;
                    case OBJ_VALUE:
                        objValueFilter &= filterByObjValue(solution, filter);
                        break;
                    case OBJ_PRIORITY:
                        objPriorityFilter &= filterByObjPriority(solution, filter);
                        break;
                }                                                                                     
                
            }
            
            if (groupFilter && shapeFilter && objPosFilter && objValueFilter && objPriorityFilter) newSet.add(solution);
        }
    
        
        return newSet; 
    }
    
    
    public static boolean filterByObjPriority(Solution solution, FilterEntry filterEntry) {
        
        int obj1 = filterEntry.getObjectiveIndex();
        int obj2 = filterEntry.getObjectiveIndexTwo();
        
        // objective has default order index 
        int currentObj1 = PlotFrame.settings.getCurrentAxesOrder().indexOf(Integer.valueOf(obj1));
        int currentObj2 = PlotFrame.settings.getCurrentAxesOrder().indexOf(Integer.valueOf(obj2));
        
        ArrayList<Integer> shape = solution.getShape();
        
         
        
        return shape.indexOf(currentObj1+1) < shape.indexOf(currentObj2+1);
    }
    
    public static boolean filterByObjPos(Solution solution, FilterEntry filterEntry) {
        
        if (filterEntry.getFilterType() != FilterEntry.FilterType.OBJ_POSITION) return false;
        
        int objective = filterEntry.getObjectiveIndex();
        int value = filterEntry.getValue().intValue();
        switch(filterEntry.getFilterPositionType()) {
            case AT:
                return filterByAt(solution, objective, value);
            case IN_FIRST:
                return filterByFirst(solution, objective, value);
            case IN_LAST:
                return filterByLast(solution, objective, value);
            case NOT_AT:
                return filterByNotAt(solution, objective, value);
        }
        return false;
    }            
    
    
    public static boolean filterByGroup(boolean groupFilter, Solution solution, FilterEntry filterEntry) {
        
        if (filterEntry.getFilterType() != FilterEntry.FilterType.GROUP) return false;
        
        Group group = PlotFrame.groups.get(filterEntry.getGroupId());
        if (group == null) return false;
        
        
        switch(filterEntry.getFilterGroupCondition()) {
            case IN:
                return groupFilter && group.isInGroup(solution);
            case OR_IN:
                return groupFilter || group.isInGroup(solution);
            case NOT_IN:                
                return groupFilter && !group.isInGroup(solution);
        }
        
        return false;
    }
    
    public static boolean filterByObjValue(Solution solution, FilterEntry filterEntry) {
        
        if (filterEntry.getFilterType() != FilterEntry.FilterType.OBJ_VALUE) return false;
        
        int objective = filterEntry.getObjectiveIndex();
        double valueMatch = filterEntry.getValue();
        double value = solution.getValueAt(objective);
        
        switch(filterEntry.getFilterValueCondition()) {
            case EQ:
                return value == valueMatch;
            case NEQ:
                return value != valueMatch;
            case GET:
                return value >= valueMatch;
            case LET:
                return value <= valueMatch;
            case GT:
                return value > valueMatch;
            case LT:
                return value < valueMatch;
        }
        return false;
    }
    
    public static boolean filterByObjShape(Solution solution, FilterEntry filterEntry) {        
        
        if (filterEntry.getFilterType() != FilterEntry.FilterType.SHAPE) return false;
        
        String[] match = filterEntry.getShape().split(",");
        
        
        
        int i = -1;
        for (String value : match) {
            i++;
            if (value.equalsIgnoreCase("*")) continue;
            if (!value.equalsIgnoreCase("" + solution.getShape().get(i))) {
                return false;
            }
        }
        
        return true;
                
    }
    
    public static boolean filterByFirst(Solution solution, int objective, int value) {
        // objective has default order index 
        int currentObj = PlotFrame.settings.getCurrentAxesOrder().indexOf(Integer.valueOf(objective));                
        ArrayList<Integer> shape = solution.getShape();            
        return shape.indexOf(currentObj+1) <= value-1;
    }
    
    
    public static boolean filterByLast(Solution solution, int objective, int value) {
        // objective has default order index 
        int currentObj = PlotFrame.settings.getCurrentAxesOrder().indexOf(Integer.valueOf(objective));                
        ArrayList<Integer> shape = solution.getShape();
        int dimension = PlotFrame.settings.getShownObjectives().size();
        return shape.indexOf(currentObj+1) >= (dimension - value);
    }
    
    public static boolean filterByAt(Solution solution, int objective, int value) {       
        // objective has default order index 
        int currentObj = PlotFrame.settings.getCurrentAxesOrder().indexOf(Integer.valueOf(objective));
        ArrayList<Integer> shape = solution.getShape();
        return shape.indexOf(currentObj+1) == value-1;
    }
    
    public static boolean filterByNotAt(Solution solution, int objective, int value) {
                
        return !filterByAt(solution, objective, value);
    }
}
