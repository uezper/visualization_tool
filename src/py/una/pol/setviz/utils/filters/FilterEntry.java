/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.filters;

import py.una.pol.setviz.gui.PlotFrame;

/**
 *
 * @author acost
 */
public class FilterEntry {        
    
    private FilterType filterType;
    
    private Integer objectiveIndex;    
    private Integer objectiveIndexTwo;
    private FilterEntry.FilterPositionType filterPositionType;            
    private FilterEntry.FilterValueCondition filterValueCondition;            
    private Double value;
    
    private FilterEntry.FilterGroupCondition filterGroupCondition;            
    private Integer groupId;
    
    private String shape;    
    
    public FilterEntry() {
    }

    public Integer getObjectiveIndexTwo() {
        return objectiveIndexTwo;
    }

    public void setObjectiveIndexTwo(Integer objectiveIndexTwo) {
        this.objectiveIndexTwo = objectiveIndexTwo;
    }

    public FilterType getFilterType() {
        return filterType;
    }

    public void setFilterType(FilterType filterType) {
        this.filterType = filterType;
    }

    public Integer getObjectiveIndex() {
        return objectiveIndex;
    }

    public void setObjectiveIndex(Integer objectiveIndex) {
        this.objectiveIndex = objectiveIndex;
    }

    public FilterPositionType getFilterPositionType() {
        return filterPositionType;
    }

    public void setFilterPositionType(FilterPositionType filterPositionType) {
        this.filterPositionType = filterPositionType;
    }

    public FilterValueCondition getFilterValueCondition() {
        return filterValueCondition;
    }

    public void setFilterValueCondition(FilterValueCondition filterValueCondition) {
        this.filterValueCondition = filterValueCondition;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public FilterGroupCondition getFilterGroupCondition() {
        return filterGroupCondition;
    }

    public void setFilterGroupCondition(FilterGroupCondition filterGroupCondition) {
        this.filterGroupCondition = filterGroupCondition;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    
    public String toString() {
        
        String str = "";
        
        switch(filterType) {
            case SHAPE:
                str = "Shape like " + shape;
                break;
            case GROUP:
                str = filterGroupConditionToString(filterGroupCondition)+ " " + PlotFrame.groups.get(groupId).getGroupName();
                break;
            case OBJ_POSITION:             
                str = PlotFrame.settings.getObjectiveNames().get(objectiveIndex) + " " + filterPositionTypeToString(filterPositionType) + " " + value.intValue();
                break;
            case OBJ_VALUE:                                
                str = PlotFrame.settings.getObjectiveNames().get(objectiveIndex) + " " + filterValueConditionToString(filterValueCondition)+ " " + value;
                break;
            case OBJ_PRIORITY:                       
                str = PlotFrame.settings.getObjectiveNames().get(objectiveIndex) + " before " +  PlotFrame.settings.getObjectiveNames().get(objectiveIndexTwo);
                break;
        }
        
        
        return str;
    }
    
    

    public enum FilterType {
        OBJ_VALUE,
        OBJ_POSITION,
        SHAPE,
        GROUP,
        OBJ_PRIORITY
    }
    
    public enum FilterPositionType {
        IN_LAST,
        IN_FIRST,
        AT,
        NOT_AT
    }
    
    public enum FilterGroupCondition {
        IN,
        OR_IN,
        NOT_IN
    }
    
    public enum FilterValueCondition {
        GT,
        LT,
        EQ,
        NEQ,
        GET,
        LET        
    }
    
    
    public static String filterTypeToString(FilterType filterType) {
        String[] val = { "By Value", "By Position", "By Shape", "By Group", "By Priority"};
        return val[filterType.ordinal()];
    }    
        
    
    public static String filterGroupConditionToString(FilterGroupCondition filterGroupCondition) {
        String[] val = { "In", "Or in", "Not in" };
        return val[filterGroupCondition.ordinal()];
    };
    
    public static String filterValueConditionToString(FilterValueCondition filterValueCondition) {
        String[] val = { ">", "<", "=", "!=", ">=", "<="};
        return val[filterValueCondition.ordinal()];
    };
    
    public static String filterPositionTypeToString(FilterPositionType filterPositionType) {
        String[] val = { "In last", "In first", "At", "Not at"};
        return val[filterPositionType.ordinal()];
    };
    
    
    
    
    
    public static FilterType getFilterTypeFromStr(String filterTypeStr) {        
        
        FilterType filterType = null;
        
        for(FilterType type : FilterType.values()) {
            if (filterTypeToString(type).equalsIgnoreCase(filterTypeStr)) {
                filterType = type;
            }
        }
        
        return filterType;           
    }
    
    
    public static FilterGroupCondition getFilterGroupConditionFromStr(String filterGroupConditionStr) {        
        
        FilterGroupCondition filterType = null;
        
        for(FilterGroupCondition type : FilterGroupCondition.values()) {
            if (filterGroupConditionToString(type).equalsIgnoreCase(filterGroupConditionStr)) {
                filterType = type;
            }
        }
        
        return filterType;           
    }
    
    
    public static FilterPositionType getFilterPositionTypeFromStr(String filterPositionTypeStr) {        
        
        FilterPositionType filterType = null;
        
        for(FilterPositionType type : FilterPositionType.values()) {
            if (filterPositionTypeToString(type).equalsIgnoreCase(filterPositionTypeStr)) {
                filterType = type;
            }
        }
        
        return filterType;           
    }
    
    
    public static FilterValueCondition getFilterValueConditionFromStr(String filterValueConditionStr) {        
        
        FilterValueCondition filterType = null;
        
        for(FilterValueCondition type : FilterValueCondition.values()) {
            if (filterValueConditionToString(type).equalsIgnoreCase(filterValueConditionStr)) {
                filterType = type;
            }
        }
        
        return filterType;           
    }
    
}
