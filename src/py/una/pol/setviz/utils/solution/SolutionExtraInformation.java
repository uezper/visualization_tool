/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.solution;

/**
 *
 * @author acost
 */
public enum SolutionExtraInformation {
    SOLUTION_SELECTED,
    SOLUTION_IS_REPRESENTATIVE,
    SOLUTION_CLUSTER_SHAPE
}
