/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.solution;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;

/**
 *
 * @author acost
 */
public class SolutionSet {
    ArrayList<Solution> solutions;
    HashMap<Integer, Solution> solutionByIds;
    
    ArrayList<String> names;
    int dimensions;
    
    public SolutionSet(int dimensions, ArrayList<String> names) {
        /* TODO!! Aca se debe controlar que names tenga la misma dimension que order */              
        this.solutions = new ArrayList<>();        
        this.dimensions = dimensions;
        this.names = names;
        this.solutionByIds = new HashMap<>();
    }
    
    
    public SolutionSet(int dimensions) {
        solutions = new ArrayList<>();        
        this.solutionByIds = new HashMap<>();
        this.dimensions = dimensions;
        
        names = new ArrayList<>();
        for(int i = 0; i < dimensions; i++) {
            names.add("f" + (i + 1));
        }
        
    }
    
    
    public void addSolution(Solution solution) {        
        /* TODO!! Aca se debe controlar que todas las soluciones tengan la misma dimension */              
        /* TODO!! Comprobar que no se agreguen soluciones repetidas por id */
        solutions.add(solution);
        solutionByIds.put(solution.getId(), solution);        
    }

    public void setSolutions(ArrayList<Solution> solutions) {
        /* TODO!! Aca se debe controlar que todas las soluciones tengan la misma dimension */
        /* TODO!! Comprobar que no se agreguen soluciones repetidas por id */
        this.solutions = solutions;
        this.solutionByIds = new HashMap<>();
        for (Solution sol : solutions) {
            solutionByIds.put(sol.getId(), sol);        
        }
    }

    public ArrayList<Solution> getSolutions() {
        return solutions;
    }
    
    public Solution getSolutionById(Integer key) {
        return solutionByIds.getOrDefault(key, null);
    }
    
    
    public Solution getSolution(int i) {
        /* TODO!! Controlar restricciones del indice */
        return solutions.get(i);
    }
    
    public int getSize() {
        return solutions.size();
    }
    
    public int getDimensions() {
        return dimensions;
    }

    public ArrayList<String> getNames() {
        return names;
    }

    public Set<Integer> getIds() {
        return solutionByIds.keySet();
    }
    
    public boolean inSet(Solution sol) {
        return solutionByIds.keySet().contains(sol.getId());
    }
    
    
  
    public void recalculateShapes(boolean isMinProblem) {
        for (Solution sol: solutions) {
            sol.getSolutionShape().recalculateShape(isMinProblem);
        }
    }
    
    private boolean needsToBeNormalized() {
        
        boolean normalize = false;
        for (Solution sol: solutions) {
            normalize = normalize || sol.needsToBeNormalized();
            if (normalize) break;
        }
        
        return normalize;
        
    }
    
    public void normalize() {
        
        if (!needsToBeNormalized()) return;
        
        List<Double> max = new ArrayList<>();
        List<Double> min = new ArrayList<>();
        
        boolean firstTime = true;
        for(Solution sol: solutions) {
            
            for(int i = 0; i < sol.getDimension(); i++) {
                if (firstTime) {
                    max.add(sol.getValueAt(i));
                    min.add(sol.getValueAt(i));
                }
                                           
                Double maxValue = Math.max(max.get(i), sol.getValueAt(i));
                Double minValue = Math.min(min.get(i), sol.getValueAt(i));
                
                max.set(i, maxValue);
                min.set(i, minValue);              
            }
            
            firstTime = false;
            
        }
        
        
        for(Solution sol: solutions) {
            
            ArrayList<Double> solValues = sol.getValues();
            
            for(int i = 0; i < solValues.size(); i++) {
                Double newValue = (solValues.get(i) - min.get(i)) / (max.get(i) - min.get(i));
                solValues.set(i, newValue);
            }
            
            
            sol.setValues(solValues);
        }
           
        
        
    }
    
    public void removeNonDominatedSolutions(boolean isMinProblem) {
        
        
        
        solutions.sort((Solution o1, Solution o2) -> {
            
            if (o1.getValueAt(0).doubleValue() == o2.getValueAt(0).doubleValue()) return 0;
            if (isMinProblem) {
                return o1.getValueAt(0) <= o2.getValueAt(0) ? -1 : 1;
            }   else {
                return o1.getValueAt(0) >= o2.getValueAt(0) ? -1 : 1;
            }
        });
        

        
        ArrayList<Integer> nonDominated = new ArrayList<>();
        nonDominated.add(0);
        
        int ind1_index = 1;
        while(ind1_index < solutions.size()) {
            
            Solution sol1 = solutions.get(ind1_index);
            
            int ind2_index = 1;
            boolean add = true;
            boolean cont = true;
            while(cont && ind2_index < nonDominated.size()) {
                Solution sol2 = solutions.get(nonDominated.get(ind2_index));
                
                if (sol1.equals(sol2)) {
                    cont = false;
                    add = false;
                } 
                if (cont) {
                    if (sol1.dominates(sol2, isMinProblem)) {
                        nonDominated.remove(ind2_index);
                        if (nonDominated.isEmpty()) {
                            cont = false;
                            add = true;
                        }
                        
                    } else if (sol2.dominates(sol1, isMinProblem)) {
                        cont = false;
                        add = false;
                    }
                }                       
                
                ind2_index++;
            }
            
            if (add) {
                nonDominated.add(ind1_index);
            }                   
            
            ind1_index++;            
        }
        
        ArrayList<Solution> newSet = new ArrayList<>();
        for(int i = 0; i < nonDominated.size(); i++) {            
            newSet.add(solutions.get(nonDominated.get(i)));
        }
        
        solutions = newSet;
   
        
    }
    
    public ArrayList<Integer> getCorrelationAdjusting(Set<Integer> axes) {
        
        ArrayList<Integer> adjustedAxesOrder = new ArrayList<>();
        
        ArrayList<Tuple> tuples = new ArrayList<>();
        ArrayList<Integer> p = new ArrayList<>();
        ArrayList<Integer> group_index = new ArrayList<>();
        ArrayList<ArrayList<Double>> byObjective = new ArrayList<>();       
        ArrayList<ArrayList<Integer>> groups_order;
        
        for(int i = 0; i < getDimensions(); i++) {            
            if (byObjective.size() < i+1) {
                byObjective.add(getCol(i));
                p.add(0);
                group_index.add(0);
            }
                        
            for(int j = i+1; j < getDimensions(); j++) {
                if (byObjective.size() < j+1) {
                    byObjective.add(getCol(j));
                    p.add(0);
                    group_index.add(0);
                }

                if (axes.contains(i) && axes.contains(j)) {
                    double cor = getCorrel(byObjective, i, j); 
                    tuples.add(new Tuple(i, j, Math.abs(cor)));
                }
                
            }
        }
        
        
        tuples.sort(new Comparator<Tuple>() {
            @Override
            public int compare(Tuple o1, Tuple o2) {
                return o1.cor > o2.cor ? -1 : 1;
            }
        });
        
  
        groups_order = new ArrayList<>();
                
        
        int group_next = 0;
        for(int i = 0; i < tuples.size(); i++) {
            
            Tuple tuple = tuples.get(i);
            
            int a = tuple.obj1;
            int b = tuple.obj2;
            
            int p1 = p.get(a);
            int p2 = p.get(b);
            
            if (p1 == 0 && p2 == 0) { 
                ArrayList<Integer> newGroup = new ArrayList<>();
                newGroup.add(a);
                newGroup.add(b);
                groups_order.add(newGroup);
                
                group_index.set(a, group_next);
                group_index.set(b, group_next);
                
                group_next++;
                
                p.set(a, p1+1);
                p.set(b, p2+1);
            }
            if (p1 == 1 && p2 == 0) { 
                int index_group = group_index.get(a);
                ArrayList<Integer> newGroup = groups_order.get(index_group);
                
                if (newGroup.indexOf(a) == 0) {
                    newGroup.add(0, b);
                } else {
                    newGroup.add(b);
                }                   
                
                groups_order.set(index_group, newGroup);
                group_index.set(b, index_group);
                
                p.set(a, p1+1);
                p.set(b, p2+1);
            }
            if (p1 == 0 && p2 == 1) { 
                int index_group = group_index.get(b);
                ArrayList<Integer> newGroup = groups_order.get(index_group);
                
                if (newGroup.indexOf(b) == 0) {
                    newGroup.add(0, a);
                } else {
                    newGroup.add(a);
                }                   
                
                groups_order.set(index_group, newGroup);
                group_index.set(a, index_group);
                
                p.set(a, p1+1);
                p.set(b, p2+1);
            }
            if (p1 == 1 && p2 == 1 && !Objects.equals(group_index.get(a), group_index.get(b))) { 
                ArrayList<Integer> group_a = groups_order.get(group_index.get(a));
                ArrayList<Integer> group_b = groups_order.get(group_index.get(b));               
                
                
                if (group_a.indexOf(a) == 0) {
                    if (group_b.indexOf(b) == 0) {
                        for(int j = 0; j < group_b.size(); j++) {
                            group_a.add(0, group_b.get(j));
                        }
                    } else {
                        for(int j = group_b.size() - 1; j >= 0; j--) {
                            group_a.add(0, group_b.get(j));
                        }
                    }
                } else {
                    if (group_b.indexOf(b) == 0) {
                        for(int j = 0; j < group_b.size(); j++) {
                            group_a.add(group_b.get(j));
                        }
                    } else {
                        for(int j = group_b.size() - 1; j >= 0; j--) {
                            group_a.add(group_b.get(j));
                        }
                    }
                }
                
                
                groups_order.set(group_index.get(a), null);
                groups_order.set(group_index.get(b), null);
                
                groups_order.add(group_a);
              
                for (int j = 0; j < group_a.size(); j++) {
                    group_index.set(group_a.get(j), group_next);
                }
                
                group_next++;
                
                p.set(a, p1+1);
                p.set(b, p2+1);
                
            }
            
            
        }
       
    
        for (int i = 0; i < groups_order.size(); i++) {
            ArrayList<Integer> group = groups_order.get(i);
            if (group != null) {
                for (Integer obj: group) {
                    adjustedAxesOrder.add(obj);
                }
            }
            
        }        
        
        
        
        
        return adjustedAxesOrder;
        
    }
    
    
    
      private ArrayList<Double> getCol(int i) {
        ArrayList<Double> col = new ArrayList<>();
        for (Solution sol : solutions) {
            col.add(sol.getValueAt(i));
        }
        return col;
    }
    
    private double getCorrel(ArrayList<ArrayList<Double>> byObjective, int i, int j) {                
        
        double[] f1;
        double[] f2;
        
        f1 = byObjective.get(i).stream().mapToDouble(Double::doubleValue).toArray();
        f2 = byObjective.get(j).stream().mapToDouble(Double::doubleValue).toArray();
        
        return new SpearmansCorrelation().correlation(f1, f2);        
    }
    
    
    
    public void printSet() {
        System.out.println("Size: " + this.getSize());
        
        this.solutions.forEach((sol) -> {
            System.out.println(sol);
        });
    }
    
    private static class Tuple {
        public final int obj1;
        public final int obj2;
        public final double cor;
        
        public Tuple(int obj1, int obj2, double cor) {
            this.obj1 = obj1;
            this.obj2 = obj2;
            this.cor = cor;
        }    
        public String toString() {
            return "(" + cor + "," + obj1 + "," + obj2 + ")";
        }
    }
    
}
