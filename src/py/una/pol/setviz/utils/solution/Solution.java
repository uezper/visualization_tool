/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.solution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import py.una.pol.setviz.gui.PlotFrame;

/**
 *
 * @author acost
 */
public class Solution {
 
    private static int num_solutions = 0;
    
    private HashMap<SolutionExtraInformation, Object> extraInformation;
    
    private int id;
    private List<String> realValues;
    private ArrayList<Double> values;    
    private SolutionShape shape;
    private String name;
    
    public Solution(String[] sol) {        
        
        this.name = null;
        this.realValues = new ArrayList<>();
        this.id = num_solutions++;
        values = new ArrayList<>();
        
        
        this.extraInformation = new HashMap<>();
        
        for (String valueStr : sol) {          
            realValues.add(valueStr);
            values.add(Double.parseDouble(valueStr));
        }              
        
        shape = new SolutionShape(this);
        
    }

    public List<String> getRealValues() {
        return realValues;
    }
    
    public String realValuesString() {
        
        ArrayList<Integer> axesOrder = PlotFrame.settings.getCurrentAxesOrder();        
        String ret = "";
        String sep = "";
        for (int i = 0; i < axesOrder.size(); i++) {
            if (i > 0) { sep = ";"; }
            ret += sep + realValues.get(axesOrder.get(i));
        }
        
        return ret;
        
    }
    
    
    public String getName() {
        if (name == null) {             
            name = String.format("S%5d", id).replace(" ", "0"); 
        }
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getId() {
        return this.id;
    }
    
    public Solution(ArrayList<Double> values) {        
        
        this.values = values;
        this.extraInformation = new HashMap<>();
        this.id = num_solutions++;
        
        shape = new SolutionShape(this);
        
    }
    
    public ArrayList<Integer> getShape() {
        return shape.getShape();
    }
    
    public SolutionShape getSolutionShape() {
        return shape;
    }
    
    public ArrayList<Double> getValues() {
        return values;
    }

    public void setValues(ArrayList<Double> values) {        
        this.values = values;
    }

    public Double getValueAt(int i) {
        if (i >= 0 && i < this.getDimension()) {
            return this.values.get(i);
        }
        return null;
    }

    public boolean needsToBeNormalized() {
        boolean normalize = false;
        
        for (int i = 0; i < values.size(); i++) {
            if (values.get(i) < 0 || values.get(i) > 1) {
                normalize = true;
                break;
            }
        }
        
        return normalize;
    }
    
    public int getDimension() {
        return values.size();
    }

    public void putExtraInformation(SolutionExtraInformation key, Object information) {
        this.extraInformation.put(key, information);
    }
    
    public Object getExtraInformation(SolutionExtraInformation key) {
        return this.extraInformation.getOrDefault(key, null);
    }
    
    public boolean dominates(Solution s2, boolean isMinProblem) {
        
        if (this.getDimension() != s2.getDimension()) return false;
        
        int all = 0;
        int any = 0;
        
        
        for(int i = 0; i < this.getDimension(); i++) {
            Double val_1 = this.values.get(i);
            Double val_2 = s2.getValueAt(i);
            if (isMinProblem) {
                if (val_1 <= val_2) all++;
                if (val_1 < val_2) any++;
            } else {
                if (val_1 >= val_2) all++;
                if (val_1 > val_2) any++;
            }

        }
        if (all == this.getDimension() && any > 0) return true;            
        
        return false;
    }
    
    public boolean equals(Solution s2) {
        int all = 0;
        
        for(int i = 0; i < this.getDimension(); i++) {
            Double val_1 = this.values.get(i);
            Double val_2 = s2.getValueAt(i);
            if (val_1.doubleValue() == val_2.doubleValue()) { all++; }
        }
        
        return all == this.getDimension();
    }
    
    
    
    public String shapeString() {
        return shapeString(null, false);
    }
    
    public String shapeString(ArrayList<String> objNames, boolean showObjNames) {
        
        String ret = "[";
        String sep = "";
        ArrayList<Integer> shape = this.getShape();
        for (int i = 0; i < shape.size(); i++) {
            if (i > 0) { sep = ","; }
                ret += sep + shape.get(i);
            
        }        
        ret += "]";
        
        return ret;
    }
    
    
    
    
    
    public String prettyString() {
        return prettyString(true);
    }
    public String prettyString(boolean shape) {
        
        ArrayList<Integer> axesOrder = PlotFrame.settings.getCurrentAxesOrder();        
        String ret = getName() + " (";
        String sep = "";
        for (int i = 0; i < axesOrder.size(); i++) {
            if (i > 0) { sep = ";"; }
            ret += sep + String.format("%.4f", values.get(axesOrder.get(i)));
        }
        ret += ")";
        if (shape) ret += shapeString();       
        return ret;
    }
        
    public String valuesString() {
        
        ArrayList<Integer> axesOrder = PlotFrame.settings.getCurrentAxesOrder();        
        String ret = "";
        String sep = "";
        for (int i = 0; i < axesOrder.size(); i++) {
            if (i > 0) { sep = ";"; }
            ret += sep + String.format("%f", values.get(axesOrder.get(i)));
        }
        
        return ret;
        
    }
    
    public String toString() {
        String ret = id + ":(";
        String sep = "";
        for (int i = 0; i < values.size(); i++) {
            if (i > 0) { sep = ","; }
            ret += sep + values.get(i);
        }
        
        ret += ")[";
        sep = "";
        ArrayList<Integer> shape = this.getShape();
        for (int i = 0; i < shape.size(); i++) {
            if (i > 0) { sep = ","; }
            ret += sep + shape.get(i);
        }        
        ret += "]";
        
        return ret;
    }



    
}
