/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.solution;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author acost
 */
public class Group {

    public static int last_group_id = 0;
    
    Integer groupId;
    String groupName;
    Set<Integer> ids;        

    public Group() {        
        this.ids = new HashSet<>();
        this.groupId = last_group_id++;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer id) {
        this.groupId = id;
    }

   
    
    
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Set<Integer> getIds() {
        return ids;
        
    }

    public void setIds(Set<Integer> ids) {
        this.ids = ids;
    }
    
    public boolean isInGroup(Solution sol) {
        return ids.contains(sol.getId());
    }
    
    public boolean isInGroup(Integer id) {
        return ids.contains(id);
    }
    
    public int getSize() {
        return ids.size();
    }
    
    public Group getIntersectionWith(Group group) {
        Group intersection = new Group();
        Set<Integer> newIds = new HashSet<Integer>();        
        newIds.addAll(ids);
        newIds.removeIf((t) -> {
            return !group.isInGroup(t); 
        });
        
        intersection.setIds(newIds);
        return intersection;        
    }
    
    public Group getUnionWith(Group group) {
        Group union = new Group();
        Set<Integer> newIds = new HashSet<Integer>();        
        newIds.addAll(ids);
        newIds.addAll(group.getIds());
        
        union.setIds(newIds);
        return union;        
    }
    
    
    public Group getDifferenceWith(Group group) {
        Group difference = new Group();
        Set<Integer> newIds = new HashSet<Integer>();        
        newIds.addAll(ids);        
        newIds.removeIf((t) -> {
            return group.isInGroup(t); 
        });
        
        difference.setIds(newIds);
        return difference;        
    }
    
}
