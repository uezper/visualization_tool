/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.solution;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Set;
import py.una.pol.setviz.gui.PlotFrame;

/**
 *
 * @author acost
 */
public class SolutionShape {

    ArrayList<Integer> shape;
    Solution solution;

    public SolutionShape(Solution solution) {
        
        this.solution = solution;
        this.shape = new ArrayList<>();
    }
   
    public final void recalculateShape(boolean isMinProblem) {
        
        
        shape = new ArrayList<>();        
        ArrayList<Double> values = solution.getValues();
        ArrayIndexCompatator aic = new ArrayIndexCompatator(values, isMinProblem);
        ArrayList<Integer> indexes = aic.createIndexArray();        
        indexes.sort(aic);                
             
        for (int i = 0; i < PlotFrame.settings.getShownObjectives().size(); i++) {
            shape.add(indexes.get(i) + 1);
        }
        

    }
    
    public ArrayList<Integer> getShape() {
        return shape;
    }
    
    public double distanceTo(SolutionShape shape2) {        
        /* TODO!! Controlar que los shape tengan la misma dimension */
                
        ArrayList values2 = shape2.getShape();
        
        double r = 0.0;
        for(int i = 1; i <= PlotFrame.settings.getShownObjectives().size(); i++) {
            int a = shape.indexOf(i);
            int b = values2.indexOf(i);
            
            r += (a-b)*(a-b);
        }
            
        return Math.sqrt(r);
    }
    
    private class ArrayIndexCompatator implements Comparator<Integer> {

        private ArrayList<Double> values;
        private boolean isMin;
        
        public ArrayIndexCompatator(ArrayList<Double> values, boolean isMin) {
            this.values = values;            
            this.isMin = isMin;
        }
        
        public ArrayList<Integer> createIndexArray() {
            ArrayList<Integer> indexes = new ArrayList<>();
            Set<Integer> shown = PlotFrame.settings.getShownObjectives();
            
            for (int i = 0; i < shown.size(); i++) {
                 indexes.add(i);
            }
            return indexes;
        }
        
        
        @Override
        public int compare(Integer o1, Integer o2) {
            ArrayList<Integer> order = PlotFrame.settings.getCurrentAxesOrder();
            if (isMin) return (values.get(order.get(o1)) <= values.get(order.get(o2)) ? -1 : 1);
            return (values.get(order.get(o1)) >= values.get(order.get(o2)) ? -1 : 1);
        }
               
        
    }

    
}
