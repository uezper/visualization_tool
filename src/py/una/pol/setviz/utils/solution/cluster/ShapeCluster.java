/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.solution.cluster;

import java.util.ArrayList;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.solution.Solution;
import py.una.pol.setviz.utils.solution.SolutionExtraInformation;
import py.una.pol.setviz.utils.solution.SolutionSet;

/**
 *
 * @author acost
 */
public class ShapeCluster {
    
    public ArrayList<Integer> ids;
    public Solution representative;


    public ShapeCluster(Integer id) {
        this.ids = new ArrayList<>();
        this.ids.add(id);
        this.representative = PlotFrame.solutionDraw.getSolutionSet().getSolutionById(id);
    }

    public ShapeCluster(ArrayList<Integer> solutionIds) {
        
        SolutionSet solutionSet = PlotFrame.solutionDraw.getSolutionSet();
        
        this.ids = new ArrayList<>(solutionIds.size());


        ArrayList<Double> rep_values_max = new ArrayList<>();
        ArrayList<Double> rep_values_min = new ArrayList<>();

        boolean isMin = true;

        for(Integer solutionId : solutionIds) {                
            this.ids.add(solutionId);

            if (rep_values_max.isEmpty()) {
                for (Double v : solutionSet.getSolutionById(solutionId.intValue()).getValues()) {
                    rep_values_max.add(v);
                    rep_values_min.add(v);
                }
                isMin = PlotFrame.isMinProblem;
            } else {
                ArrayList<Double> values = solutionSet.getSolutionById(solutionId.intValue()).getValues();
                for(int k = 0; k < values.size(); k++) {
                    if (rep_values_max.get(k) < values.get(k)) {
                        rep_values_max.set(k, values.get(k));
                    }
                    if (rep_values_min.get(k) > values.get(k)) {
                        rep_values_min.set(k, values.get(k));
                    }
                }
            }
        }

        ArrayList<Double> rep_values = new ArrayList<>();
        for(int i = 0; i < rep_values_max.size(); i++) {
            rep_values.add((rep_values_max.get(i) + rep_values_min.get(i)) / 2);
        }

        representative = new Solution(rep_values);
        representative.putExtraInformation(SolutionExtraInformation.SOLUTION_IS_REPRESENTATIVE, true);
        
        
    }                

    public ArrayList<Integer> getIndexesCopy() {
        ArrayList<Integer> copy = new ArrayList<>();
        for (int i = 0; i < ids.size(); i++) {
            copy.add(new Integer(ids.get(i)));
        }
        return copy;
    }

}
