/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.solution.cluster;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.solution.Group;
import py.una.pol.setviz.utils.solution.Solution;
import py.una.pol.setviz.utils.solution.SolutionExtraInformation;
import py.una.pol.setviz.utils.solution.SolutionSet;

/**
 *
 * @author acost
 */
public class ClusterFactory {
    
    
    public static ArrayList<ShapeCluster> makeShapeClusters(ArrayList<Integer> solutionIds) {
        
        ArrayList<ShapeCluster> clusters = new ArrayList<>();        
        Set<Integer> markedIds = new HashSet<>();
        SolutionSet solutionSet = PlotFrame.solutionDraw.getSolutionSet();        

        
        ShapeCluster cluster;                
        
        
        for(int i = 0; i < solutionIds.size(); i++) {
            Integer solutionId_1 = solutionIds.get(i);
            if (markedIds.contains(solutionId_1)) continue;
            
            Solution sol_1 = solutionSet.getSolutionById(solutionId_1);
            markedIds.add(solutionId_1);
            
            ArrayList<Integer> clusterSolutionsId = new ArrayList<>();            
            clusterSolutionsId.add(sol_1.getId());
            
            for(int j = 0; j < solutionIds.size(); j++) {
                Integer solutionId_2 = solutionIds.get(j);
                if (markedIds.contains(solutionId_2) || i == j) continue;
                
                Solution sol_2 = solutionSet.getSolutionById(solutionId_2);
                
                if (sol_1.shapeString().equalsIgnoreCase(sol_2.shapeString())) {
                    clusterSolutionsId.add(solutionId_2);
                    markedIds.add(solutionId_2);
                }
            }
            
            cluster = new ShapeCluster(clusterSolutionsId);           
            cluster.representative.getSolutionShape().recalculateShape(PlotFrame.isMinProblem);
            cluster.representative.putExtraInformation(SolutionExtraInformation.SOLUTION_CLUSTER_SHAPE, cluster);                        
            clusters.add(cluster);
        }       
        
        // Sort so clusters with more ammount of solutions are first
        clusters.sort(new Comparator<ShapeCluster>() {
            @Override
            public int compare(ShapeCluster o1, ShapeCluster o2) {
                return o1.ids.size() >= o2.ids.size() ? -1 : 1;
            }
        });

        
        return clusters;
    }



    public static ArrayList<KmeansCluster> makeKmeansClusters(ArrayList<Integer> solutionIds, int K) {
    
        SolutionSet solutionSet = PlotFrame.solutionDraw.getSolutionSet();
        ArrayList<KmeansCluster> clusters = new ArrayList<>();
        HashMap<Integer, Set<Integer>> solutionIdsByClusterId = new HashMap<>();
        HashMap<Integer, KmeansCluster> clusterBySolutionId = new HashMap<>();
        Set<Integer> initialPoints = new HashSet<>();
        
        // We get the first initial point
        Integer initialPoint = solutionIds.get(new Random().nextInt(solutionIds.size()));
        KmeansCluster cluster = new KmeansCluster(initialPoint);
        clusters.add(cluster);
        solutionIdsByClusterId.put(cluster.getId(), new HashSet<>());
        initialPoints.add(initialPoint);
        
        // We get the next (k-1) initial points
        for(int i = 1; i < K; i++) {            
            
            Double maxDistance = null;
            Integer maxDistanceId = null;
            for(int j = 0; j < solutionIds.size(); j++) {
                Double minDistance = null;
                Integer minDistanceId = null;
                for(Integer initialPointId : initialPoints) {
                    Double distance = getDistance(
                            solutionSet.getSolutionById(initialPointId), 
                            solutionSet.getSolutionById(solutionIds.get(j)));
                    
                    if (minDistance == null || minDistance > distance) {
                        minDistance = distance;
                        minDistanceId = solutionIds.get(j);
                    }
                }
                if (maxDistance == null || maxDistance < minDistance) {
                    maxDistance = minDistance;
                    maxDistanceId = minDistanceId;
                }
            }
            
            initialPoint = maxDistanceId;
            cluster = new KmeansCluster(initialPoint);
            clusters.add(cluster);
            solutionIdsByClusterId.put(cluster.getId(), new HashSet<>());
            initialPoints.add(initialPoint);
        }
        
        
        // We start the iteration
        boolean solutionMoved = true;
        while(solutionMoved) {
            
            solutionMoved = false;
            for (int i = 0; i < solutionIds.size(); i++) {                
                Solution sol = solutionSet.getSolutionById(solutionIds.get(i));                
                
                Double minDistance = null;
                KmeansCluster minCluster = null;
                for(KmeansCluster actualCluster : clusters) {
                    Double distance = getDistance(actualCluster.getRepresentative(), sol);
                    
                    if (minDistance == null || minDistance > distance) {
                        minDistance = distance;
                        minCluster = actualCluster;
                    }
                }
               
                if (minCluster == null) continue;
                
                // We move the solution to the new cluster ids set and remove from the previous one
                if (!solutionIdsByClusterId.get(minCluster.getId()).contains(sol.getId())) {
                    solutionMoved = true;
                    solutionIdsByClusterId.get(minCluster.getId()).add(sol.getId());
                    KmeansCluster oldCluster = clusterBySolutionId.get(sol.getId());
                    if (oldCluster != null) {
                        solutionIdsByClusterId.get(oldCluster.getId()).remove(sol.getId());                        
                    }                   
                    clusterBySolutionId.put(sol.getId(), minCluster);
                }
                
                
            }                      
            
                        
            // We assign each set to its cluster
            for (int i = 0; i < clusters.size(); i++) {                
                cluster = clusters.get(i);
                cluster.setSolutionsIds(new ArrayList<>(solutionIdsByClusterId.get(cluster.getId())));                               
                cluster.getRepresentative().getSolutionShape().recalculateShape(PlotFrame.isMinProblem);

            }
            
        }
                
        
        return clusters;
    }

    private static Double getDistance(Solution sol_1, Solution sol_2) {
        
        Double ret = 0.0;
        for (int i = 0; i < PlotFrame.settings.getShownObjectives().size(); i++) {
            ret += Math.pow((sol_1.getValueAt(i) - sol_2.getValueAt(i)), 2);
        }
        
        ret = Math.sqrt(ret);
        return ret;
    }
    

    public static ArrayList<GroupCluster> makeGroupClusters(ArrayList<Integer> solutionIds) {
        
        Map<Integer, Group> groups = PlotFrame.groups;
        
        Map<String, GroupCluster> groupClusters = new HashMap<>();
        Set<Integer> unAssignedSolutions = new HashSet<>(solutionIds);
        
        for(Group group : groups.values()) {            
            for(Integer solutionId : group.getIds()) {            
                if (solutionIds.contains(solutionId)) {
                    
                    unAssignedSolutions.remove(solutionId);
                    
                    if (!groupClusters.containsKey(group.getGroupName())) {
                        groupClusters.put(group.getGroupName(), new GroupCluster(group.getGroupName()));
                    }
                    
                    groupClusters.get(group.getGroupName()).addSolutionId(solutionId);                    
                }
            }            
        }
        
        if (!unAssignedSolutions.isEmpty()) {
            String name = "No group";
            if (groupClusters.containsKey(name)) {
                name += UUID.randomUUID().toString().substring(0, 4);
            }
            
            GroupCluster gc = new GroupCluster(name, new ArrayList<>(unAssignedSolutions));
            groupClusters.put(name, gc);
        }
        
        return new ArrayList<>(groupClusters.values());
    }
    
    
}
