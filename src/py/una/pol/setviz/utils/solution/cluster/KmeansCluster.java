/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.solution.cluster;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.solution.Solution;
import py.una.pol.setviz.utils.solution.SolutionSet;

/**
 *
 * @author acost
 */
public class KmeansCluster {
    
    private static Integer last_id = 0;
    private Set<Integer> solutionIds;
    private int id;    
    private Solution representative;
    private boolean lazyRepresentative;

    
    public static void resetIds() {
        last_id = 0;
    }
    
    public KmeansCluster(Integer id) {
        this.id = last_id++;
        this.solutionIds = new HashSet<>();
        this.solutionIds.add(id);
        this.representative = PlotFrame.solutionDraw.getSolutionSet().getSolutionById(id);
        this.lazyRepresentative = false;
    }

        
    public int getId() {
        return this.id;
    }
    
    public KmeansCluster(ArrayList<Integer> solutionIds) {
        
        this.id = last_id++;
        SolutionSet solutionSet = PlotFrame.solutionDraw.getSolutionSet();       
        this.solutionIds = new HashSet<>(solutionIds);
        this.lazyRepresentative = true;
    }                

    public int getSize() {
        return solutionIds.size();
    }
    public Set<Integer> getSolutionIds() {
        Set<Integer> ret = new HashSet<>(solutionIds);
        return ret;
    }
    
    public void addSolutions(ArrayList<Integer> solutionsIds) {
        solutionsIds.addAll(solutionIds);
        lazyRepresentative = true;
    }
    
    public void setSolutionsIds(ArrayList<Integer> solutionIds) {
        this.solutionIds = new HashSet<>(solutionIds);                
        lazyRepresentative = true;
    }
    
    public void removeSolution(Integer solutionId) {
        this.solutionIds.remove(solutionId);
        lazyRepresentative = true;
    }
    
    public Solution getRepresentative() {
        
        if (lazyRepresentative) {
            
            ArrayList<Double> sumValues = new ArrayList<>(PlotFrame.solutionDraw.getSolutionSet().getDimensions());
        
            for(int i = 0; i < PlotFrame.solutionDraw.getSolutionSet().getDimensions(); i++) {
                sumValues.add(0.0);
            }
            
            
            for(Integer solutionID : solutionIds) {
                Solution sol = PlotFrame.solutionDraw.getSolutionSet().getSolutionById(solutionID);
                
                for(int i = 0; i < sol.getDimension(); i++) {
                  Double value = sol.getValueAt(i);                  
                  sumValues.set(i, sumValues.get(i) + value);
                  
                }
            }
            
            for(int i = 0; i < sumValues.size(); i++) {
                sumValues.set(i, sumValues.get(i) / solutionIds.size());
            }
            
            
            representative = new Solution(sumValues);
            lazyRepresentative = false;
        }
        
        return representative;
    }

    public String toString() {
        return "" + this.id;
    }
    

}
