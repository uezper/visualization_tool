/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.solution.cluster;

import java.util.ArrayList;

/**
 *
 * @author acost
 */
public class GroupCluster {
    
    public ArrayList<Integer> ids;
    String name;

    public GroupCluster(String name) {
        this.name = name;
        this.ids = new ArrayList<>();
    }


    public GroupCluster(String name, ArrayList<Integer> solutionIds) {        
        this.name = name;
        this.ids = new ArrayList<>(solutionIds);        
    }                

    public void addSolutionId(Integer id) {
        this.ids.add(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    
    public ArrayList<Integer> getIndexesCopy() {
        ArrayList<Integer> copy = new ArrayList<>(ids);
        for (int i = 0; i < ids.size(); i++) {
            copy.add(new Integer(ids.get(i)));
        }
        return copy;
    }

}
