/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.solution.preferences;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import py.una.pol.setviz.utils.math.Matrix;

/**
 *
 * @author uriel
 */
public class SolutionPreference {    
    
    public static class Priority {
        Integer group;
        Double priority;

        public Integer getGroup() {
            return group;
        }

        public void setGroup(Integer group) {
            this.group = group;
        }

        public Double getPriority() {
            return priority;
        }

        public void setPriority(Double priority) {
            this.priority = priority;
        }

        @Override
        public String toString() {
            return "Priority{" + "group=" + group + ", priority=" + priority + '}';
        }
        
        
    }
    
    public static enum PreferenceType {
        LESS_IMPORTANT_THAN("LIT", "MIT", "Less important than", 0.35),
        MUCH_LESS_IMPORTANT_THAN("MLIT", "MMIT", "Much less important than", 0.05),
        MORE_IMPORTANT_THAN("MIT", "LIT", "More important than", 0.65),
        MUCH_MORE_IMPORTANT_THAN("MMIT", "MLIT", "Much more important than", 0.95),
        EQUALLY_IMPORTANT_TO("EIT", "EIT", "Equally important to", 0.5);

        private String code;
        private String description;
        private String complementCode;
        private Double value;
        private PreferenceType(String code, String complementCode, String description, Double value) {
            this.code = code;
            this.value = value;
            this.description = description;
            this.complementCode = complementCode;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public String getComplementCode() {
            return complementCode;
        }

        public void setComplementCode(String complementCode) {
            this.complementCode = complementCode;
        }
        
        
        public PreferenceType getComplement() {
            return getFromCode(complementCode);
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
        
        
        public static PreferenceType getFromCode(String code) {
            
            for(PreferenceType p : PreferenceType.values()) {
                if (p.getCode().equalsIgnoreCase(code)) {
                    return p;
                }
            }
            
            return null;
        }
        

        @Override
        public String toString() {
            return "PreferenceType{" + "code=" + code + ", value=" + value + '}';
        }

    }

    private class CellValue {

        PreferenceType preference;
        Integer value;

        public CellValue() {
            this.preference = null;
            this.value = 0;
        }

        
        
        @Override
        public String toString() {
            return "CellValue{" + "preference=" + preference + ", value=" + value + '}';
        }

    }

    public class Relation {
        int group1;
        PreferenceType preference;
        int group2;

        public Relation(int group1, PreferenceType preference,  int group2) {
            this.group1 = group1;
            this.group2 = group2;
            this.preference = preference;
        }

        public int getGroup1() {
            return group1;
        }

        public void setGroup1(int group1) {
            this.group1 = group1;
        }

        public PreferenceType getPreference() {
            return preference;
        }

        public void setPreference(PreferenceType preference) {
            this.preference = preference;
        }

        public int getGroup2() {
            return group2;
        }

        public void setGroup2(int group2) {
            this.group2 = group2;
        }

        
        
        @Override
        public String toString() {
            return "Relation{" + "group1=" + group1 + ", preference=" + preference + ", group2=" + group2 + '}';
        }
        
        

        
    }
    
    List<Relation> answeredQuestions;
    List<Relation> unAnsweredQuestions;
    List<Priority> priorities;
    int groups;
    Matrix<CellValue> preferences;
    boolean finished;
    

    public SolutionPreference(int groups) {
        this.groups = groups;
        priorities = new ArrayList<>(groups);
        preferences = new Matrix<>(groups);
        finished = false;
        
        answeredQuestions = new ArrayList<>();
        unAnsweredQuestions = new ArrayList<>();
        
        for (int i = 0; i < groups; i++) {
            for (int j = 0; j < groups; j++) {
                preferences.setValue(i, j, new CellValue());
                
                if (i == j) {
                    preferences.getValue(i, j).value = 1;
                    preferences.getValue(i, j).preference = PreferenceType.EQUALLY_IMPORTANT_TO;
                }
            }
        }
        
        updatePreferences();
        
    }
    

    public boolean finished() {
        return finished;
    }

    public List<Priority> getPriorities() {
        if (!finished) {
            return null;
        }
        return priorities;
    }
    
    public void iterate(int group1, PreferenceType preferenceType, int group2) {
        
        if (finished) return;
        if (group1 < 0 || group1 >= groups || group2 < 0 || group2 >= groups) return;
        if (preferences.getValue(group1, group2).value + preferences.getValue(group2, group1).value != 0) return;
        
        setPreference(group1, preferenceType, group2);
        setPreference(group2, preferenceType.getComplement(), group1);
        
        calculateTransitiveClosure();        
        updatePreferences();
        
        
        if (finished) {
            calculatePriorities();
        }
        
    }
    
    
    
    private void calculatePriorities() {
        
       List<Double> SLi = new ArrayList<>(groups);
       Double SumSLi = 0.0;
               
       for (int i = 0; i < groups; i++) {
           SLi.add(0.0);
           
           for (int j = 0; j < groups; j++) {
               if (i == j) continue;
               
               SLi.set(i, SLi.get(i) + preferences.getValue(i, j).preference.value);
           }
           
           SumSLi += SLi.get(i);
       }        
       
       
       for (int i = 0; i < groups; i++) {
           Priority priority = new Priority();
           priority.setGroup(i);
           priority.setPriority(SLi.get(i) / SumSLi);
           priorities.add(priority);
       }
       
       priorities.sort(new Comparator<Priority>() {
           @Override
           public int compare(Priority o1, Priority o2) {
               if (o1.priority < o2.priority) return 1;
               if (o1.priority > o2.priority) return -1;
               return 0;
           }
       });
       
    }   
    
    
    private void updatePreferences() {
        
        answeredQuestions = new ArrayList<>();
        unAnsweredQuestions = new ArrayList<>();
        
        boolean foundZero = false;
        for (int i = 0; i < groups; i++) {
            for (int j = i; j < groups; j++) {
                updatePreferenceType(i, j);
                if (preferences.getValue(i, j).value + preferences.getValue(j, i).value == 0) {
                    foundZero = true;                    
                    unAnsweredQuestions.add(new Relation(i,null, j));
                } else if (i != j) {
                    answeredQuestions.add(new Relation(i,preferences.getValue(i, j).preference, j));
                }
            }
        }
        
        finished = !foundZero;
    }
    
    private void updatePreferenceType(int group1, int group2) {
        
        int value1 = preferences.getValue(group1, group2).value;
        int value2 = preferences.getValue(group2, group1).value;
        
        if (value1 == 0) {            
            if (value2 == 1) {                
                setPreference(group1, PreferenceType.LESS_IMPORTANT_THAN, group2);
                setPreference(group2, PreferenceType.LESS_IMPORTANT_THAN.getComplement(), group1);                
            } else if (value2 == 2) {
                setPreference(group1, PreferenceType.MUCH_LESS_IMPORTANT_THAN, group2);
                setPreference(group2, PreferenceType.MUCH_LESS_IMPORTANT_THAN.getComplement(), group1);
            }
            
        } else if (value1 == 1) {            
            if (value2 == 0) {
                setPreference(group1, PreferenceType.MORE_IMPORTANT_THAN, group2);
                setPreference(group2, PreferenceType.MORE_IMPORTANT_THAN.getComplement(), group1);   
            } else if (value2 == 1) {
                setPreference(group1, PreferenceType.EQUALLY_IMPORTANT_TO, group2);
                setPreference(group2, PreferenceType.EQUALLY_IMPORTANT_TO.getComplement(), group1);            
            }            
        } else if (value1 == 2) {            
            if (value2 == 0) {
                setPreference(group1, PreferenceType.MUCH_MORE_IMPORTANT_THAN, group2);
                setPreference(group2, PreferenceType.MUCH_MORE_IMPORTANT_THAN.getComplement(), group1);
            }            
        }
        
    }
    
    private void calculateTransitiveClosure() {
        
        for (int k = 0; k < groups; k++) {
            for (int i = 0; i < groups; i++) {
                for (int j = 0; j < groups; j++) {
                    preferences.getValue(i, j).value = Math.min(2, Math.max(preferences.getValue(i, j).value, 
                            preferences.getValue(i, k).value * preferences.getValue(k, j).value));
                }
            }
        }
        
    }
    
    private void setPreference(int group1, PreferenceType type, int group2) {

        preferences.getValue(group1, group2).preference = type;        
        switch(type) {       
            case EQUALLY_IMPORTANT_TO:
                   preferences.getValue(group1, group2).value = 1;
                   break;
            case LESS_IMPORTANT_THAN:
                   preferences.getValue(group1, group2).value = 0;
                   break;
            case MUCH_LESS_IMPORTANT_THAN:
                   preferences.getValue(group1, group2).value = 0;
                   break;                
            case MORE_IMPORTANT_THAN:
                   preferences.getValue(group1, group2).value = 1;
                   break;                
            case MUCH_MORE_IMPORTANT_THAN:
                   preferences.getValue(group1, group2).value = 2;
                   break;                
        }
        
    }

    public List<Relation> getAnsweredQuestions() {
        return answeredQuestions;
    }

    public List<Relation> getUnAnsweredQuestions() {
        return unAnsweredQuestions;
    }
    
    
    
    
    
}
