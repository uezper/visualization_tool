/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.solution.ranking;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.math.Graph;
import py.una.pol.setviz.utils.solution.Solution;
import py.una.pol.setviz.utils.solution.SolutionSet;

/**
 *
 * @author acost
 */
public class RankByEPreferred implements SolutionRanking {

    private List<Integer> solutionIds;
    private List<Double> eLimits; 
    private List<RankLevel> generatedSet;
    private boolean ranked;
            
    public RankByEPreferred(Set<Integer> solutionIds, List<Double> eLimits) {
        this.solutionIds = solutionIds.stream().sorted().collect(Collectors.toList());
        this.eLimits = eLimits;
        this.ranked = false;                
    }

    
    @Override
    public void doRanking() {
        this.generatedSet = new ArrayList<>();
            
        Graph dominationGraph = new Graph();
        
        SolutionSet solutionSet = PlotFrame.solutionDraw.getSolutionSet();        
        
        for(int i = 0; i < solutionIds.size(); i++) {
            Integer idA = solutionIds.get(i);
            Solution solA = solutionSet.getSolutionById(idA);
            for(int j = i+1; j < solutionIds.size(); j++) {
                Integer idB = solutionIds.get(j);
                Solution solB = solutionSet.getSolutionById(idB);
                
                int sA_dominates_sB = ePreferredRelation(solA, solB);
                                
                if (sA_dominates_sB < 0) {
                   dominationGraph.addDirectedEdge(String.valueOf(idA), String.valueOf(idB));
                } else if (sA_dominates_sB > 0) {
                   dominationGraph.addDirectedEdge(String.valueOf(idB), String.valueOf(idA));
                }
                
            }
        }
        
        this.generatedSet = new ArrayList<>();
        List<Set<String>> levels = dominationGraph.SCO();
        for (int i  = 0; i < levels.size(); i++) {
            RankLevel level = new RankLevel();
            level.level = i+1;
            level.solutionIds = new ArrayList<>();
            
            for (String id : levels.get(i)) {
                level.solutionIds.add(Integer.parseInt(id));
            }
            
            this.generatedSet.add(level);
        }
        
       
    }

    private int ePreferredRelation(Solution s1, Solution s2) {
                
        
        List<Integer> axesOrder = PlotFrame.settings.getCurrentAxesOrder();
        
        int s1_nb_s2 = 0;
        int s2_nb_s1 = 0;
        int s1_ExceedsNum_s2 = 0;
        int s2_ExceedsNum_s1 = 0;
        
        for(int i = 0; i < axesOrder.size(); i++) {
            double eLimit = eLimits.get(i);            
            double s1_value = s1.getValueAt(axesOrder.get(i));
            double s2_value = s2.getValueAt(axesOrder.get(i));
            double diff = Math.abs(s1_value - s2_value);
           
            boolean s1_betterThan_s2 = betterThan(s1_value, s2_value);
            boolean s2_betterThan_s1 = betterThan(s2_value, s1_value);
            
            if (s1_betterThan_s2) {
                s1_nb_s2++;
                if (diff > eLimit) s1_ExceedsNum_s2++;
                
            }            
            
            if (s2_betterThan_s1) {
                s2_nb_s1++;
                if (diff > eLimit) s2_ExceedsNum_s1++;
                
            }            
            
            
        }
                
        
        boolean s1_Exceeds_s2 = s1_ExceedsNum_s2 > s2_ExceedsNum_s1;
        boolean s2_Exceeds_s1 = s2_ExceedsNum_s1 > s1_ExceedsNum_s2;
                
        boolean s1_FavouredTo_s2 = s1_nb_s2 > s2_nb_s1;
        boolean s2_FavouredTo_s1 = s2_nb_s1 > s1_nb_s2;
        
        boolean s1_dominates_s2 = s1_Exceeds_s2 || (!s2_Exceeds_s1 && s1_FavouredTo_s2);
        boolean s2_dominates_s1 = s2_Exceeds_s1 || (!s1_Exceeds_s2 && s2_FavouredTo_s1);
        
        if (s1_dominates_s2) return -1;
        if (s2_dominates_s1) return 1;
        return 0;
    }
    
    
    private boolean betterThan(Double v1, Double v2) {
        boolean isMin = PlotFrame.isMinProblem;
        return (isMin && v1 < v2) || (!isMin && v1 > v2);
    }
    
    
    @Override
    public List<RankLevel> getNFirstSolutions(int n) {
        if (!ranked) doRanking();
        
        List<RankLevel> ret = new ArrayList<>();
        int count = 0;        
        for (int i = 0; i < generatedSet.size(); i++) {
            RankLevel rankL = generatedSet.get(i);           
            ret.add(rankL);
            count++;
            if (count >= n) return ret;            
        }
        
        return ret;
    }

    
}
