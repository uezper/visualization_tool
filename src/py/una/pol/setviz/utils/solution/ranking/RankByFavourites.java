/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.solution.ranking;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.math.Graph;
import py.una.pol.setviz.utils.solution.Solution;
import py.una.pol.setviz.utils.solution.SolutionSet;

/**
 *
 * @author acost
 */
public class RankByFavourites implements SolutionRanking {

    private static Map<String, List<Integer>> favouriteRelationships = null;
    
    private static class Rank {
        public int rank = 0;
        public List<Integer> solutionIds;
    }
    
    private final List<Integer> solutionIds;
    private List<RankLevel> generatedSet;
    private ArrayList<Integer> priorities;
    private boolean ranked;
            
    public RankByFavourites(Set<Integer> solutionIds) {
        this.solutionIds = solutionIds.stream().sorted().collect(Collectors.toList());
        this.ranked = false;
                
        
    }
    
    @Override
    public void doRanking() {        
        this.ranked = true;        
        
        SolutionSet solutionSet = PlotFrame.solutionDraw.getSolutionSet();
        Graph dominationGraph = new Graph();       
        
        
        for (int i = 0; i < solutionIds.size(); i++) {
            Integer idA = solutionIds.get(i);
            Solution solA = solutionSet.getSolutionById(idA);
            
            for (int j = i+1; j < solutionIds.size(); j++) {                                
                Integer idB = solutionIds.get(j);
                Solution solB = solutionSet.getSolutionById(idB);
                                
                FavourRelationType relAtoB = calculateFavourBetween(solA, solB);
                
                switch(relAtoB) {
                    case DOMINATES:                        
                        dominationGraph.addDirectedEdge(String.valueOf(idA), String.valueOf(idB));
                        break;
                    case IS_DOMINATED:                        
                        dominationGraph.addDirectedEdge(String.valueOf(idB), String.valueOf(idA));
                        break;
                    default:
                        break;
                }
                
                
            }
        }
        
        
        List<Set<String>> levels = dominationGraph.SCO();
        this.generatedSet = new ArrayList<>();        
        for (int i  = 0; i < levels.size(); i++) {
            RankLevel level = new RankLevel();
            level.level = i+1;
            level.solutionIds = new ArrayList<>();
            
            for (String id : levels.get(i)) {
                level.solutionIds.add(Integer.parseInt(id));
            }
            
            this.generatedSet.add(level);
        }
                
    }
    
    
    public FavourRelationType calculateFavourBetween(Solution a, Solution b) {
        
        List<Integer> axesOrder = PlotFrame.settings.getCurrentAxesOrder();
        
        int aBetter = 0;
        int bBetter = 0;
        
        for (int i = 0; i < a.getDimension(); i++) {
            Double aValue = a.getValueAt(axesOrder.get(i));
            Double bValue = b.getValueAt(axesOrder.get(i));
            
            int c = compare(aValue, bValue);
            if (c < 0) { aBetter++; }
            if (c > 0) { bBetter++; }
        }
        
        
        if (aBetter > bBetter) { return FavourRelationType.DOMINATES; }
        if (aBetter < bBetter) { return FavourRelationType.IS_DOMINATED; }
        return FavourRelationType.NONE;
        
    }    
  
    public int compare(Double a, Double b) {
        
        if (PlotFrame.isMinProblem) {            
            if (a < b) { return -1; }
            if (a > b) { return 1; }
        } else {
            if (a > b) { return -1; }
            if (a < b) { return 1; }
        }
        return 0;
    }
    
    public static enum FavourRelationType {
        DOMINATES,
        IS_DOMINATED,
        NONE
    }
    
    
    @Override
    public List<RankLevel> getNFirstSolutions(int n) {
        if (!ranked) doRanking();
        
        List<RankLevel> ret = new ArrayList<>();
        int count = 0;        
        for (int i = 0; i < generatedSet.size(); i++) {
            RankLevel rankL = generatedSet.get(i);           
            ret.add(rankL);
            count++;
            if (count >= n) return ret;            
        }
        
        return ret;
    }

    

    
}
