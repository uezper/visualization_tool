/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.solution.ranking;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acost
 */
public interface SolutionRanking {
           
    public void doRanking();
    public List<RankLevel> getNFirstSolutions(int n);
    
    
    public static class RankLevel {
        public Integer level;
        public List<Integer> solutionIds;
        
        public RankLevel() {
            solutionIds = new ArrayList<>();
        }
    }
    
}
