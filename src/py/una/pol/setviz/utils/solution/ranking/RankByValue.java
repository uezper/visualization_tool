/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils.solution.ranking;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.solution.Solution;
import py.una.pol.setviz.utils.solution.SolutionSet;

/**
 *
 * @author acost
 */
public class RankByValue implements SolutionRanking {

    private Set<Integer> solutionIds;
    private ArrayList<Integer> generatedSet;
    private ArrayList<Integer> priorities;
    private boolean ranked;
    private Integer maxPriority;
            
    public RankByValue(Set<Integer> solutionIds) {
        this.solutionIds = new HashSet<>(solutionIds);
        this.ranked = false;
        
        
        this.priorities = new ArrayList<>();
        for (int i = 0; i < PlotFrame.settings.getShownObjectives().size(); i++) {
            this.priorities.add(1);
        }
    }

    public void setPriorities(ArrayList<Integer> priorities) {
        this.priorities = priorities;
    }

    
    @Override
    public void doRanking() {
        this.generatedSet = new ArrayList<>();
        
        maxPriority = null;
        
        SolutionSet solutionSet = PlotFrame.solutionDraw.getSolutionSet();
        HashMap<Integer, ArrayList<Double>> valuesSumBySolutionId = new HashMap<>();        
                
        for(Integer solutionId : solutionIds) {
            ArrayList<Double> newValues = new ArrayList<>();
            for (int i = 0; i < PlotFrame.settings.getShownObjectives().size(); i++) {
                newValues.add(0.0);
            }
            valuesSumBySolutionId.put(solutionId, newValues);
        }
        
        ArrayList<Integer> ids = new ArrayList<>(solutionIds);
        for(int i = 0; i < ids.size(); i++) {
            Solution sol_1 = solutionSet.getSolutionById(ids.get(i));
            ArrayList<Double> valuesSum_1 = valuesSumBySolutionId.get(ids.get(i));
            for(int j = i + 1; j < ids.size(); j++) {
                Solution sol_2 = solutionSet.getSolutionById(ids.get(j));
                ArrayList<Double> valuesSum_2 = valuesSumBySolutionId.get(ids.get(j));
                
                Double eps = getRange(sol_1, sol_2);
                for (int k = 0; k < PlotFrame.settings.getShownObjectives().size(); k++) {
                    Double diff = sol_1.getValueAt(k) - sol_2.getValueAt(k);                    
                    if (Math.abs(diff) >= eps) {
                        diff *= getPriority(k);
                        valuesSum_1.set(k, valuesSum_1.get(k) + diff);
                        valuesSum_2.set(k, valuesSum_2.get(k) - diff);                        
                    }                    
                }
                
                valuesSumBySolutionId.put(sol_2.getId(), valuesSum_2);                
            }
            valuesSumBySolutionId.put(sol_1.getId(), valuesSum_1);
            
        }
                
        HashMap<Integer, Double> finalValues = new HashMap<>();
        for(int i = 0; i < ids.size(); i++) {
            Double value = 0.0;
            ArrayList<Double> sumValues = valuesSumBySolutionId.get(ids.get(i));
            for (Double val : sumValues) {
                value += val;
            }
            finalValues.put(ids.get(i), value);
        }
        
        ids.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if (Objects.equals(finalValues.get(o1), finalValues.get(o2))) return 0;
                
                if (PlotFrame.isMinProblem) return finalValues.get(o1) < finalValues.get(o2) ? -1 : 1;
                else return finalValues.get(o1) > finalValues.get(o2) ? -1 : 1;
            }
        });
        
        generatedSet = ids;
        /*for (Integer id : generatedSet) {
            System.out.println(solutionSet.getSolutionById(id) + " - " + finalValues.get(id));
        }*/
    }

    private Integer getPriority(int index) {
       
        if (maxPriority == null) {
            maxPriority = priorities.get(0);
            for (int i = 1; i < priorities.size(); i++) {
                if (priorities.get(i) > maxPriority) maxPriority = priorities.get(i);
            }
        }
        
        int defPriority = priorities.get(index);
        return maxPriority + 1 - defPriority;
        
        
    }
    
    
    public Double getRange(Solution sol_1, Solution sol_2) {
        Double eps = 0.0; 
        for (int i = 0; i  < PlotFrame.settings.getShownObjectives().size(); i++) {
            Double val_1 = sol_1.getValueAt(PlotFrame.settings.getCurrentAxesOrder().get(i));
            Double val_2 = sol_2.getValueAt(PlotFrame.settings.getCurrentAxesOrder().get(i));
            Double diff = Math.abs(val_1 - val_2);
            if (diff > eps) eps = diff;
        }
        return eps / 3.0;
    }
    
    
    @Override
    public List<RankLevel> getNFirstSolutions(int n) {
        if (!ranked) doRanking();
        
        List<RankLevel>  ret = new ArrayList<>();
        for (int i = 0; i < generatedSet.size(); i++) {
            RankLevel rk = new RankLevel();
            rk.level = i+1;
            rk.solutionIds.add(generatedSet.get(i));
            ret.add(rk);
            if (i + 1 >= n) break;
        }
        
        return ret;
    }

    
}
