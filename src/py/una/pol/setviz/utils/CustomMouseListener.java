/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author acost
 */
public class CustomMouseListener implements MouseListener {

    FunctionHandler mouseClicked;
    FunctionHandler mousePressed;
    FunctionHandler mouseReleased;
    FunctionHandler mouseEntered;
    FunctionHandler mouseExited;
    
    public CustomMouseListener() {
        mouseClicked = (MouseEvent e) -> { };
        mousePressed = (MouseEvent e) -> { };
        mouseReleased = (MouseEvent e) -> { };
        mouseEntered = (MouseEvent e) -> { };
        mouseExited = (MouseEvent e) -> { };
    }

    public CustomMouseListener setMouseClicked(FunctionHandler mouseClicked) {
        this.mouseClicked = mouseClicked;
        return this;
    }

    public CustomMouseListener setMousePressed(FunctionHandler mousePressed) {
        this.mousePressed = mousePressed;
        return this;
    }

    public CustomMouseListener setMouseReleased(FunctionHandler mouseReleased) {
        this.mouseReleased = mouseReleased;
        return this;
    }

    public CustomMouseListener setMouseEntered(FunctionHandler mouseEntered) {
        this.mouseEntered = mouseEntered;
        return this;
    }

    public CustomMouseListener setMouseExited(FunctionHandler mouseExited) {
        this.mouseExited = mouseExited;
        return this;
    }
    
    
    public static interface FunctionHandler {
        public void handler(MouseEvent e);
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        mouseClicked.handler(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        mousePressed.handler(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        mousePressed.handler(e);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        mouseEntered.handler(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        mouseExited.handler(e);
    }
    
}
