/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils;

import java.awt.Color;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import py.una.pol.setviz.utils.solution.Solution;

/**
 *
 * @author acost
 */
public class DrawEntry {
 
    Optional<String> name;
    Set<Integer> solutionIds;
    Color color;
    Solution representative;
    
    public DrawEntry() {
        this.solutionIds = new HashSet<>();
        this.color = Color.BLACK;
        this.name = Optional.empty();
    }

    public Set<Integer> getSolutionIds() {
        return solutionIds;
    }

    public void setSolutionIds(Set<Integer> solutionIds) {
        this.solutionIds = solutionIds;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Solution getRepresentative() {
        return representative;
    }

    public void setRepresentative(Solution representative) {
        this.representative = representative;
    }

    public Optional<String> getName() {
        return name;
    }

    public void setName(Optional<String> name) {
        this.name = name;
    }
    
    
    
    
    
}
