/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils;

import py.una.pol.setviz.utils.solution.cluster.ShapeCluster;
import java.awt.Color;
import py.una.pol.setviz.utils.solution.Solution;

/**
 *
 * @author acost
 */
public class SelectionEntry {
    
    Color color;
    Solution solution;   
    ShapeCluster cluster;
    int size;
    SelectionType selectionType;
        
    public SelectionEntry() {        
        selectionType = SelectionType.SOLUTION;
        solution = null;
        color = Color.GRAY;
        size = 1;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    
    
    
    public boolean inSelection(Solution sol) {
        if (selectionType == SelectionType.SOLUTION) {
            if (solution == null) return false;
            return solution.getId() == sol.getId();
        } else {
            return cluster.ids.indexOf(sol.getId()) >= 0;
        }
    }

    public ShapeCluster getCluster() {
        return cluster;
    }

    public void setCluster(ShapeCluster cluster) {
        this.cluster = cluster;
    }
    
    
    
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    
    
    
    public Solution getSolution() {
        return solution;
    }

    public void setSolution(Solution solution) {
        this.solution = solution;
    }

    public SelectionType getSelectionType() {
        return selectionType;
    }

    public void setSelectionType(SelectionType selectionType) {
        this.selectionType = selectionType;
    }
    
    
    public enum SelectionType {
        SOLUTION,
        SET
    }
}
