/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils;

import java.io.FileWriter;
import java.util.List;

/**
 *
 * @author uriel
 */
public class ExportToCsvUtil {

    public static void export(List<String> titles, List<List<String>> values, String path) {

        try(FileWriter fw = new FileWriter(path)) {            
            
            String separator = "";
            for (String title: titles) {
                fw.write(separator + "\"" + title + "\"");
                separator = ",";
            }
            fw.write("\n");
            
            for (List<String> line : values) {                                
                separator = "";                
                for (String value: line) {
                    fw.write(separator + "\"" + value + "\"");
                    separator = ",";
                }                          
                fw.write("\n");
            }
            
            fw.close();            
        } catch (Exception e) {
            return;
        }        
    }

}

