/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils;

import py.una.pol.setviz.utils.solution.SolutionSet;
import py.una.pol.setviz.utils.solution.Solution;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author acost
 */
public class FileParser {
    
    
    public static SolutionSet parseCSV(String filename, String separator, boolean headLine, boolean firstColumnNames) {
        
        String line = "";
        String cvsSplitBy = ",";
        SolutionSet set = null;
        
        
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            
            int l = 1;
            while ((line = br.readLine()) != null) {
                if ((l == 1) && (headLine)) { 
                    l = l + 1;
                    continue; 
                }
                
                if (!line.contains(separator)) {
                    l = l + 1;
                    continue; 
                }
                
                Solution sol = null;
                String[] vals = line.split(separator);
                if (firstColumnNames) {
                    String[] newVals = new String[vals.length - 1];
                    int count = 0;
                    for (int i = 1; i < vals.length; i++) {
                        newVals[count++] = vals[i];
                    }
                    
                    sol = new Solution(newVals);    
                    sol.setName(vals[0]);
                } else {
                    sol = new Solution(vals);    
                }
                
                
                
                if (set == null) set = new SolutionSet(sol.getDimension());
                set.addSolution(sol);
                
                l = l + 1;
            }

        } catch (NumberFormatException e) {
            throw new RuntimeException("(1x02) Invalid format");
        } catch (IOException e) {
            throw new RuntimeException("(1x01) File could not be read: " + filename);
        }
        return set;
    }
    
}
