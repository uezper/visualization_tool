/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.solution.Solution;
import py.una.pol.setviz.utils.solution.cluster.ShapeCluster;

/**
 *
 * @author uriel
 */
public class ExportSolutionsToCsvUtil1 {
    
    private static final List<String> TITLES;
    
    static {
        
        TITLES = new ArrayList<>();
        TITLES.add("Name");
        TITLES.add("Shape");
        TITLES.add("Normalized values");        
        TITLES.add("Real values");        
        
    }
    
    public static void export(List<Integer> solutionIds, String path) {
        
        List<List<String>> values = new ArrayList<>();
        
        for (Integer id : solutionIds) {
            Solution sol = PlotFrame.solutionDraw.getSolutionSet().getSolutionById(id);
            
            values.add(new ArrayList<>());
            int current = values.size() - 1;
            
            values.get(current).add(sol.getName());
            values.get(current).add(sol.shapeString());
            values.get(current).add(sol.valuesString());
            values.get(current).add(sol.realValuesString());            
        }
        
        ExportToCsvUtil.export(TITLES, values, path);
                
    }
    
    
    
}
