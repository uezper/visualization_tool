/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz;

import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.gui.ProjectFrame;
import py.una.pol.setviz.utils.FileParser;
import py.una.pol.setviz.utils.solution.SolutionSet;

/**
 *
 * @author Uriel Pereira
 */
public class Main {

    

    public static void main(String[] argv) {                
        
        java.awt.EventQueue.invokeLater(new Runnable() {
          public void run() {
              new ProjectFrame().setVisible(true);
          }
        });

        
       //init();
    }
    
    
    
    public static void init(boolean isMinProblem, boolean firstColumnNames, String path) {
        
        
        boolean headlines = false;                
        SolutionSet set = FileParser.parseCSV(path, ",", headlines, firstColumnNames);
        
        set.normalize();
        //set.removeNonDominatedSolutions(isMinProblem);        
                
        java.awt.EventQueue.invokeLater(new Runnable() {
          public void run() {
              new PlotFrame(set, isMinProblem).setVisible(true);
          }
        });
               
    }
    
}
