/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.language;

/**
 *
 * @author acost
 */
public class LanguageKeys {
    public static final String MAIN_WINDOW_TITLE = "MAIN_WINDOW_TITLE";
    public static final String MAIN_WINDOW_MENUBAR_FILE_DESC = "MAIN_WINDOW_MENUBAR_FILE_DESC";
    public static final String PROJECT_VIEW_TITLE = "PROJECT_VIEW_TITLE";
}
