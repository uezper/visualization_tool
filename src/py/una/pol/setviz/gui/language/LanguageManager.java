/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.language;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author Uriel Pereira
 */
public class LanguageManager {
    
    private final ResourceBundle messages;
    
    public LanguageManager(Locale locale) {        
        messages = ResourceBundle.getBundle("locale.lang", locale);
    }
    
    
    public String getMessage(String property) {
        return messages.getString(property);
    }
}
