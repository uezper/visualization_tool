/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.ranking;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.CustomMouseListener;
import py.una.pol.setviz.utils.ImageUtils;
import py.una.pol.setviz.utils.SelectionEntry;
import py.una.pol.setviz.utils.solution.Group;
import py.una.pol.setviz.utils.solution.Solution;
import py.una.pol.setviz.utils.solution.ranking.SolutionRanking;

/**
 *
 * @author acost
 */
public class RankingPanel extends javax.swing.JPanel {

    /**
     * Creates new form RankingPanel
     */
    
    
    List<SolutionRanking.RankLevel> solutionsRank;
    
    HashMap<Integer, JPanel> panelRows;
    MouseListener hoverListener;
    Color lastColor;
    RankType rankType;
    
    RankingByFavouritePanel rankingByFavouritePanel;
    RankingByValuesPanel rankingByValuesPanel;
    RankingByEPreferredPanel rankingByEPreferredPanel;
    
    HashMap<Integer, Color> colors;
    HashMap<Integer, JPanel> panels;
    HashMap<Integer, JCheckBox> checkboxes;    
    Set<Integer> selected;           
    
    boolean updateCheckbox;           
    
    ArrayList<Group> existingGroups;
    
    static final String[] rankTypeStr = {"By E-Preferred", "By Value", "By Favour"};
    
    public RankingPanel() {
        initComponents();
        
        updateCheckbox = true;
        
        solutionsRank = new ArrayList<>();
        
        rankingByFavouritePanel = new RankingByFavouritePanel();              
        rankingByValuesPanel = new RankingByValuesPanel();
        rankingByEPreferredPanel = new RankingByEPreferredPanel();
        
        rankType = RankType.BY_EPREFERRED;
        hoverListener = new CustomMouseListener()
            .setMouseEntered((e) -> {hoverEntered(e);})
            .setMouseExited((e) -> {hoverExited(e);});

        
        this.jlblAddToGroup.setIcon(ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("add-to-group.png"), 
                this.jlblAddToGroup.getPreferredSize().width, 
                this.jlblAddToGroup.getPreferredSize().height));                
        this.jlblAddToGroup.setCursor(new Cursor(Cursor.HAND_CURSOR));
        this.jlblAddToGroup.setToolTipText("Add to group");
        this.jlblAddToGroup.setEnabled(false);
        
        
        this.jcbRankType.removeAllItems();
        for(int i = 0; i < rankTypeStr.length; i++) {
            this.jcbRankType.addItem(rankTypeStr[i]);
        }
        
        
        
        this.jtfGroupName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                check();
            }
            
            private void check() {
                boolean valid = PlotFrame.groupListPanel.checkGroupName(jtfGroupName.getText());
                if (valid) {
                    jtfGroupName.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                } else {
                    jtfGroupName.setBorder(BorderFactory.createLineBorder(Color.RED));
                }                    
                jbtnAdd.setEnabled(valid);
            }
            
        });
        
        
        
        
        JScrollBar vertical = this.jScrollPane1.getVerticalScrollBar();
	InputMap verticalMap = vertical.getInputMap( JComponent.WHEN_IN_FOCUSED_WINDOW );
	verticalMap.put( KeyStroke.getKeyStroke( "DOWN" ), "positive_" );        
	verticalMap.put( KeyStroke.getKeyStroke( "UP" ), "negative_" );
        
        
        Action actionListener1 = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                
                JScrollBar jScrollBar = (JScrollBar) actionEvent.getSource();               
                jScrollBar.setValue(jScrollBar.getValue() + 20);
                
            }
        };
        
        Action actionListener2 = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                
                JScrollBar jScrollBar = (JScrollBar) actionEvent.getSource();               
                jScrollBar.setValue(jScrollBar.getValue() - 20);
                
            }
        };
        
        ActionMap actionMap = new ActionMap();
        actionMap.put("positive_", actionListener1);
        actionMap.put("negative_", actionListener2);
        this.jScrollPane1.getVerticalScrollBar().setActionMap(actionMap);
                
        
                this.jtfGroupName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                check();
            }
            
            private void check() {
                boolean valid = PlotFrame.groupListPanel.checkGroupName(jtfGroupName.getText());
                if (valid) {
                    jtfGroupName.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                } else {
                    jtfGroupName.setBorder(BorderFactory.createLineBorder(Color.RED));
                }                    
                jbtnAdd.setEnabled(valid);
            }
            
        });
        
        
        showByEPreferredPanel();
        updateList();
    }

    
    
    private void hoverEntered(MouseEvent e) {        
        int id = Integer.parseInt(e.getComponent().getName().split("_")[1]);
        
        JPanel panel = panelRows.get(id);
        lastColor = panel.getBackground();
        panel.setBackground(new Color(210, 210, 210));
        PlotFrame.solutionDraw.getHoveredSolutions().add(PlotFrame.solutionDraw.createSelectionEntry(PlotFrame.solutionDraw.getSolutionSet().getSolutionById(id)));
        PlotFrame.redraw();

    }
    
    private void hoverExited(MouseEvent e) {        
        int id = Integer.parseInt(e.getComponent().getName().split("_")[1]);
        
        JPanel panel = panelRows.get(id);
        panel.setBackground(lastColor);
        PlotFrame.solutionDraw.setHoveredSolutions(new ArrayList<>());
        PlotFrame.redraw();

    }
    
    
    
    
    public void updateList() {
    
        
        checkboxes = new HashMap<>();
        panels = new HashMap<>();
        colors = new HashMap<>();
        selected = new HashSet<>();
        
        
        int ammount_solutions = 0;
        
        this.panelRows = new HashMap<>();        
        ArrayList<JPanel> rows = new ArrayList<>();
        
        //selected.addAll(solutionIds);
        
        ArrayList<ArrayList<Integer>> rankedSolutions = new ArrayList<>();
        
        for (int i = 0; i < solutionsRank.size(); i++) {            

            List<Integer> solutions = solutionsRank.get(i).solutionIds;            
            rankedSolutions.add(new ArrayList<>(solutions));
            
            for (int j = 0; j < solutions.size(); j++) {
                
                Solution sol = PlotFrame.solutionDraw.getSolutionSet().getSolutionById(solutions.get(j) );
                JPanel row = createRow(i % 2 == 0, solutionsRank.get(i).level, sol);
                rows.add(row);
                panelRows.put(sol.getId(), row);
                
            }
            
        }
        
        PlotFrame.solutionDraw.setRankedSolutions(rankedSolutions);
        
        updateAddToGroup();
        updateSelected();
        
        this.jpnList.removeAll();
        this.jpnList.revalidate();
        this.jpnList.repaint();
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.jpnList);
        this.jpnList.setLayout(layout);                       
        
        
        GroupLayout.ParallelGroup pG = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
        for (JPanel row : rows) {
            pG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
        }                            
                
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pG)
                .addContainerGap())
        );
        
        GroupLayout.SequentialGroup sG = layout.createSequentialGroup().addContainerGap();
        
        for (JPanel row : rows) {            
            sG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25);
        }
        
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sG)
        );                
        
               
    }
    
    
    private JPanel createRow(boolean color, Integer level, Solution sol) {
        
        JPanel jpnRow = new javax.swing.JPanel();
        JTextField jtfLevel = new JTextField();
        JTextField jtfSolution = new JTextField();
        JTextField jtfShape = new JTextField();        
        JCheckBox jckSelect = new JCheckBox();
        
        jckSelect.setSelected(selected.contains(sol.getId()));        
        jckSelect.setText("");       
        jckSelect.setName("ck_" + sol.getId());
        jckSelect.setPreferredSize(new Dimension(14, 16));
        jckSelect.setOpaque(false);
        jckSelect.addChangeListener((ChangeEvent e) -> {
            if (!updateCheckbox) return;
            
            JCheckBox jc = (JCheckBox)e.getSource();
            int id = Integer.parseInt(jc.getName().split("_")[1]);
            if (jc.isSelected()) {
                selected.add(id);                
                //panels.get(id).setBackground(new Color(210, 210, 210));
            } else {
                selected.remove(id);
                //panels.get(id).setBackground(colors.get(id));
            }
            updateAddToGroup();
            updateSelected();
            
        });
        checkboxes.put(sol.getId(), jckSelect);
        
        
        
        jtfLevel.setText("" + level);                               
        jtfLevel.setEditable(false);
        jtfLevel.setBorder(BorderFactory.createEmptyBorder());
        jtfLevel.setOpaque(false);
        
        jtfSolution.setText(sol.prettyString(false));                               
        jtfSolution.setToolTipText(sol.prettyString(false));
        jtfSolution.setEditable(false);
        jtfSolution.setBorder(BorderFactory.createEmptyBorder());
        jtfSolution.setOpaque(false);
        jtfSolution.setName("sol_" + sol.getId());
        jtfSolution.addMouseListener(hoverListener);
               
        jtfShape.setText(sol.shapeString());
        jtfShape.setToolTipText(sol.shapeString());
        jtfShape.setEditable(false);
        jtfShape.setBorder(BorderFactory.createEmptyBorder());
        jtfShape.setOpaque(false);
        jtfShape.setName("shape_" + sol.getId());
        jtfShape.addMouseListener(hoverListener);
        
        
        javax.swing.GroupLayout jpnRowLayout = new javax.swing.GroupLayout(jpnRow);
        jpnRow.setLayout(jpnRowLayout);
        jpnRowLayout.setHorizontalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnRowLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jckSelect, 18, 18, 18)                                
                .addGap(20, 20, 20)                    
                .addComponent(jtfLevel, 50, 50, 50)
                .addGap(20, 20, 20)                    
                .addComponent(jtfShape, 100, 100, 100)
                .addGap(15, 15, 15)
                .addComponent(jtfSolution, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jpnRowLayout.setVerticalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jckSelect, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 19, 19)
            .addComponent(jtfSolution, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            .addComponent(jtfLevel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            .addComponent(jtfShape, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            
                
                
        );

        
        jpnRow.setName("pnsol_" + sol.getId());        
        jpnRow.addMouseListener(hoverListener);
        if (color) jpnRow.setBackground(new java.awt.Color(230, 230, 230));        
        panels.put(sol.getId(), jpnRow);
        colors.put(sol.getId(), jpnRow.getBackground());
        
        
        return jpnRow;
    }
    
    
    
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDgAddToGroup = new javax.swing.JDialog();
        jrbAddToExistingGroup = new javax.swing.JRadioButton();
        jcbGroupName = new javax.swing.JComboBox<>();
        jrbCreateNew = new javax.swing.JRadioButton();
        jtfGroupName = new javax.swing.JTextField();
        jbtnAdd = new javax.swing.JButton();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jcbRankType = new javax.swing.JComboBox<>();
        jpnRankingTypeParams = new javax.swing.JPanel();
        jchkSelectAll = new javax.swing.JCheckBox();
        jlblAddToGroup = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jpnList = new javax.swing.JPanel();

        jDgAddToGroup.setMinimumSize(new java.awt.Dimension(278, 157));
        jDgAddToGroup.setModal(true);
        jDgAddToGroup.setResizable(false);

        jrbAddToExistingGroup.setSelected(true);
        jrbAddToExistingGroup.setText("Existing:");
        jrbAddToExistingGroup.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jrbAddToExistingGroup.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jrbAddToExistingGroupMouseClicked(evt);
            }
        });
        jrbAddToExistingGroup.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jrbAddToExistingGroupPropertyChange(evt);
            }
        });

        jcbGroupName.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jcbGroupName.setPreferredSize(new java.awt.Dimension(140, 20));

        jrbCreateNew.setText("Create new:");
        jrbCreateNew.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jrbCreateNew.setPreferredSize(new java.awt.Dimension(97, 23));
        jrbCreateNew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jrbCreateNewMouseClicked(evt);
            }
        });
        jrbCreateNew.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jrbCreateNewPropertyChange(evt);
            }
        });

        jtfGroupName.setText("jTextField1");
        jtfGroupName.setPreferredSize(new java.awt.Dimension(140, 20));
        jtfGroupName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtfGroupNameKeyReleased(evt);
            }
        });

        jbtnAdd.setText("Add");
        jbtnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnAddActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDgAddToGroupLayout = new javax.swing.GroupLayout(jDgAddToGroup.getContentPane());
        jDgAddToGroup.getContentPane().setLayout(jDgAddToGroupLayout);
        jDgAddToGroupLayout.setHorizontalGroup(
            jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgAddToGroupLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jDgAddToGroupLayout.createSequentialGroup()
                        .addComponent(jbtnAdd)
                        .addGap(12, 12, 12))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDgAddToGroupLayout.createSequentialGroup()
                        .addGroup(jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDgAddToGroupLayout.createSequentialGroup()
                                .addComponent(jrbAddToExistingGroup, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jcbGroupName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDgAddToGroupLayout.createSequentialGroup()
                                .addComponent(jrbCreateNew, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jtfGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        jDgAddToGroupLayout.setVerticalGroup(
            jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgAddToGroupLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jrbAddToExistingGroup)
                    .addComponent(jcbGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jrbCreateNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jbtnAdd)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        buttonGroup1.add(this.jrbAddToExistingGroup);
        buttonGroup1.add(this.jrbCreateNew);

        setMinimumSize(new java.awt.Dimension(383, 307));

        jLabel1.setText("Ranking type:");

        jcbRankType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jcbRankType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcbRankTypeItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jpnRankingTypeParamsLayout = new javax.swing.GroupLayout(jpnRankingTypeParams);
        jpnRankingTypeParams.setLayout(jpnRankingTypeParamsLayout);
        jpnRankingTypeParamsLayout.setHorizontalGroup(
            jpnRankingTypeParamsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jpnRankingTypeParamsLayout.setVerticalGroup(
            jpnRankingTypeParamsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 51, Short.MAX_VALUE)
        );

        jchkSelectAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jchkSelectAllActionPerformed(evt);
            }
        });

        jlblAddToGroup.setPreferredSize(new java.awt.Dimension(16, 16));
        jlblAddToGroup.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlblAddToGroupMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jpnListLayout = new javax.swing.GroupLayout(jpnList);
        jpnList.setLayout(jpnListLayout);
        jpnListLayout.setHorizontalGroup(
            jpnListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 361, Short.MAX_VALUE)
        );
        jpnListLayout.setVerticalGroup(
            jpnListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 170, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(jpnList);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpnRankingTypeParams, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jcbRankType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(45, 45, 45)
                                .addComponent(jchkSelectAll, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jlblAddToGroup, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 183, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jcbRankType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jpnRankingTypeParams, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jchkSelectAll, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlblAddToGroup, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jcbRankTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcbRankTypeItemStateChanged
        
        String type = (String)this.jcbRankType.getSelectedItem();
        if (type == null) return;
        if (type.equalsIgnoreCase(rankTypeStr[RankType.BY_VALUE.ordinal()])) {                                    
            rankType = RankType.BY_VALUE;
            
            showByValuesPanel();
            
        } else if (type.equalsIgnoreCase(rankTypeStr[RankType.BY_EPREFERRED.ordinal()])) {
            rankType = RankType.BY_EPREFERRED;
            
            showByEPreferredPanel();
        } else if (type.equalsIgnoreCase(rankTypeStr[RankType.BY_FAVOURITE.ordinal()])) {
            rankType = RankType.BY_FAVOURITE;
            
            showByFavouritePanel();
        }
        
    }//GEN-LAST:event_jcbRankTypeItemStateChanged

    private void jlblAddToGroupMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlblAddToGroupMouseClicked
        if (!this.jlblAddToGroup.isEnabled()) return;
        
        initializeAddToGroupDialog();
        this.jDgAddToGroup.setVisible(true);
        
    }//GEN-LAST:event_jlblAddToGroupMouseClicked

    private void jchkSelectAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jchkSelectAllActionPerformed
        
        this.updateCheckbox = false;
                
        boolean isSelected = this.jchkSelectAll.isSelected();
        for(Integer id : checkboxes.keySet()) {

            JCheckBox chk = checkboxes.get(id);
            chk.setSelected(isSelected);
            if (isSelected) selected.add(id);
            else selected.remove(id);
        }
        
        this.updateCheckbox = true;
        updateAddToGroup();
        updateSelected();
    }//GEN-LAST:event_jchkSelectAllActionPerformed

    
    
    private void addToExistingGroup() {
        
        
        
        if (this.jrbCreateNew.isSelected()) {
            Group newGroup = new Group();
            newGroup.setGroupName(this.jtfGroupName.getText());
            newGroup.getIds().addAll(selected);
            PlotFrame.groups.put(newGroup.getGroupId(), newGroup);
            PlotFrame.groupListPanel.updateList();
            this.jDgAddToGroup.dispose();
        } else {
            Group group = existingGroups.get(this.jcbGroupName.getSelectedIndex());
            group.getIds().addAll(selected);
            PlotFrame.groupListPanel.updateList();
            this.jDgAddToGroup.dispose();
        }
    }
    
    
    
    private void changeAddToGroupSelection() {
        
        if (this.jrbAddToExistingGroup.isSelected()) {
            this.jcbGroupName.setEnabled(true);
            this.jtfGroupName.setEnabled(false);
            this.jbtnAdd.setEnabled(true);            
        } else {
           this.jtfGroupName.setEnabled(true);
           this.jcbGroupName.setEnabled(false);
           this.jtfGroupName.requestFocus();
        }
    }
    
    
    private void initializeAddToGroupDialog() {
        
        existingGroups = new ArrayList<>();
        
        this.jtfGroupName.setText("");
        this.jcbGroupName.removeAllItems();
        
        if (PlotFrame.groups.size() > 0) {           
            
            for(Group group : PlotFrame.groups.values()) {
                existingGroups.add(group);
                this.jcbGroupName.addItem(group.getGroupName());
            }
                       
            
            this.jcbGroupName.setSelectedIndex(0);
            this.jcbGroupName.setEnabled(true);            
            this.jrbAddToExistingGroup.setEnabled(true);            
            this.jbtnAdd.setEnabled(true);
            this.jrbAddToExistingGroup.setSelected(true);              
        } else {
            this.jcbGroupName.setEnabled(false);
            this.jrbAddToExistingGroup.setEnabled(false);
            this.jrbCreateNew.setSelected(true);
            this.jtfGroupName.setEnabled(true);
            this.jtfGroupName.requestFocus();
            this.jbtnAdd.setEnabled(false);
        }
        
        
    }

    
    
    private void jrbAddToExistingGroupMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jrbAddToExistingGroupMouseClicked
        changeAddToGroupSelection();
    }//GEN-LAST:event_jrbAddToExistingGroupMouseClicked

    private void jrbAddToExistingGroupPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jrbAddToExistingGroupPropertyChange
        changeAddToGroupSelection();
    }//GEN-LAST:event_jrbAddToExistingGroupPropertyChange

    private void jrbCreateNewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jrbCreateNewMouseClicked
        changeAddToGroupSelection();
    }//GEN-LAST:event_jrbCreateNewMouseClicked

    private void jrbCreateNewPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jrbCreateNewPropertyChange
        changeAddToGroupSelection();
    }//GEN-LAST:event_jrbCreateNewPropertyChange

    private void jtfGroupNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtfGroupNameKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (this.jbtnAdd.isEnabled()) addToExistingGroup();
        }
    }//GEN-LAST:event_jtfGroupNameKeyReleased

    private void jbtnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnAddActionPerformed
        addToExistingGroup();
    }//GEN-LAST:event_jbtnAddActionPerformed

    
    private void updateAddToGroup() {
        this.jlblAddToGroup.setEnabled(selected.size() > 0);
    }
    
    
    
    private void addRankTypePanel(JPanel clusterPanel) {
        this.jpnRankingTypeParams.removeAll();
        this.jpnRankingTypeParams.revalidate();
        this.jpnRankingTypeParams.repaint();
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.jpnRankingTypeParams);
        this.jpnRankingTypeParams.setLayout(layout);                       
        
        
        GroupLayout.ParallelGroup pG = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
        pG.addComponent(clusterPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);                
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(pG)
        ));
        
        GroupLayout.SequentialGroup sG = layout.createSequentialGroup();
        sG.addComponent(clusterPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
        
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sG)
        );
        
        rankingByValuesPanel.setSize(this.jpnRankingTypeParams.getSize());
        
    }
    
    public void resetRank() {
        this.solutionsRank = new ArrayList<>();
    }
    
    
    private void showByFavouritePanel() {        
                        
        addRankTypePanel(rankingByFavouritePanel);                        
    }
    
    private void showByValuesPanel() {        
                        
        addRankTypePanel(rankingByValuesPanel);                        
    }
    
    private void showByEPreferredPanel() {        
                        
        addRankTypePanel(rankingByEPreferredPanel);                        
    }
    
    

    private void updateSelected() {        
        HashMap<Integer, SelectionEntry> selectionEntries = new HashMap<>();
        for(Integer id : selected) {
            
            Solution sol = PlotFrame.solutionDraw.getSolutionSet().getSolutionById(id);
            selectionEntries.put(sol.getId(), PlotFrame.solutionDraw.createSelectionEntry(sol));
            
        }
        
        PlotFrame.solutionDraw.setClickedSolutions(selectionEntries);
        PlotFrame.selectionsPanel.setClickedSolutions(selectionEntries);
        PlotFrame.selectionsPanel.updateList();
        PlotFrame.redraw();
    }
    
    
    
    public enum RankType {
        BY_EPREFERRED,
        BY_VALUE,
        BY_FAVOURITE
    }
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JDialog jDgAddToGroup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton jbtnAdd;
    private javax.swing.JComboBox<String> jcbGroupName;
    private javax.swing.JComboBox<String> jcbRankType;
    private javax.swing.JCheckBox jchkSelectAll;
    private javax.swing.JLabel jlblAddToGroup;
    private javax.swing.JPanel jpnList;
    private javax.swing.JPanel jpnRankingTypeParams;
    private javax.swing.JRadioButton jrbAddToExistingGroup;
    private javax.swing.JRadioButton jrbCreateNew;
    private javax.swing.JTextField jtfGroupName;
    // End of variables declaration//GEN-END:variables
}
