/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.groups;


import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.gui.filters.FilterListPanel;
import py.una.pol.setviz.utils.CustomMouseListener;
import py.una.pol.setviz.utils.ImageUtils;
import py.una.pol.setviz.utils.filters.FilterEntry;
import py.una.pol.setviz.utils.solution.Group;

/**
 *
 * @author acost
 */
public class GroupListPanel extends javax.swing.JPanel {

    /**
     * Creates new form SelectionsPanel
     */
    
    ImageIcon removeIcon;    
    ImageIcon drawIcon;    
    ImageIcon copyIcon;    
    
    HashMap<Integer, JCheckBox> checkboxes;
    Set<Integer> selected;
    
    ArrayList<Group> mergeResultingGroups;
    ArrayList<Group> mergeGroups;
   
    Group copyGroup;
    boolean updateCheckbox;
    
    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    public GroupListPanel() {
        initComponents();
        
        updateCheckbox = true;
        
        selected = new HashSet<>();
        
        removeIcon = ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("delete-icon.png"), 
                this.jlblRemoveSelected.getPreferredSize().width, 
                this.jlblRemoveSelected.getPreferredSize().height);
        
        drawIcon = ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("draw-icon.png"), 
                this.jlblRemoveSelected.getPreferredSize().width, 
                this.jlblRemoveSelected.getPreferredSize().height);
        
        copyIcon = ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("copy-icon.png"), 
                this.jlblRemoveSelected.getPreferredSize().width, 
                this.jlblRemoveSelected.getPreferredSize().height);
        
        
        
        this.jlblRemoveSelected.setIcon(removeIcon);                
        this.jlblRemoveSelected.setToolTipText("Remove selected");
        makeButtonStyle(jlblRemoveSelected);
        
        
        this.jlblNew.setIcon(ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("new-icon.png"), 
                this.jlblNew.getPreferredSize().width, 
                this.jlblNew.getPreferredSize().height));
        this.jlblNew.setToolTipText("Create new group");
        makeButtonStyle(jlblNew);
        
        
        this.jlblMerge.setIcon(ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("merge-icon.png"), 
                18, 
                18));
        this.jlblMerge.setToolTipText("Merge groups");
        makeButtonStyle(jlblMerge);
        
        
        JScrollBar vertical = this.jScrollPane1.getVerticalScrollBar();
	InputMap verticalMap = vertical.getInputMap( JComponent.WHEN_IN_FOCUSED_WINDOW );
	verticalMap.put( KeyStroke.getKeyStroke( "DOWN" ), "positive_" );        
	verticalMap.put( KeyStroke.getKeyStroke( "UP" ), "negative_" );
        
        
        Action actionListener1 = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                
                JScrollBar jScrollBar = (JScrollBar) actionEvent.getSource();               
                jScrollBar.setValue(jScrollBar.getValue() + 20);
                
            }
        };
        
        Action actionListener2 = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                
                JScrollBar jScrollBar = (JScrollBar) actionEvent.getSource();               
                jScrollBar.setValue(jScrollBar.getValue() - 20);
                
            }
        };
        
        ActionMap actionMap = new ActionMap();
        actionMap.put("positive_", actionListener1);
        actionMap.put("negative_", actionListener2);
        this.jScrollPane1.getVerticalScrollBar().setActionMap(actionMap);
        
        this.jtfMergeGroupName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                check();
            }
            
            private void check() {
                boolean valid = checkGroupName(jtfMergeGroupName.getText());
                if (valid) {
                    jtfMergeGroupName.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                } else {
                    jtfMergeGroupName.setBorder(BorderFactory.createLineBorder(Color.RED));
                }                    
                jbtnMergeComplete.setEnabled(valid);
            }
        });
        
        
        this.jtfCreateGroupName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                check();
            }
            
            private void check() {
                boolean valid = checkGroupName(jtfCreateGroupName.getText());
                if (valid) {
                    jtfCreateGroupName.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                } else {
                    jtfCreateGroupName.setBorder(BorderFactory.createLineBorder(Color.RED));
                }                    
                jbtnCreate.setEnabled(valid);
            }
        });
        
        updateList();
    }


    public boolean checkGroupName(String groupName) {
        if (groupName.trim().isEmpty()) return false;
        for(Group group : PlotFrame.groups.values()) {
            if (group.getGroupName().equalsIgnoreCase(groupName.trim())) return false;
        }
        return true;
    }

    
    private void makeButtonStyle(JLabel component) {
        component.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        component.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        component.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
        component.setBackground(new Color(210,210,210));
        component.setCursor(new Cursor(Cursor.HAND_CURSOR));
        component.setPreferredSize(new Dimension(16,16));
        component.setOpaque(true);        
    }            
    
    private void checkSelectedButtons() {        
       this.jlblMerge.setEnabled(selected.size() > 1);
       this.jlblRemoveSelected.setEnabled(selected.size() > 0);
    }
    
    public final void updateList() {
            
        checkSelectedButtons();        
        checkboxes = new HashMap<>();
        
        ArrayList<JPanel> rows = new ArrayList<>();
        
        ArrayList<Group> groups = new ArrayList<>(PlotFrame.groups.values());
        groups.sort(new Comparator<Group>() {
            @Override
            public int compare(Group o1, Group o2) {
                return o1.getGroupName().compareTo(o2.getGroupName());
            }
        });
        
        int index = 0;
        for (Group group : groups) {                                    
            JPanel row = createRow(index++ % 2 == 0, group);
            rows.add(row);
        }
        
        this.jpnList.removeAll();
        this.jpnList.revalidate();
        this.jpnList.repaint();
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.jpnList);
        this.jpnList.setLayout(layout);                       
        
        
        GroupLayout.ParallelGroup pG = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
        for (JPanel row : rows) {
            pG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
        }                            
                
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pG)
                .addContainerGap())
        );
        
        GroupLayout.SequentialGroup sG = layout.createSequentialGroup().addContainerGap();
        
        for (JPanel row : rows) {            
            sG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25);
        }
        
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sG)
        );
              
    }
    
    
    
    
    
    private JPanel createRow(boolean color, Group group) {
        
        JPanel jpnRow = new javax.swing.JPanel();

        JCheckBox jckSelect = new JCheckBox();
        JTextField jtfGroupName = new JTextField();
        JTextField jtfSolutions = new JTextField();
        JLabel jlblCopy = new javax.swing.JLabel();       
        JLabel jlblDraw = new javax.swing.JLabel();       
        JLabel jlblRemove = new javax.swing.JLabel();       


        jckSelect.setSelected(selected.contains(group.getGroupId()));        
        jckSelect.setText("");
        jckSelect.setName("ck_" + group.getGroupId());
        jckSelect.setPreferredSize(new Dimension(14, 16));
        jckSelect.setOpaque(false);
        jckSelect.addChangeListener((ChangeEvent e) -> {
            if (!updateCheckbox) return;
            
            JCheckBox jc = (JCheckBox)e.getSource();
            int id = Integer.parseInt(jc.getName().split("_")[1]);
            if (jc.isSelected()) {
                selected.add(id);
            } else {
                selected.remove(id);
            }
            
            checkSelectedButtons();
        });
        checkboxes.put(group.getGroupId(), jckSelect);
        
        jtfGroupName.setText(group.getGroupName());
        jtfGroupName.setToolTipText(group.getGroupName());
        jtfGroupName.setEditable(false);
        jtfGroupName.setBorder(BorderFactory.createEmptyBorder());
        jtfGroupName.setOpaque(false);
        
        jtfSolutions.setText(""+group.getSize());
        jtfSolutions.setToolTipText(""+group.getSize());
        jtfSolutions.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jtfSolutions.setEditable(false);
        jtfSolutions.setBorder(BorderFactory.createEmptyBorder());
        jtfSolutions.setOpaque(false);
        
        jlblCopy.setPreferredSize(new Dimension(14, 14));
        jlblCopy.setIcon(copyIcon);        
        jlblCopy.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jlblCopy.setToolTipText("Copy");
        jlblCopy.setName("copy_" + group.getGroupId());
        jlblCopy.addMouseListener(
            new CustomMouseListener().setMouseClicked((MouseEvent e) -> {
                int id = Integer.parseInt(e.getComponent().getName().split("_")[1]);
                
               copyGroup = PlotFrame.groups.get(id);
               this.jtfCreateGroupName.setText("Copy of " + copyGroup.getGroupName());
               this.jbtnCreate.setEnabled(true);
               this.jDgCreateGroup.setVisible(true);
            })
        );
        
        
        jlblDraw.setPreferredSize(new Dimension(14, 14));
        jlblDraw.setIcon(drawIcon);        
        jlblDraw.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jlblDraw.setToolTipText("Draw");
        jlblDraw.setName("draw_" + group.getGroupId());
        jlblDraw.addMouseListener(
            new CustomMouseListener().setMouseClicked((MouseEvent e) -> {
                int id = Integer.parseInt(e.getComponent().getName().split("_")[1]);
                
                FilterEntry filterEntry = new FilterEntry();
                filterEntry.setFilterType(FilterEntry.FilterType.GROUP);
                filterEntry.setFilterGroupCondition(FilterEntry.FilterGroupCondition.IN);
                filterEntry.setGroupId(id);
                
                FilterListPanel.filterEntries = new ArrayList<>();
                FilterListPanel.filterEntries.add(filterEntry);
                PlotFrame.filterListPanel.setAutomaticUpdate(true);
                PlotFrame.filterListPanel.setApplyFilters(true);
                PlotFrame.filterListPanel.updateList();
                
            })
        );
        
                
        jlblRemove.setPreferredSize(new Dimension(14, 14));
        jlblRemove.setIcon(removeIcon);        
        jlblRemove.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jlblRemove.setToolTipText("Remove");
        jlblRemove.setName("rem_" + group.getGroupId());
        jlblRemove.addMouseListener(
            new CustomMouseListener().setMouseClicked((MouseEvent e) -> {
                int id = Integer.parseInt(e.getComponent().getName().split("_")[1]);
                Group group1 = PlotFrame.groups.get(id);
                if (group1 != null) {
                    PlotFrame.groups.remove(id);
                    PlotFrame.filterListPanel.updateList(true);
                    updateList();
                }
            })
        );
        
        
        javax.swing.GroupLayout jpnRowLayout = new javax.swing.GroupLayout(jpnRow);
        jpnRow.setLayout(jpnRowLayout);
        jpnRowLayout.setHorizontalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnRowLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jckSelect, 18, 18, 18)                
                .addGap(20,20,20)
                .addComponent(jlblCopy, 20, 20, 20)
                .addGap(5,5,5)
                .addComponent(jlblDraw, 20, 20, 20)
                .addGap(5,5,5)
                .addComponent(jlblRemove, 20, 20, 20)
                .addGap(20, 20, 20)
                .addComponent(jtfGroupName, 177, 177, 177)
                .addGap(18, 18, 18)
                .addComponent(jtfSolutions, 65, 65, 65)                 
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)                
                
                .addContainerGap())
        );
        jpnRowLayout.setVerticalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jckSelect, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 19, 19)
            .addComponent(jtfGroupName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            .addComponent(jtfSolutions, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            .addComponent(jlblCopy, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)                    
            .addComponent(jlblDraw, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)                    
            .addComponent(jlblRemove, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
                            
        );
               
        if (color) jpnRow.setBackground(new java.awt.Color(230, 230, 230));        

        return jpnRow;
    }
    
    
    
    
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDgCreateGroup = new javax.swing.JDialog();
        jLabel1 = new javax.swing.JLabel();
        jtfCreateGroupName = new javax.swing.JTextField();
        jbtnCreate = new javax.swing.JButton();
        jDgMergeGroups = new javax.swing.JDialog();
        jLabel2 = new javax.swing.JLabel();
        jtfMergeGroupsSelected = new javax.swing.JTextField();
        jlblMergeGroupA = new javax.swing.JLabel();
        jtfMergeGroupA = new javax.swing.JTextField();
        jlblMergeGroupB = new javax.swing.JLabel();
        jtfMergeGroupB = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jcbMergeType = new javax.swing.JComboBox<>();
        jckMergePreserveGroups = new javax.swing.JCheckBox();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        jtfMergeGroupName = new javax.swing.JTextField();
        jbtnMergeComplete = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jtfMergeSolutionsResult = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jlblNameTitle = new javax.swing.JLabel();
        jlblSolutionsTitle = new javax.swing.JLabel();
        jcbSelectAll = new javax.swing.JCheckBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        jpnList = new javax.swing.JPanel();
        jlblNew = new javax.swing.JLabel();
        jlblRemoveSelected = new javax.swing.JLabel();
        jlblMerge = new javax.swing.JLabel();

        jDgCreateGroup.setMinimumSize(new java.awt.Dimension(278, 100));
        jDgCreateGroup.setModal(true);
        jDgCreateGroup.setResizable(false);
        jDgCreateGroup.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jDgCreateGroupFocusGained(evt);
            }
        });

        jLabel1.setText("Group Name:");

        jtfCreateGroupName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtfCreateGroupNameKeyReleased(evt);
            }
        });

        jbtnCreate.setText("Create");
        jbtnCreate.setEnabled(false);
        jbtnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnCreateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDgCreateGroupLayout = new javax.swing.GroupLayout(jDgCreateGroup.getContentPane());
        jDgCreateGroup.getContentPane().setLayout(jDgCreateGroupLayout);
        jDgCreateGroupLayout.setHorizontalGroup(
            jDgCreateGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgCreateGroupLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDgCreateGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDgCreateGroupLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jtfCreateGroupName, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDgCreateGroupLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jbtnCreate)))
                .addContainerGap())
        );
        jDgCreateGroupLayout.setVerticalGroup(
            jDgCreateGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgCreateGroupLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDgCreateGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jtfCreateGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtnCreate)
                .addContainerGap())
        );

        jDgMergeGroups.setMinimumSize(new java.awt.Dimension(270, 300));
        jDgMergeGroups.setModal(true);
        jDgMergeGroups.setResizable(false);
        jDgMergeGroups.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                jDgMergeGroupsComponentShown(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Groups:");

        jtfMergeGroupsSelected.setEditable(false);
        jtfMergeGroupsSelected.setText("jTextField1");
        jtfMergeGroupsSelected.setBorder(null);

        jlblMergeGroupA.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jlblMergeGroupA.setText("Group A:");

        jtfMergeGroupA.setEditable(false);
        jtfMergeGroupA.setText("jTextField1");
        jtfMergeGroupA.setBorder(null);

        jlblMergeGroupB.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jlblMergeGroupB.setText("Group B:");

        jtfMergeGroupB.setEditable(false);
        jtfMergeGroupB.setText("jTextField1");
        jtfMergeGroupB.setBorder(null);

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Type:");

        jcbMergeType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jcbMergeType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcbMergeTypeItemStateChanged(evt);
            }
        });

        jckMergePreserveGroups.setText("Preserve groups");

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Group Name:");

        jbtnMergeComplete.setText("Merge Groups");
        jbtnMergeComplete.setEnabled(false);
        jbtnMergeComplete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnMergeCompleteActionPerformed(evt);
            }
        });

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Solutions:");

        jtfMergeSolutionsResult.setEditable(false);
        jtfMergeSolutionsResult.setText("jTextField1");
        jtfMergeSolutionsResult.setBorder(null);

        javax.swing.GroupLayout jDgMergeGroupsLayout = new javax.swing.GroupLayout(jDgMergeGroups.getContentPane());
        jDgMergeGroups.getContentPane().setLayout(jDgMergeGroupsLayout);
        jDgMergeGroupsLayout.setHorizontalGroup(
            jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgMergeGroupsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(jDgMergeGroupsLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDgMergeGroupsLayout.createSequentialGroup()
                                .addGroup(jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jtfMergeGroupName)
                                    .addComponent(jcbMergeType, 0, 130, Short.MAX_VALUE)
                                    .addComponent(jtfMergeSolutionsResult)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDgMergeGroupsLayout.createSequentialGroup()
                                .addGroup(jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlblMergeGroupB, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jlblMergeGroupA, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jtfMergeGroupA, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                                    .addComponent(jtfMergeGroupsSelected)
                                    .addComponent(jtfMergeGroupB))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDgMergeGroupsLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jckMergePreserveGroups, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jbtnMergeComplete, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        jDgMergeGroupsLayout.setVerticalGroup(
            jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgMergeGroupsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jtfMergeGroupsSelected, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfMergeGroupA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlblMergeGroupA))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfMergeGroupB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlblMergeGroupB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jtfMergeGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jcbMergeType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDgMergeGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jtfMergeSolutionsResult, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jckMergePreserveGroups)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jbtnMergeComplete)
                .addContainerGap(77, Short.MAX_VALUE))
        );

        setPreferredSize(new java.awt.Dimension(840, 194));

        jlblNameTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlblNameTitle.setText("Name");
        jlblNameTitle.setPreferredSize(new java.awt.Dimension(177, 16));

        jlblSolutionsTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlblSolutionsTitle.setText("Solutions");
        jlblSolutionsTitle.setMaximumSize(new java.awt.Dimension(65, 16));
        jlblSolutionsTitle.setMinimumSize(new java.awt.Dimension(65, 16));
        jlblSolutionsTitle.setPreferredSize(new java.awt.Dimension(65, 16));

        jcbSelectAll.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jcbSelectAllMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jcbSelectAll)
                .addGap(110, 110, 110)
                .addComponent(jlblNameTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jlblSolutionsTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jlblNameTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jcbSelectAll)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jlblSolutionsTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jpnListLayout = new javax.swing.GroupLayout(jpnList);
        jpnList.setLayout(jpnListLayout);
        jpnListLayout.setHorizontalGroup(
            jpnListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 818, Short.MAX_VALUE)
        );
        jpnListLayout.setVerticalGroup(
            jpnListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(jpnList);

        jlblNew.setPreferredSize(new java.awt.Dimension(14, 14));
        jlblNew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlblNewMouseClicked(evt);
            }
        });

        jlblRemoveSelected.setPreferredSize(new java.awt.Dimension(14, 14));
        jlblRemoveSelected.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlblRemoveSelectedMouseClicked(evt);
            }
        });

        jlblMerge.setEnabled(false);
        jlblMerge.setPreferredSize(new java.awt.Dimension(14, 14));
        jlblMerge.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlblMergeMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jlblNew, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlblMerge, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlblRemoveSelected, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jlblNew, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                    .addComponent(jlblMerge, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                    .addComponent(jlblRemoveSelected, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jcbSelectAllMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jcbSelectAllMouseClicked
        
        updateCheckbox = false;
        JCheckBox source = (JCheckBox)evt.getSource();
        for(Integer id : checkboxes.keySet()) {
            JCheckBox ck = checkboxes.get(id);
            ck.setSelected(source.isSelected());
            if (source.isSelected()) selected.add(id);
            else selected.remove(id);
        }
        
        checkSelectedButtons();
        updateCheckbox = true;
                
    }//GEN-LAST:event_jcbSelectAllMouseClicked

    private void jlblRemoveSelectedMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlblRemoveSelectedMouseClicked
        ArrayList<Integer> remove = new ArrayList<>();
        for (Integer id : selected) {
            remove.add(id);
        }
        for (Integer id : remove) {
            selected.remove(id);
            PlotFrame.groups.remove(id);
        }

        PlotFrame.filterListPanel.setAutomaticUpdate(true);
        PlotFrame.filterListPanel.setApplyFilters(true);
        PlotFrame.filterListPanel.updateList(true);
        updateList();
    }//GEN-LAST:event_jlblRemoveSelectedMouseClicked

    private void jlblNewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlblNewMouseClicked
        this.jtfCreateGroupName.setText("");
        this.jbtnCreate.setEnabled(false);
        this.jDgCreateGroup.setVisible(true);
    }//GEN-LAST:event_jlblNewMouseClicked

    private void createGroup() {
        Group newGroup = new Group();
        if (copyGroup != null) {
            newGroup = newGroup.getUnionWith(copyGroup);            
            copyGroup = null;
        } 
        newGroup.setGroupName(this.jtfCreateGroupName.getText().trim());
        
        PlotFrame.groups.put(newGroup.getGroupId(), newGroup);
        updateList();
        this.jDgCreateGroup.dispose();
    }
    
    private void initializeMergeDialog() {
        
        
        
        mergeGroups = new ArrayList<>();        
        mergeResultingGroups = new ArrayList<>();
                
        
        Iterator<Integer> iterator = selected.iterator();
        while(iterator.hasNext()) {
            mergeGroups.add(PlotFrame.groups.get(iterator.next()));
        }        
        
        boolean difference = selected.size() == 2;
        this.jtfMergeGroupName.setText("");
        this.jlblMergeGroupA.setVisible(difference);
        this.jlblMergeGroupB.setVisible(difference);
        this.jtfMergeGroupA.setVisible(difference);
        this.jtfMergeGroupB.setVisible(difference);
        if (difference) {
            this.jtfMergeGroupA.setText(mergeGroups.get(0).getGroupName());
            this.jtfMergeGroupB.setText(mergeGroups.get(1).getGroupName());
        }
        
        this.jtfMergeGroupsSelected.setText("" + selected.size());        
        
        Group intersection = new Group();
        Group union = new Group();        
        
        union.setIds(mergeGroups.get(0).getIds());
        intersection.setIds(mergeGroups.get(0).getIds());
        for(int i = 1; i < mergeGroups.size(); i++) {
            intersection = intersection.getIntersectionWith(mergeGroups.get(i));
            union = union.getUnionWith(mergeGroups.get(i));
        }
        
        
        mergeResultingGroups.add(intersection);
        mergeResultingGroups.add(union);
        
        if (difference) {
            mergeResultingGroups.add(mergeGroups.get(0).getDifferenceWith(mergeGroups.get(1)));
            mergeResultingGroups.add(mergeGroups.get(1).getDifferenceWith(mergeGroups.get(0)));
        }
        
        this.jcbMergeType.removeAllItems();
        this.jcbMergeType.addItem("Intersection");
        this.jcbMergeType.addItem("Union");
        if (difference) {
            this.jcbMergeType.addItem("A - B");
            this.jcbMergeType.addItem("B - A");
        }
                
        this.jcbMergeType.setSelectedIndex(0);
        this.jtfMergeSolutionsResult.setText("" + intersection.getSize());
        this.jckMergePreserveGroups.setSelected(true);        
        
    }    
    
    private void jlblMergeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlblMergeMouseClicked
        if (!this.jlblMerge.isEnabled()) return;
        
        initializeMergeDialog();
        this.jDgMergeGroups.setVisible(true);
    }//GEN-LAST:event_jlblMergeMouseClicked

    private void jcbMergeTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcbMergeTypeItemStateChanged
        
        if (this.jcbMergeType.getSelectedIndex() >= 0)
            this.jtfMergeSolutionsResult.setText("" + mergeResultingGroups.get(this.jcbMergeType.getSelectedIndex()).getSize());
    }//GEN-LAST:event_jcbMergeTypeItemStateChanged

    private void jDgMergeGroupsComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jDgMergeGroupsComponentShown
        this.jtfMergeGroupName.requestFocus();
    }//GEN-LAST:event_jDgMergeGroupsComponentShown

    private void jDgCreateGroupFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jDgCreateGroupFocusGained
        this.jtfMergeGroupName.requestFocus();
    }//GEN-LAST:event_jDgCreateGroupFocusGained

    private void jbtnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnCreateActionPerformed
        createGroup();
    }//GEN-LAST:event_jbtnCreateActionPerformed

    private void jbtnMergeCompleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnMergeCompleteActionPerformed
        Group newGroup = mergeResultingGroups.get(this.jcbMergeType.getSelectedIndex());
        newGroup.setGroupName(this.jtfMergeGroupName.getText());
        
        if (!this.jckMergePreserveGroups.isSelected()) {
            for(Group group : mergeGroups) {
                PlotFrame.groups.remove(group.getGroupId());
                selected.remove(group.getGroupId());
            }
        }
        
        PlotFrame.groups.put(newGroup.getGroupId(), newGroup);
        updateList();
        this.jDgMergeGroups.dispose();
    }//GEN-LAST:event_jbtnMergeCompleteActionPerformed

    private void jtfCreateGroupNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtfCreateGroupNameKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (this.jbtnCreate.isEnabled()) {
                createGroup();
            }
        }
    }//GEN-LAST:event_jtfCreateGroupNameKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDialog jDgCreateGroup;
    private javax.swing.JDialog jDgMergeGroups;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JButton jbtnCreate;
    private javax.swing.JButton jbtnMergeComplete;
    private javax.swing.JComboBox<String> jcbMergeType;
    private javax.swing.JCheckBox jcbSelectAll;
    private javax.swing.JCheckBox jckMergePreserveGroups;
    private javax.swing.JLabel jlblMerge;
    private javax.swing.JLabel jlblMergeGroupA;
    private javax.swing.JLabel jlblMergeGroupB;
    private javax.swing.JLabel jlblNameTitle;
    private javax.swing.JLabel jlblNew;
    private javax.swing.JLabel jlblRemoveSelected;
    private javax.swing.JLabel jlblSolutionsTitle;
    private javax.swing.JPanel jpnList;
    private javax.swing.JTextField jtfCreateGroupName;
    private javax.swing.JTextField jtfMergeGroupA;
    private javax.swing.JTextField jtfMergeGroupB;
    private javax.swing.JTextField jtfMergeGroupName;
    private javax.swing.JTextField jtfMergeGroupsSelected;
    private javax.swing.JTextField jtfMergeSolutionsResult;
    // End of variables declaration//GEN-END:variables
}

