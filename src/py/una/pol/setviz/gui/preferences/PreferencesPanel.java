/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.preferences;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.CustomMouseListener;
import py.una.pol.setviz.utils.ImageUtils;
import py.una.pol.setviz.Settings;
import py.una.pol.setviz.gui.utils.SelectFolderFileUtil;
import py.una.pol.setviz.utils.solution.preferences.SolutionPreference;

/**
 *
 * @author acost
 */
public class PreferencesPanel extends javax.swing.JPanel {

    /**
     * Creates new form PreferencesPanel
     */
  
    ArrayList<String> objectiveNames;
    ArrayList<String> defaultObjetiveNames;
    Set<Integer> shownObjectives;
    Set<Integer> selected;
    ArrayList<Integer> customOrder;
    ArrayList<Integer> customOrderTemp;
    HashMap<Integer, JTextField> textFieldsObjectiveNames;
    HashMap<Integer, JCheckBox> checkboxesObjectiveNames;
    List<PreferencesOrderGuiHelper.Row> objectivePreferences;
    SolutionPreference solutionPreference;
    String capturesLocation;
    
    boolean updateCheckbox;
    
    ImageIcon upIcon;
    ImageIcon downIcon;
    JDialog parent;
    
    
    public PreferencesPanel() {
        initComponents();
                    
        
        
        upIcon = ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("move-up-icon.png"), 
                16, 16);
        
        downIcon = ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("move-down-icon.png"), 
                16, 16);

    }

    public void setParent(JDialog parent) {
        this.parent = parent;
    }
    
    
    
    
    public void initializeFields(Settings settings) {
    
        objectiveNames = settings.getObjectiveNames();
        defaultObjetiveNames = settings.getDefaultObjectiveNames();
        shownObjectives = settings.getShownObjectives();
        updateCheckbox = true;
        customOrder = settings.getCustomAxesOrder();
        objectivePreferences = settings.getPreferenceRows();
        solutionPreference = settings.getSolutionPreference();
        capturesLocation = settings.getCapturesLocation();
        
        this.jtfCapturesLocation.setText(capturesLocation);
        this.jckShownOriginals.setSelected(settings.isShowOriginalSetInBackground());
        switch(settings.getCurrentAxesOrderType()) {
            case DEFAULT: this.jrbDefaultOrder.setSelected(true); break;
            case AUTOMATIC: this.jrbAutomaticOrder.setSelected(true); break;
            case CUSTOM: this.jrbCustomOrder.setSelected(true); break;
            case PREFERENCES: this.jrbPreferences.setSelected(true); break;
        }            
        
        checkRadioButtons();
    }
    
    
    public final void updateListOjectiveOrder() {
        
        if (customOrderTemp == null) {
            customOrderTemp = new ArrayList<>();
            for (int i = 0; i < customOrder.size(); i++) {
                if (shownObjectives.contains(customOrder.get(i)))
                    customOrderTemp.add(customOrder.get(i));
            }
        }
                
        ArrayList<JPanel> rows = new ArrayList<>();
                
        int actualIndex = 0;
        for (int index = 0; index < customOrderTemp.size(); index++) {                                                
            if (!shownObjectives.contains(customOrderTemp.get(index))) continue;
            JPanel row = createRowOjectiveOrder(index % 2 == 0, actualIndex, customOrderTemp.get(index));
            actualIndex++;
            rows.add(row);
        }
        
        this.jpnListObjectiveOrder.removeAll();
        this.jpnListObjectiveOrder.revalidate();
        this.jpnListObjectiveOrder.repaint();
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.jpnListObjectiveOrder);
        this.jpnListObjectiveOrder.setLayout(layout);                       
        
        
        GroupLayout.ParallelGroup pG = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
        for (JPanel row : rows) {
            pG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
        }                            
                
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pG)
                .addContainerGap())
        );
        
        GroupLayout.SequentialGroup sG = layout.createSequentialGroup().addContainerGap();
        
        for (JPanel row : rows) {            
            sG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25);
        }
        
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sG)
        );
              
    }
    
    
    
    
    
    private JPanel createRowOjectiveOrder(boolean color, Integer index, Integer objective) {
        
        JPanel jpnRow = new javax.swing.JPanel();
        
        JTextField jtfObjectiveName = new JTextField();
        JTextField jtfNum = new JTextField();
        JLabel jlblUp = new javax.swing.JLabel();       
        JLabel jlblDown = new javax.swing.JLabel();               
        
        
        jlblUp.setPreferredSize(new Dimension(14, 14));
        jlblUp.setIcon(upIcon);        
        jlblUp.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jlblUp.setToolTipText("Move up");
        jlblUp.setName("up_" + index);
        jlblUp.addMouseListener(
            new CustomMouseListener().setMouseClicked((MouseEvent e) -> {            
                if (!e.getComponent().isEnabled()) return;
                int idx = Integer.parseInt(e.getComponent().getName().split("_")[1]);                
                
                System.out.println(customOrderTemp);
                
                int previous = customOrderTemp.get(idx - 1);
                customOrderTemp.set(idx - 1, customOrderTemp.get(idx));
                customOrderTemp.set(idx, previous);
                
                updateListOjectiveOrder();
                
            })
        );
        
        jlblDown.setPreferredSize(new Dimension(14, 14));
        jlblDown.setIcon(downIcon);        
        jlblDown.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jlblDown.setToolTipText("Move down");
        jlblDown.setName("down_" + index);
        jlblDown.addMouseListener(
            new CustomMouseListener().setMouseClicked((MouseEvent e) -> {
                if (!e.getComponent().isEnabled()) return;
                int idx = Integer.parseInt(e.getComponent().getName().split("_")[1]);                
                
                
                int previous = customOrderTemp.get(idx + 1);
                customOrderTemp.set(idx + 1, customOrderTemp.get(idx));
                customOrderTemp.set(idx, previous);
                
                updateListOjectiveOrder();
                               
            })
        );
        
        
        
        
        jtfObjectiveName.setText(objectiveNames.get(objective));
        jtfObjectiveName.setToolTipText(objectiveNames.get(objective));
        jtfObjectiveName.setEditable(false);
        jtfObjectiveName.setBorder(BorderFactory.createEmptyBorder());
        jtfObjectiveName.setOpaque(false);
        jtfObjectiveName.setName("tfoN_" + index);                
                
        jtfNum.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);        
        jtfNum.setText("" + (index + 1));
        jtfNum.setEditable(false);
        jtfNum.setBorder(BorderFactory.createEmptyBorder());
        jtfNum.setOpaque(false);
        jtfNum.setName("tfoN_" + index);                
        
        
        if (index == 0) jlblUp.setEnabled(false);
        if (index + 1 == shownObjectives.size()) jlblDown.setEnabled(false);
        
        javax.swing.GroupLayout jpnRowLayout = new javax.swing.GroupLayout(jpnRow);
        jpnRow.setLayout(jpnRowLayout);
        jpnRowLayout.setHorizontalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnRowLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jtfNum, 25, 25, 25)                
                .addGap(20,20,20)                
                .addComponent(jlblDown, 18, 18, 18)
                .addGap(5,5,5)
                .addComponent(jlblUp, 18, 18, 18)
                .addGap(20,20,20)                    
                .addComponent(jtfObjectiveName, 200, 200, 200)                 
                .addGap(20,20,20)                
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)                                
                .addContainerGap())
        );
        jpnRowLayout.setVerticalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtfNum, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            .addComponent(jtfObjectiveName, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            .addComponent(jlblDown, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 19, 19)                            
            .addComponent(jlblUp, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 19, 19)                            
        );
               
        if (color) jpnRow.setBackground(new java.awt.Color(230, 230, 230));        

        return jpnRow;
    }
    
    
    
    
    
    
    
    public final void updateListOjectiveNames() {
            
        selected = new HashSet<>();
        selected.addAll(shownObjectives);
        textFieldsObjectiveNames = new HashMap<>();
        checkboxesObjectiveNames = new HashMap<>();
        
        
        ArrayList<JPanel> rows = new ArrayList<>();
        
        
        for (int index = 0; index < objectiveNames.size(); index++) {                                    
            JPanel row = createRowOjectiveNames(index % 2 == 0, index, objectiveNames.get(index),defaultObjetiveNames.get(index));
            rows.add(row);
        }
        
        this.jpnListObjectiveName.removeAll();
        this.jpnListObjectiveName.revalidate();
        this.jpnListObjectiveName.repaint();
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.jpnListObjectiveName);
        this.jpnListObjectiveName.setLayout(layout);                       
        
        
        GroupLayout.ParallelGroup pG = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
        for (JPanel row : rows) {
            pG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
        }                            
                
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pG)
                .addContainerGap())
        );
        
        GroupLayout.SequentialGroup sG = layout.createSequentialGroup().addContainerGap();
        
        for (JPanel row : rows) {            
            sG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25);
        }
        
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sG)
        );
              
    }
    
    
    
    
    
    private JPanel createRowOjectiveNames(boolean color, Integer index, String objectiveName, String defaultObjectiveName) {
        
        JPanel jpnRow = new javax.swing.JPanel();
        
        JCheckBox jckSelect = new JCheckBox();
        JTextField jtfObjectiveName = new JTextField();
        JTextField jtfDefaultObjectiveName = new JTextField();


        jckSelect.setSelected(selected.contains(index));        
        jckSelect.setText("");
        jckSelect.setName("ck_" + index);
        jckSelect.setPreferredSize(new Dimension(14, 16));
        jckSelect.setOpaque(false);
        jckSelect.addChangeListener((ChangeEvent e) -> {
            if (!updateCheckbox) return;
            
            JCheckBox jc = (JCheckBox)e.getSource();
            int id = Integer.parseInt(jc.getName().split("_")[1]);
            if (jc.isSelected()) {
                selected.add(id);
            } else {
                selected.remove(id);
            }
            
            checkSelections();
        });
        checkboxesObjectiveNames.put(index, jckSelect);
        
        jtfObjectiveName.setText(objectiveName);
        jtfObjectiveName.setToolTipText(objectiveName);
        jtfObjectiveName.setEditable(true);
        //jtfObjectiveName.setBorder(BorderFactory.createEmptyBorder());
        jtfObjectiveName.setOpaque(false);
        jtfObjectiveName.setName("tfoN_" + index);                
        jtfObjectiveName.getDocument().putProperty("owner", jtfObjectiveName);
        jtfObjectiveName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                 check(e);                                              
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                 check(e);                                              
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                 check(e);                              
            }
            
            private void check(DocumentEvent e) {
                JTextField jtf = (JTextField)e.getDocument().getProperty("owner");
                int id = Integer.parseInt(jtf.getName().split("_")[1]);
                
                String text = jtf.getText();
                
                if (text.trim().isEmpty()) {
                    jtf.setBorder(BorderFactory.createLineBorder(Color.RED));                       
                    jbtnAcceptObjetiveNames.setEnabled(false);                    
                } else {
                    jtf.setBorder(BorderFactory.createLineBorder(Color.BLACK));                    
                    checkTextFields();
                }
            }
        });
        

        textFieldsObjectiveNames.put(index, jtfObjectiveName);
        
        jtfDefaultObjectiveName.setText(defaultObjectiveName);
        jtfDefaultObjectiveName.setToolTipText(defaultObjectiveName);
        jtfDefaultObjectiveName.setEditable(false);
        jtfDefaultObjectiveName.setBorder(BorderFactory.createEmptyBorder());
        jtfDefaultObjectiveName.setOpaque(false);
        
        
        
        
        javax.swing.GroupLayout jpnRowLayout = new javax.swing.GroupLayout(jpnRow);
        jpnRow.setLayout(jpnRowLayout);
        jpnRowLayout.setHorizontalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnRowLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jckSelect, 18, 18, 18)                
                .addGap(20,20,20)                
                .addComponent(jtfDefaultObjectiveName, 200, 200, 200)                 
                .addGap(18, 18, 18)
                .addComponent(jtfObjectiveName, 200, 200, 200)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)                                
                .addContainerGap())
        );
        jpnRowLayout.setVerticalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jckSelect, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 19, 19)
            .addComponent(jtfObjectiveName, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 21, 21)
            .addComponent(jtfDefaultObjectiveName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)                            
        );
               
        if (color) jpnRow.setBackground(new java.awt.Color(230, 230, 230));        

        return jpnRow;
    }
    
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jDgObjectiveNames = new javax.swing.JDialog();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jckSelectAll = new javax.swing.JCheckBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        jpnListObjectiveName = new javax.swing.JPanel();
        jbtnAcceptObjetiveNames = new javax.swing.JButton();
        jDgObjectivesOrder = new javax.swing.JDialog();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jpnListObjectiveOrder = new javax.swing.JPanel();
        jbtnAcceptbjectiveOrder = new javax.swing.JButton();
        jDgObjectivePreferences = new javax.swing.JDialog();
        jPanel4 = new javax.swing.JPanel();
        jlblObj1 = new javax.swing.JLabel();
        jlblRelation = new javax.swing.JLabel();
        jckSelectAllPreferences = new javax.swing.JCheckBox();
        jlblObj2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jpnListObjectivePreferences = new javax.swing.JPanel();
        jbtnAcceptPreference = new javax.swing.JButton();
        jckShownOriginals = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        jrbDefaultOrder = new javax.swing.JRadioButton();
        jrbAutomaticOrder = new javax.swing.JRadioButton();
        jrbCustomOrder = new javax.swing.JRadioButton();
        jbtnEditOrder = new javax.swing.JButton();
        jrbPreferences = new javax.swing.JRadioButton();
        jbtnEditPreferences = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jbtnShowObjectiveNames = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jbtnAccept = new javax.swing.JButton();
        jbtnSetDefault = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jtfCapturesLocation = new javax.swing.JTextField();
        jbtnChange = new javax.swing.JButton();

        buttonGroup1.add(this.jrbAutomaticOrder);
        buttonGroup1.add(this.jrbCustomOrder);
        buttonGroup1.add(this.jrbDefaultOrder);
        buttonGroup1.add(this.jrbPreferences);

        jDgObjectiveNames.setMinimumSize(new java.awt.Dimension(560, 273));
        jDgObjectiveNames.setModal(true);
        jDgObjectiveNames.setResizable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Original Name");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Current Name");

        jckSelectAll.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jckSelectAllMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jckSelectAll, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jckSelectAll, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jpnListObjectiveNameLayout = new javax.swing.GroupLayout(jpnListObjectiveName);
        jpnListObjectiveName.setLayout(jpnListObjectiveNameLayout);
        jpnListObjectiveNameLayout.setHorizontalGroup(
            jpnListObjectiveNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 538, Short.MAX_VALUE)
        );
        jpnListObjectiveNameLayout.setVerticalGroup(
            jpnListObjectiveNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 137, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(jpnListObjectiveName);

        jbtnAcceptObjetiveNames.setText("Accept");
        jbtnAcceptObjetiveNames.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jbtnAcceptObjetiveNamesMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jDgObjectiveNamesLayout = new javax.swing.GroupLayout(jDgObjectiveNames.getContentPane());
        jDgObjectiveNames.getContentPane().setLayout(jDgObjectiveNamesLayout);
        jDgObjectiveNamesLayout.setHorizontalGroup(
            jDgObjectiveNamesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgObjectiveNamesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDgObjectiveNamesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDgObjectiveNamesLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jbtnAcceptObjetiveNames)))
                .addContainerGap())
        );
        jDgObjectiveNamesLayout.setVerticalGroup(
            jDgObjectiveNamesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgObjectiveNamesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtnAcceptObjetiveNames)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jDgObjectivesOrder.setMinimumSize(new java.awt.Dimension(410, 253));
        jDgObjectivesOrder.setModal(true);
        jDgObjectivesOrder.setResizable(false);

        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Objective");

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("#");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(81, 81, 81)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jpnListObjectiveOrderLayout = new javax.swing.GroupLayout(jpnListObjectiveOrder);
        jpnListObjectiveOrder.setLayout(jpnListObjectiveOrderLayout);
        jpnListObjectiveOrderLayout.setHorizontalGroup(
            jpnListObjectiveOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 420, Short.MAX_VALUE)
        );
        jpnListObjectiveOrderLayout.setVerticalGroup(
            jpnListObjectiveOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 137, Short.MAX_VALUE)
        );

        jScrollPane2.setViewportView(jpnListObjectiveOrder);

        jbtnAcceptbjectiveOrder.setText("Accept");
        jbtnAcceptbjectiveOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnAcceptbjectiveOrderActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDgObjectivesOrderLayout = new javax.swing.GroupLayout(jDgObjectivesOrder.getContentPane());
        jDgObjectivesOrder.getContentPane().setLayout(jDgObjectivesOrderLayout);
        jDgObjectivesOrderLayout.setHorizontalGroup(
            jDgObjectivesOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgObjectivesOrderLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDgObjectivesOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDgObjectivesOrderLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jbtnAcceptbjectiveOrder)))
                .addContainerGap())
        );
        jDgObjectivesOrderLayout.setVerticalGroup(
            jDgObjectivesOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgObjectivesOrderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtnAcceptbjectiveOrder)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jDgObjectivePreferences.setMinimumSize(new java.awt.Dimension(935, 273));
        jDgObjectivePreferences.setModal(true);
        jDgObjectivePreferences.setResizable(false);

        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jlblObj1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlblObj1.setText("Select important objectives");

        jlblRelation.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlblRelation.setText("Priority");

        jckSelectAllPreferences.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jckSelectAllPreferencesMouseClicked(evt);
            }
        });

        jlblObj2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlblObj2.setText("Objective");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jckSelectAllPreferences, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(jlblObj1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(jlblRelation, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(jlblObj2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jlblObj1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jlblRelation, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jckSelectAllPreferences, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jlblObj2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jpnListObjectivePreferences.setPreferredSize(new java.awt.Dimension(740, 167));

        javax.swing.GroupLayout jpnListObjectivePreferencesLayout = new javax.swing.GroupLayout(jpnListObjectivePreferences);
        jpnListObjectivePreferences.setLayout(jpnListObjectivePreferencesLayout);
        jpnListObjectivePreferencesLayout.setHorizontalGroup(
            jpnListObjectivePreferencesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 908, Short.MAX_VALUE)
        );
        jpnListObjectivePreferencesLayout.setVerticalGroup(
            jpnListObjectivePreferencesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 167, Short.MAX_VALUE)
        );

        jScrollPane3.setViewportView(jpnListObjectivePreferences);

        jbtnAcceptPreference.setText("Accept");
        jbtnAcceptPreference.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jbtnAcceptPreferenceMouseClicked(evt);
            }
        });
        jbtnAcceptPreference.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnAcceptPreferenceActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDgObjectivePreferencesLayout = new javax.swing.GroupLayout(jDgObjectivePreferences.getContentPane());
        jDgObjectivePreferences.getContentPane().setLayout(jDgObjectivePreferencesLayout);
        jDgObjectivePreferencesLayout.setHorizontalGroup(
            jDgObjectivePreferencesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgObjectivePreferencesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDgObjectivePreferencesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 911, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDgObjectivePreferencesLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jbtnAcceptPreference)))
                .addContainerGap())
        );
        jDgObjectivePreferencesLayout.setVerticalGroup(
            jDgObjectivePreferencesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgObjectivePreferencesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtnAcceptPreference)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jckShownOriginals.setText("Always show original set in background");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Axes order"));

        jrbDefaultOrder.setText("Default order");
        jrbDefaultOrder.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jrbDefaultOrderMouseClicked(evt);
            }
        });

        jrbAutomaticOrder.setText("Automatic order by correlation of objectives [1]");
        jrbAutomaticOrder.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jrbAutomaticOrderMouseClicked(evt);
            }
        });

        jrbCustomOrder.setText("Custom order");
        jrbCustomOrder.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jrbCustomOrderMouseClicked(evt);
            }
        });

        jbtnEditOrder.setText("Edit order");
        jbtnEditOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnEditOrderActionPerformed(evt);
            }
        });

        jrbPreferences.setText("Preferences");
        jrbPreferences.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jrbPreferencesMouseClicked(evt);
            }
        });

        jbtnEditPreferences.setText("Customize");
        jbtnEditPreferences.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnEditPreferencesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jrbAutomaticOrder)
                    .addComponent(jrbDefaultOrder)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jrbPreferences)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jbtnEditPreferences))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jrbCustomOrder)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jbtnEditOrder))))
                .addContainerGap(86, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jrbDefaultOrder)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jrbAutomaticOrder)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jrbCustomOrder)
                    .addComponent(jbtnEditOrder))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jrbPreferences)
                    .addComponent(jbtnEditPreferences))
                .addGap(0, 30, Short.MAX_VALUE))
        );

        jLabel1.setText("Configure objectives:");

        jbtnShowObjectiveNames.setText("Objectives");
        jbtnShowObjectiveNames.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jbtnShowObjectiveNamesMouseClicked(evt);
            }
        });

        jbtnAccept.setText("Apply");
        jbtnAccept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnAcceptActionPerformed(evt);
            }
        });

        jbtnSetDefault.setText("Set deffault");
        jbtnSetDefault.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnSetDefaultActionPerformed(evt);
            }
        });

        jLabel6.setText("Captures location:");

        jtfCapturesLocation.setEditable(false);

        jbtnChange.setText("Change");
        jbtnChange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnChangeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jckShownOriginals)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jSeparator1)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jbtnSetDefault)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jbtnAccept))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jbtnShowObjectiveNames)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jtfCapturesLocation)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jbtnChange)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jckShownOriginals)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jbtnShowObjectiveNames))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jtfCapturesLocation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtnChange)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbtnAccept)
                    .addComponent(jbtnSetDefault))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jbtnShowObjectiveNamesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jbtnShowObjectiveNamesMouseClicked
        updateListOjectiveNames();
        this.jDgObjectiveNames.setVisible(true);
    }//GEN-LAST:event_jbtnShowObjectiveNamesMouseClicked

    
    
    private void checkTextFields() {
        boolean enabled = true;
        for(Integer index1 : this.textFieldsObjectiveNames.keySet()) {
            JTextField jtf1 = this.textFieldsObjectiveNames.get(index1);            
            boolean valid = true;
            if (jtf1.getText().trim().isEmpty())  valid = false;
            
            for(Integer index2 : this.textFieldsObjectiveNames.keySet()) {
                if (!Objects.equals(index1, index2)) {
                    JTextField jtf2 = this.textFieldsObjectiveNames.get(index2);
                    
                    if (jtf1.getText().trim().equalsIgnoreCase(jtf2.getText().trim())) {                        
                        valid = false;
                        enabled = false;
                    }                   
                    
                }
            }            
            
            if (!valid) jtf1.setBorder(BorderFactory.createLineBorder(Color.RED));
            else jtf1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        }
        
        jbtnAcceptObjetiveNames.setEnabled(enabled);
    }
    
    
    private void jbtnAcceptObjetiveNamesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jbtnAcceptObjetiveNamesMouseClicked
        
        for(Integer index : this.textFieldsObjectiveNames.keySet()) {
            JTextField jtf = this.textFieldsObjectiveNames.get(index);
            
            this.objectiveNames.set(index, jtf.getText());
        }
        
        this.shownObjectives = new HashSet<>();
        this.shownObjectives.addAll(selected);
        this.jDgObjectiveNames.dispose();
        
        if (PreferencesOrderGuiHelper.checkShownChanged(objectivePreferences, shownObjectives)) {
            objectivePreferences = null;
        }
        
    }//GEN-LAST:event_jbtnAcceptObjetiveNamesMouseClicked

    
    private void checkSelections() {
        jbtnAcceptObjetiveNames.setEnabled(selected.size() >= 2);        
    }
    
    private void jckSelectAllMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jckSelectAllMouseClicked
        updateCheckbox = false;
        for (Integer index : checkboxesObjectiveNames.keySet()) {
            JCheckBox jck = checkboxesObjectiveNames.get(index);
            
            jck.setSelected(jckSelectAll.isSelected());
            if (jckSelectAll.isSelected()) {
                selected.add(index);
            } else {
                selected.remove(index);
            }
        }
        updateCheckbox = true;
        checkSelections();
    }//GEN-LAST:event_jckSelectAllMouseClicked

    private void jbtnSetDefaultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnSetDefaultActionPerformed
        Settings settings = new Settings(PlotFrame.solutionDraw.getSolutionSet());
        initializeFields(settings);
    }//GEN-LAST:event_jbtnSetDefaultActionPerformed

    private void jbtnEditOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnEditOrderActionPerformed
        customOrderTemp = null;
        updateListOjectiveOrder();
        this.jDgObjectivesOrder.setVisible(true);
    }//GEN-LAST:event_jbtnEditOrderActionPerformed

    private void jbtnAcceptbjectiveOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnAcceptbjectiveOrderActionPerformed
        customOrder = new ArrayList<>(customOrderTemp);
        
        for(int i = 0; i < objectiveNames.size(); i++) {
            if (customOrder.indexOf(i) < 0) customOrder.add(i);
        }
        
        this.jDgObjectivesOrder.dispose();
    }//GEN-LAST:event_jbtnAcceptbjectiveOrderActionPerformed

    
    private void checkRadioButtons() {
       this.jbtnAccept.setEnabled(true);
       this.jbtnEditOrder.setEnabled(this.jrbCustomOrder.isSelected());
       this.jbtnEditPreferences.setEnabled(this.jrbPreferences.isSelected());
       
       if (this.jrbPreferences.isSelected() && solutionPreference == null) {
           this.jbtnAccept.setEnabled(false);
       } 
    }
    
    private void jrbCustomOrderMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jrbCustomOrderMouseClicked
        checkRadioButtons();
    }//GEN-LAST:event_jrbCustomOrderMouseClicked

    private void jrbAutomaticOrderMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jrbAutomaticOrderMouseClicked
        checkRadioButtons();
    }//GEN-LAST:event_jrbAutomaticOrderMouseClicked

    private void jrbDefaultOrderMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jrbDefaultOrderMouseClicked
        checkRadioButtons();
    }//GEN-LAST:event_jrbDefaultOrderMouseClicked

    private void jbtnAcceptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnAcceptActionPerformed
        
        Settings settings = new Settings(PlotFrame.solutionDraw.getSolutionSet());
        
        settings.setObjectiveNames(objectiveNames);
        settings.setShowOriginalSetInBackground(this.jckShownOriginals.isSelected());
        
        settings.setCustomAxesOrder(customOrder);
        
        settings.setPreferenceRows(objectivePreferences);
        settings.setSolutionPreference(solutionPreference);
        
        if (this.jrbDefaultOrder.isSelected()) {
            settings.setCurrentAxesOrderType(Settings.AxesOrderType.DEFAULT);
        } else if (this.jrbAutomaticOrder.isSelected()) {
            settings.setCurrentAxesOrderType(Settings.AxesOrderType.AUTOMATIC);
        } else if (this.jrbPreferences.isSelected()) {
            settings.setCurrentAxesOrderType(Settings.AxesOrderType.PREFERENCES);
        } else {
            settings.setCurrentAxesOrderType(Settings.AxesOrderType.CUSTOM);
        }
        
        settings.setCapturesLocation(this.jtfCapturesLocation.getText());
        settings.setShownObjectives(shownObjectives);
        PlotFrame.settings = settings;
        PlotFrame.solutionDraw.getSolutionSet().recalculateShapes(PlotFrame.isMinProblem);
        PlotFrame.selectionsPanel.updateList();
        PlotFrame.clusterizePanel.updateList();        
        //PlotFrame.plotPanel.redraw();        
        parent.dispose();
        
    }//GEN-LAST:event_jbtnAcceptActionPerformed

    private void jrbPreferencesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jrbPreferencesMouseClicked
        checkRadioButtons();
    }//GEN-LAST:event_jrbPreferencesMouseClicked

    private void jbtnEditPreferencesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnEditPreferencesActionPerformed
        //updateListOjectiveOrder();       
        
        
        PreferencesOrderGuiHelper.prepareShowObjectiveRelations(jckSelectAllPreferences, jlblObj1, jlblRelation, jlblObj2, jbtnAcceptPreference);
        if (objectivePreferences != null) {
            PreferencesOrderGuiHelper.prepareFromCustomRows(objectivePreferences);            
        } else {
            List<SolutionPreference.Relation> relations = PreferencesOrderGuiHelper.getDefault(objectiveNames, shownObjectives);
            PreferencesOrderGuiHelper.prepareRelationsFromList(relations);
            solutionPreference = PreferencesOrderGuiHelper.solutionPreference;
            
        }
        
        PreferencesOrderGuiHelper.updateList(this.jpnListObjectivePreferences, jbtnAcceptPreference, objectiveNames);
        this.jDgObjectivePreferences.setVisible(true);
    }//GEN-LAST:event_jbtnEditPreferencesActionPerformed

    private void jckSelectAllPreferencesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jckSelectAllPreferencesMouseClicked
        PreferencesOrderGuiHelper.toogleSelectAll(jckSelectAllPreferences, jbtnAcceptPreference);
    }//GEN-LAST:event_jckSelectAllPreferencesMouseClicked

    private void jbtnAcceptPreferenceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jbtnAcceptPreferenceMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jbtnAcceptPreferenceMouseClicked

    private void jbtnAcceptPreferenceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnAcceptPreferenceActionPerformed
        if (PreferencesOrderGuiHelper.state == PreferencesOrderGuiHelper.State.SHOW_RELATIONS) {
            PreferencesOrderGuiHelper.prepareSelectObjectives(jckSelectAllPreferences, jlblObj1, jlblRelation, jlblObj2, jbtnAcceptPreference, objectiveNames, shownObjectives);
            PreferencesOrderGuiHelper.updateList(jpnListObjectivePreferences, jbtnAcceptPreference, objectiveNames);
        } else if (PreferencesOrderGuiHelper.state == PreferencesOrderGuiHelper.State.SELECT_OBJECTIVES) {
            PreferencesOrderGuiHelper.prepareForEditRelations(jckSelectAllPreferences, jlblObj1, jlblRelation, jlblObj2, jbtnAcceptPreference);
            PreferencesOrderGuiHelper.nextRelationStep(jpnListObjectivePreferences, jbtnAcceptPreference, objectiveNames);            
        } else if (PreferencesOrderGuiHelper.state == PreferencesOrderGuiHelper.State.FINISHED) {
            objectivePreferences = PreferencesOrderGuiHelper.rows;
            solutionPreference = PreferencesOrderGuiHelper.solutionPreference;
            this.jbtnAccept.setEnabled(true);
            this.jDgObjectivePreferences.dispose();
        }
    }//GEN-LAST:event_jbtnAcceptPreferenceActionPerformed

    private void jbtnChangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnChangeActionPerformed
       
                        
        SelectFolderFileUtil.select(
                EnumSet.of(SelectFolderFileUtil.SelectionOption.SELECT_FOLDER), 
                new SelectFolderFileUtil.SelectAfter() {
         
            @Override
            public void excecute(String fullPath) {
                if (fullPath != null) {
                    PlotFrame.preferencesPanel.jtfCapturesLocation.setText(fullPath);
                }
            }
        });
        
    }//GEN-LAST:event_jbtnChangeActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JDialog jDgObjectiveNames;
    private javax.swing.JDialog jDgObjectivePreferences;
    private javax.swing.JDialog jDgObjectivesOrder;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JButton jbtnAccept;
    private javax.swing.JButton jbtnAcceptObjetiveNames;
    private javax.swing.JButton jbtnAcceptPreference;
    private javax.swing.JButton jbtnAcceptbjectiveOrder;
    private javax.swing.JButton jbtnChange;
    private javax.swing.JButton jbtnEditOrder;
    private javax.swing.JButton jbtnEditPreferences;
    private javax.swing.JButton jbtnSetDefault;
    private javax.swing.JButton jbtnShowObjectiveNames;
    private javax.swing.JCheckBox jckSelectAll;
    private javax.swing.JCheckBox jckSelectAllPreferences;
    private javax.swing.JCheckBox jckShownOriginals;
    private javax.swing.JLabel jlblObj1;
    private javax.swing.JLabel jlblObj2;
    private javax.swing.JLabel jlblRelation;
    private javax.swing.JPanel jpnListObjectiveName;
    private javax.swing.JPanel jpnListObjectiveOrder;
    private javax.swing.JPanel jpnListObjectivePreferences;
    private javax.swing.JRadioButton jrbAutomaticOrder;
    private javax.swing.JRadioButton jrbCustomOrder;
    private javax.swing.JRadioButton jrbDefaultOrder;
    private javax.swing.JRadioButton jrbPreferences;
    private javax.swing.JTextField jtfCapturesLocation;
    // End of variables declaration//GEN-END:variables
}
