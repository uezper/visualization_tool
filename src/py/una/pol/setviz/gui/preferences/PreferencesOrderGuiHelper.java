/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.preferences;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import net.infonode.properties.propertymap.ref.ThisPropertyMapRef;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.solution.preferences.SolutionPreference;

/**
 *
 * @author uriel
 */
public class PreferencesOrderGuiHelper {
    
    public static enum State {
        SHOW_RELATIONS,
        SELECT_OBJECTIVES,
        EDIT_RELATIONS,
        FINISHED
    }
    
    public static class Row { 
        
        public static enum RowType {
            OBJECTIVE_SELECTION,
            OBJECTIVE_RELATION
        }        

        public Row() {
            rowId = 0;
            rowType = RowType.OBJECTIVE_SELECTION;
            objectiveOne = 0;
            objectiveTwo = 0;
            preference = "";
            isEditable = false;
        }
        
        
        
        int rowId;        
        boolean isEditable;
        RowType rowType;
        Integer objectiveOne;
        Integer objectiveTwo;
        String preference;

        public int getRowId() {
            return rowId;
        }

        public void setRowId(int rowId) {
            this.rowId = rowId;
        }

        public boolean isIsEditable() {
            return isEditable;
        }

        public void setIsEditable(boolean isEditable) {
            this.isEditable = isEditable;
        }

        public RowType getRowType() {
            return rowType;
        }

        public void setRowType(RowType rowType) {
            this.rowType = rowType;
        }

        public Integer getObjectiveOne() {
            return objectiveOne;
        }

        public void setObjectiveOne(Integer objectiveOne) {
            this.objectiveOne = objectiveOne;
        }

        public Integer getObjectiveTwo() {
            return objectiveTwo;
        }

        public void setObjectiveTwo(Integer objectiveTwo) {
            this.objectiveTwo = objectiveTwo;
        }

        public String getPreference() {
            return preference;
        }

        public void setPreference(String preference) {
            this.preference = preference;
        }

        @Override
        public String toString() {
            return "Row{" + "rowId=" + rowId + ", isEditable=" + isEditable + ", rowType=" + rowType + ", objectiveOne=" + objectiveOne + ", objectiveTwo=" + objectiveTwo + ", preference=" + preference + '}';
        }
        
        
        
    }
 
    public static State state;
    public static List<Row> rows;
    public static Map<Integer, Row> rowsById;
    public static SolutionPreference solutionPreference;
    
    private static HashMap<Integer, JCheckBox> checkboxesObjectiveNames;
    private static Map<Integer, Integer> objIdByCustomId;
    private static Map<String, SolutionPreference.PreferenceType> preferenceTypeByDesc;
    private static Set<Integer> selected;
    private static boolean updateCheckbox;
    
    
    static {
        preferenceTypeByDesc = new HashMap<>();
        for (SolutionPreference.PreferenceType p : SolutionPreference.PreferenceType.values()) {
           preferenceTypeByDesc.put(p.getDescription(), p);
        }
        
        selected = new HashSet<>();
        state = State.SHOW_RELATIONS;
        prepareRelationsFromList(getDefault(PlotFrame.settings.getObjectiveNames(), PlotFrame.settings.getShownObjectives()));
    }
    
    
    public static void prepareShowObjectiveRelations(JCheckBox jckSelectAll, JLabel jlblObj1,  JLabel jlblPref, JLabel jlblObj2, JButton jbtnAccept) {
        
        jckSelectAll.setVisible(false);
        jlblObj2.setVisible(true);
        jlblPref.setVisible(true);
        jlblObj1.setText("Objective");
        jbtnAccept.setText("Edit");
        jbtnAccept.setEnabled(true);
        
        state = State.SHOW_RELATIONS;
    }
    
    
    public static void prepareSelectObjectives(JCheckBox jckSelectAll, JLabel jlblObj1,  JLabel jlblPref, JLabel jlblObj2, JButton jbtnAccept, List<String> objectiveNames, Set<Integer> shownObjectives) {
        
        jckSelectAll.setVisible(true);
        jlblObj1.setText("Select important objectives");
        jlblObj1.setVisible(true);
        jlblObj2.setVisible(false);
        jlblPref.setVisible(false);
        jbtnAccept.setText("Next");
        jbtnAccept.setEnabled(true);
        
        state = State.SELECT_OBJECTIVES;
        
        
        rows = new ArrayList<>();
        rowsById = new HashMap<>();
        selected = new HashSet<>();
        
        
        int i = 0;
        for(int objId = 0; objId < objectiveNames.size(); objId++) {
            if (!shownObjectives.contains(objId)) continue;
            
            Row row = new Row();
            row.setRowId(i++);
            row.setRowType(Row.RowType.OBJECTIVE_SELECTION);
            row.setIsEditable(false);
            row.setObjectiveOne(objId);
            
            
            rows.add(row);
            rowsById.put(row.getRowId(), row);
            selected.add(row.getRowId());
        }
        
    }
    
    
    public static void prepareForEditRelations(JCheckBox jckSelectAll, JLabel jlblObj1,  JLabel jlblPref, JLabel jlblObj2, JButton jbtnAccept) {
        
        jckSelectAll.setVisible(false);
        jlblObj1.setText("Objective");
        jlblObj1.setVisible(true);
        jlblObj2.setVisible(true);
        jlblPref.setVisible(true);
        jbtnAccept.setText("Apply");
        jbtnAccept.setEnabled(false);
        
        state = State.EDIT_RELATIONS;
        
        objIdByCustomId = new HashMap<>();
        
        int newId = 0;
        for (Integer idSelected : selected) {            
            objIdByCustomId.put(newId++, rowsById.get(idSelected).objectiveOne);            
        }
        
        solutionPreference = new SolutionPreference(selected.size());
        
        rows = new ArrayList<>();        
        
    }
    
    
    public static void nextRelationStep(JPanel jpnPanel, JButton jbtnButton, List<String> objectiveNames) {
        
        if (!solutionPreference.finished()) {
            
            List<SolutionPreference.Relation> nextRelations = solutionPreference.getUnAnsweredQuestions();
            
            
            for(SolutionPreference.Relation nextRelation : nextRelations) {
                
                Row newRow = new Row();
                newRow.setRowId(rows.size());
                newRow.setIsEditable(true);
                newRow.setObjectiveOne(objIdByCustomId.get(nextRelation.getGroup1()));
                newRow.setObjectiveTwo(objIdByCustomId.get(nextRelation.getGroup2()));
                newRow.setPreference(SolutionPreference.PreferenceType.LESS_IMPORTANT_THAN.getDescription());
                newRow.setRowType(Row.RowType.OBJECTIVE_RELATION);

                rows.add(newRow);
                rowsById.put(newRow.getRowId(), newRow);
                
            }
            
            
            updateList(jpnPanel, jbtnButton, objectiveNames);
        } else {
            state = State.FINISHED;
            updateList(jpnPanel, jbtnButton, objectiveNames);
            jbtnButton.setEnabled(true);
        }
        
    }
    
    
    private static void submitRow(Integer rowId, JPanel jpnPanel, JButton jbtnButton, List<String> objectiveNames) {
        
        Row row = rowsById.get(rowId);
        
        
        int obj1 = -1;
        int obj2 = -1;
        
        for (Integer customId : objIdByCustomId.keySet()) {
            Integer id = objIdByCustomId.get(customId);
            if (id.intValue() == row.getObjectiveOne().intValue()) {
                obj1 = customId;
            } else if (id.intValue() == row.getObjectiveTwo().intValue()) {
                obj2 = customId;
            }            
        }
        
        solutionPreference.iterate(obj1, preferenceTypeByDesc.get(row.preference), obj2);
        
        rows = new ArrayList<>();
        rowsById = new HashMap<>();
        
        prepareRelationsFromList(solutionPreference.getAnsweredQuestions());        
        nextRelationStep(jpnPanel, jbtnButton, objectiveNames);
        
    }
    
    public static void updateList(JPanel jpnListPanel, JButton jbtnButton, List<String> objectiveNames) {
        
        ArrayList<JPanel> jpRows = new ArrayList<>();
        
        updateCheckbox = true;       
        checkboxesObjectiveNames = new HashMap<>();
        
        for (int index = 0; index < rows.size(); index++) {                                    
            JPanel row = createRow(index % 2 == 0, index, rows.get(index), jpnListPanel, jbtnButton, objectiveNames);
            jpRows.add(row);
        }
        
        jpnListPanel.removeAll();
        jpnListPanel.revalidate();
        jpnListPanel.repaint();
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(jpnListPanel);
        jpnListPanel.setLayout(layout);                       
        
        
        GroupLayout.ParallelGroup pG = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
        for (JPanel row : jpRows) {
            pG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
        }                            
                
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pG)
                .addContainerGap())
        );
        
        GroupLayout.SequentialGroup sG = layout.createSequentialGroup().addContainerGap();
        
        for (JPanel row : jpRows) {            
            sG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25);
        }
        
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sG)
        );
              
    }
    
    
    
    
    
    private static void checkSelections(JButton jbtnButton) {
        jbtnButton.setEnabled(selected.size() >= 2);        
    }    
    
    
    public static void toogleSelectAll(JCheckBox jckSelectAll, JButton jbtnButton) {       
        
        updateCheckbox = false;
        for (Integer index : checkboxesObjectiveNames.keySet()) {
            JCheckBox jck = checkboxesObjectiveNames.get(index);
            
            jck.setSelected(jckSelectAll.isSelected());
            if (jckSelectAll.isSelected()) {
                selected.add(index);
            } else {
                selected.remove(index);
            }
        }
        updateCheckbox = true;
        checkSelections(jbtnButton);
    }
    
    public static List<SolutionPreference.Relation> getDefault(ArrayList<String> objectiveNames, Set<Integer> shownObjectives) {
        
        
        objIdByCustomId = new HashMap<>();
        
        int customId = 0;
        for(int id = 0; id < objectiveNames.size(); id++) {
            if (!shownObjectives.contains(id)) continue;
            objIdByCustomId.put(customId, id);
            customId++;
        }
        
        
        solutionPreference = new SolutionPreference(shownObjectives.size());
        
        while(!solutionPreference.finished()) {
                        
            SolutionPreference.Relation unAswered = solutionPreference.getUnAnsweredQuestions().get(0);
            solutionPreference.iterate(unAswered.getGroup1(), SolutionPreference.PreferenceType.EQUALLY_IMPORTANT_TO, unAswered.getGroup2());
            
        }
        
        return solutionPreference.getAnsweredQuestions();
    }
    
    
    public static void prepareFromCustomRows(List<Row> customRows) {
        
        rows = new ArrayList<>();
        rowsById = new HashMap<>();
        
        int id = 0;
        for(Row row : customRows) {            
            rows.add(row);
            rowsById.put(row.getRowId(), row);
        }
        
        
    }
    
    public static boolean checkShownChanged(List<Row> rows, Set<Integer> show) {
        
        boolean affected = false;
        for(Row row : rows) {
            if (!show.contains(row.getObjectiveOne()) || !show.contains(row.getObjectiveTwo())) {
                affected = true;
                break;
            }
        }
        
        return affected;
    }
        
    public static void prepareRelationsFromList(List<SolutionPreference.Relation> relations) {
        
        rows = new ArrayList<>();
        rowsById = new HashMap<>();
        
        int id = 0;
        for(SolutionPreference.Relation r : relations) {
            Row row = new Row();
            row.setIsEditable(false);
            row.setRowType(Row.RowType.OBJECTIVE_RELATION);
            row.setObjectiveOne(objIdByCustomId.get(r.getGroup1()));
            row.setObjectiveTwo(objIdByCustomId.get(r.getGroup2()));
            row.setPreference(r.getPreference().getDescription());
            
            rows.add(row);
            rowsById.put(row.getRowId(), row);
        }
        
    }
    
    
    public static List<Integer> getOrderFromPreferences(Set<Integer> showObjectives) {
        if (solutionPreference == null || !solutionPreference.finished()) return null;
        
        List<Integer> ret = new ArrayList<>();
        List<SolutionPreference.Priority> priorities = solutionPreference.getPriorities();
                
        
        for(SolutionPreference.Priority priority : priorities) {
            ret.add(objIdByCustomId.get(priority.getGroup()));
        }
                
        for(Integer id : showObjectives) {
            if (!ret.contains(id)) {
                ret.add(id);
            }
        }
        
        return ret;
    }
    
    
    private static JPanel createRow(boolean color, Integer index, Row row, JPanel jpnPanelList, JButton jbtnButton, List<String> objectiveNames) {
        
        JPanel jpnRow = new javax.swing.JPanel();
        
        JCheckBox jckSelect = new JCheckBox();
        JTextField jtfObjectiveOne = new JTextField();
        JTextField jtfObjectiveTwo = new JTextField();        
        JTextField jtfObjectivePreference = new JTextField();
        JComboBox jcmbObjectivePreference = new JComboBox();
        JButton jbtnApply = new JButton();

        jckSelect.setSelected(selected.contains(row.rowId));        
        jckSelect.setText("");
        jckSelect.setName("ck_" + row.rowId);
        jckSelect.setPreferredSize(new Dimension(14, 16));
        jckSelect.setOpaque(false);
        jckSelect.addChangeListener((ChangeEvent e) -> {
            
            if (!updateCheckbox) return;
            
            JCheckBox jc = (JCheckBox)e.getSource();
            int id = Integer.parseInt(jc.getName().split("_")[1]);         
            
            if (jc.isSelected()) {
                selected.add(id);
            } else {
                selected.remove(id);
            }
            
                        
            checkSelections(jbtnButton);
        });
        
        
        checkboxesObjectiveNames.put(row.rowId, jckSelect);
        
        jtfObjectiveOne.setText(objectiveNames.get(row.objectiveOne));
        jtfObjectiveOne.setToolTipText(objectiveNames.get(row.objectiveOne));
        jtfObjectiveOne.setEditable(false);
        jtfObjectiveOne.setBorder(BorderFactory.createEmptyBorder());
        jtfObjectiveOne.setOpaque(false);
        jtfObjectiveOne.setHorizontalAlignment(JTextField.CENTER);
        jtfObjectiveOne.setName("tfobj1_" + row.rowId); 
                
        jtfObjectiveTwo.setText(objectiveNames.get(row.objectiveTwo));
        jtfObjectiveTwo.setToolTipText(objectiveNames.get(row.objectiveTwo));
        jtfObjectiveTwo.setEditable(false);
        jtfObjectiveTwo.setBorder(BorderFactory.createEmptyBorder());
        jtfObjectiveTwo.setOpaque(false);
        jtfObjectiveTwo.setHorizontalAlignment(JTextField.CENTER);
        jtfObjectiveTwo.setName("tfobj2_" + row.rowId); 
    
        jtfObjectivePreference.setText(row.preference);
        jtfObjectivePreference.setToolTipText(row.preference);
        jtfObjectivePreference.setEditable(false);
        jtfObjectivePreference.setBorder(BorderFactory.createEmptyBorder());
        jtfObjectivePreference.setOpaque(false);
        jtfObjectivePreference.setHorizontalAlignment(JTextField.CENTER);
        jtfObjectivePreference.setName("tfobjp_" + row.rowId); 
    
        jbtnApply.setText("Apply");
        jbtnApply.setVisible(false);
        jbtnApply.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            
                submitRow(row.getRowId(), jpnPanelList, jbtnButton, objectiveNames);
           
            }
        });
                   
        for(SolutionPreference.PreferenceType p : SolutionPreference.PreferenceType.values()) {
            jcmbObjectivePreference.addItem(p.getDescription());
        }
        
        jcmbObjectivePreference.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                
                String selected = (String)jcmbObjectivePreference.getSelectedItem();
                row.setPreference(preferenceTypeByDesc.get(selected).getDescription());                                
                
            }
        });
    
        Component preference;
        if (row.rowType == Row.RowType.OBJECTIVE_SELECTION) {
            preference = jtfObjectivePreference;
            preference.setVisible(false);
        } else {
            if (row.isEditable) {
                preference = jcmbObjectivePreference;
                jbtnApply.setVisible(true);
            } else {
                preference = jtfObjectivePreference;
            }
        }
        
        jckSelect.setVisible(row.rowType == Row.RowType.OBJECTIVE_SELECTION);        
        jtfObjectiveTwo.setVisible(row.rowType != Row.RowType.OBJECTIVE_SELECTION);
        
        
        
        
        javax.swing.GroupLayout jpnRowLayout = new javax.swing.GroupLayout(jpnRow);
        jpnRow.setLayout(jpnRowLayout);
        jpnRowLayout.setHorizontalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnRowLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jckSelect, 18, 18, 18)                
                .addGap(20,20,20)                
                .addComponent(jtfObjectiveOne, 200, 200, 200)
                .addGap(20,20,20)
                .addComponent(preference, 200, 200, 200)
                .addGap(20,20,20)                    
                .addComponent(jtfObjectiveTwo, 200, 200, 200)                 
                .addGap(20,20,20)                
                .addComponent(jbtnApply, 100, 100, 100)                 
                .addGap(20,20,20)     
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)                                
                .addContainerGap())
        );
        jpnRowLayout.setVerticalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jckSelect, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            .addComponent(jtfObjectiveOne, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            .addComponent(preference, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 19, 19)                            
            .addComponent(jtfObjectiveTwo, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 19, 19)                            
            .addComponent(jbtnApply, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 19, 19)                            
        );
        
        
        
        
        if (color) jpnRow.setBackground(new java.awt.Color(230, 230, 230));        
        
        return jpnRow;
    }    
    
    
    
    
    
        
}
