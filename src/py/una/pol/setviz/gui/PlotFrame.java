/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui;

import py.una.pol.setviz.gui.selections.SelectionsPanel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import net.infonode.docking.DockingWindow;
import net.infonode.docking.RootWindow;
import net.infonode.docking.SplitWindow;
import net.infonode.docking.TabWindow;
import net.infonode.docking.View;
import net.infonode.docking.theme.DockingWindowsTheme;
import net.infonode.docking.theme.GradientDockingTheme;
import net.infonode.docking.util.DockingUtil;
import net.infonode.docking.util.ViewMap;
import py.una.pol.setviz.gui.clusters.ClusterizePanel;
import py.una.pol.setviz.gui.draw.SolutionDrawManager;
import py.una.pol.setviz.gui.filters.FilterListPanel;
import py.una.pol.setviz.gui.groups.GroupListPanel;
import py.una.pol.setviz.gui.language.LanguageKeys;
import py.una.pol.setviz.gui.language.LanguageManager;
import py.una.pol.setviz.gui.preferences.PreferencesPanel;
import py.una.pol.setviz.gui.ranking.RankingPanel;
import py.una.pol.setviz.utils.CustomMouseListener;
import py.una.pol.setviz.utils.DrawEntry;
import py.una.pol.setviz.utils.ImageUtils;
import py.una.pol.setviz.utils.SelectionEntry;
import py.una.pol.setviz.Settings;
import py.una.pol.setviz.gui.help.AboutPanel;
import py.una.pol.setviz.utils.ScreenImage;
import py.una.pol.setviz.utils.solution.Group;
import py.una.pol.setviz.utils.solution.SolutionSet;

/**
 *
 * @author acost
 */
public class PlotFrame extends javax.swing.JFrame {

    private final double ZOOM_PROP_WIDTH = 1/4f;
    private final double ZOOM_PROP_HEIGHT = 1/4f;
    
    private final int ZOOM_MIN_WIDTH = 100;
    private final int ZOOM_MIN_HEIGHT = 100;
    
    private static final String PLOT_RAD_NAME = "RAD";
    private static final String PLOT_PAR_NAME = "PAR";    

    ToolSelected toolSelected;
    ZoomState zoomState;
    int draggingX;
    int draggingY;
    
    // VIEWS IDS
    private static final int TOOLS_VIEW_ID = 0;
    private static final int FILTER_VIEW_ID = 1;
    private static final int GENERAL_VIEW_ID = 2;
    private static final int GROUP_VIEW_ID = 3;
    private static final int CLUSTER_VIEW_ID = 4;
    private static final int SELECTION_VIEW_ID = 5;       
    private static final int PLOT_PAR_VIEW_ID = 6;       
    private static final int PLOT_RAD_VIEW_ID = 7;       
    private static final int[] VIEWS_IDS = {
        TOOLS_VIEW_ID, 
        FILTER_VIEW_ID, 
        GENERAL_VIEW_ID,
        GROUP_VIEW_ID, 
        CLUSTER_VIEW_ID, 
        SELECTION_VIEW_ID,
        PLOT_PAR_VIEW_ID,
        PLOT_RAD_VIEW_ID};
    
    
    // PANELS    
    public static SelectionsPanel selectionsPanel;
    public static SolutionDrawManager solutionDraw;
    public static GroupListPanel groupListPanel;
    public static PreferencesPanel preferencesPanel;    
    public static AboutPanel aboutPanelPanel;    
    public static FilterListPanel filterListPanel;
    public static ClusterizePanel clusterizePanel;
    public static GeneralInfoPanel generalInfoPanel;
    public static RankingPanel rankingPanel;    
    public static PlotPanel plotParPanel;
    public static PlotPanel plotRadPanel;    
    
        
    // STATIC VARIABLES
    public static boolean isMinProblem;    
    public static Settings settings;        
    
    public static int PANEL_WIDTH = 756;
    public static int PANEL_HEIGHT = 395;
    
    public static PlotFrame plotFrame;        
    public static HashMap<Integer, Group> groups;
    
    public static Dimension screenSize;
    public static Dimension windowSize;
    public static LanguageManager languageManager;
    
    RootWindow rootWindow;
    JDialog jDgRanking;
    JDialog jDgPreferences;
    JDialog jDgAbout;
    
    
    private final javax.swing.JLabel jlblIconSelect;
    private final javax.swing.JLabel jlblIconZoom;
    
    
    /**
     * Creates new form PlotWindow
     */
    public PlotFrame(SolutionSet solutionSet, boolean isMinProblemParam) {        
        
        languageManager = new LanguageManager(new Locale("es"));             
        
        groups = new HashMap<>();
        
        toolSelected = ToolSelected.SELECT;
        zoomState = ZoomState.ZOOM_1;
        
    
        this.jlblIconSelect = new JLabel();                
        this.jlblIconZoom = new JLabel();
        this.jlblIconSelect.setSize(new Dimension(35, 35));
        this.jlblIconZoom.setSize(new Dimension(35, 35));
        
        this.jlblIconSelect.addMouseListener(
                new CustomMouseListener().setMouseClicked((MouseEvent e) -> {
                    jlblIconSelectMouseClicked(e);
        })
        );
        
        this.jlblIconZoom.addMouseListener(
                new CustomMouseListener().setMouseClicked((MouseEvent e) -> {
                    jlblIconZoomMouseClicked(e);
        })
        );
        
        this.jlblIconZoom.setIcon(ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("zoom-icon.png"), 
                this.jlblIconZoom.getWidth() - 5, 
                this.jlblIconZoom.getHeight() - 5));
        
        
        this.jlblIconSelect.setIcon(ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("select-icon.png"), 
                this.jlblIconSelect.getWidth() - 5, 
                this.jlblIconSelect.getHeight() - 5));
            
        changedTool();
        
        
        // INIT PANELS        
        solutionDraw = new SolutionDrawManager(solutionSet);        
        
        
        MouseListener mouseListener = getPlotMouseListener();
        MouseMotionListener mouseMotionListener = getPlotMouseMotionListener();
        
        plotParPanel = new PlotPanel(solutionDraw, PlotPanel.SolutionDrawMode.PARALLEL);
        plotParPanel.setName(PLOT_PAR_NAME);
        plotParPanel.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
        plotParPanel.setSize(PANEL_WIDTH, PANEL_HEIGHT);                
        plotParPanel.addMouseListener(mouseListener);
        plotParPanel.addMouseMotionListener(mouseMotionListener);
        
        plotRadPanel = new PlotPanel(solutionDraw, PlotPanel.SolutionDrawMode.RADVIZ);
        plotRadPanel.setName(PLOT_RAD_NAME);
        plotRadPanel.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
        plotRadPanel.setSize(PANEL_WIDTH, PANEL_HEIGHT);       
        plotRadPanel.addMouseListener(mouseListener);
        plotRadPanel.addMouseMotionListener(mouseMotionListener);
        
        isMinProblem = isMinProblemParam;
        
        settings = new Settings(solutionSet);                        
        solutionSet.recalculateShapes(isMinProblem);        
        
        selectionsPanel = new SelectionsPanel();                        
        groupListPanel = new GroupListPanel();                
        preferencesPanel = new PreferencesPanel();
        aboutPanelPanel = new AboutPanel();                
        filterListPanel = new FilterListPanel();    
        generalInfoPanel = new GeneralInfoPanel();                
        clusterizePanel = new ClusterizePanel();
        rankingPanel = new RankingPanel();
        
        filterListPanel.applyFilters();                
        
        jDgPreferences = new JDialog();
        jDgRanking = new JDialog();
        jDgAbout = new JDialog();
        
        initViews();     
        
        plotFrame = this;
    }
    
    
    
    private void initViews() {
     
        // GET AND DEFINE SCREEN AND WINDOW SIZES        
        screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        windowSize = new Dimension(new Double(screenSize.getWidth() - 50F).intValue(), 
                new Double(screenSize.getHeight() - 50F).intValue());
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
        setPreferredSize(windowSize);
        setTitle(languageManager.getMessage(LanguageKeys.MAIN_WINDOW_TITLE));
        
        // MENU        
        createMenu();
      
        // OTHER STUFF        
        ViewMap viewMap = new ViewMap();                          
        
        View[] views = new View[VIEWS_IDS.length];
        
        for (int i = 0; i < views.length; i++) {
            views[VIEWS_IDS[i]] = getView(VIEWS_IDS[i]);
            viewMap.addView(VIEWS_IDS[i], views[VIEWS_IDS[i]]);            
        }

        
        rootWindow = DockingUtil.createRootWindow(viewMap, true);                
        
        View tmpView = getView(TOOLS_VIEW_ID);
        RootWindow toolBarWindow = DockingUtil.createRootWindow(
                new ViewMap(new View[]{ tmpView })
                , false);
        
        toolBarWindow.setMaximizedWindow(tmpView);
        
        rootWindow.setWindow(new SplitWindow(false, 0.6097973f, 
                new TabWindow(new DockingWindow[]{
                    views[PLOT_PAR_VIEW_ID], 
                    views[PLOT_RAD_VIEW_ID]}),                
                new TabWindow(new DockingWindow[]{
                    views[CLUSTER_VIEW_ID], 
                    views[FILTER_VIEW_ID], 
                    views[SELECTION_VIEW_ID], 
                    views[GROUP_VIEW_ID]})));

        DockingWindowsTheme theme = new GradientDockingTheme();
        rootWindow.getRootWindowProperties().addSuperObject(theme.getRootWindowProperties());
        
        toolBarWindow.setPreferredSize(new Dimension(new Double(windowSize.getWidth()).intValue(), 50));
        toolBarWindow.setMinimumSize(new Dimension(new Double(windowSize.getWidth()).intValue(), 50));
        toolBarWindow.setMaximumSize(new Dimension(new Double(screenSize.getWidth()).intValue(), 50));
        toolBarWindow.setSize(new Dimension(new Double(windowSize.getWidth()).intValue(), 50));
        
        setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));        
        add(toolBarWindow);
        add(rootWindow);       
        
        
        pack();       
    }   
    
    
    private void createMenu() {
        
        JMenuBar jmb = new JMenuBar();
        JMenu jmnFile = new JMenu();
        JMenu jmnEdit = new JMenu();
        JMenu jmnRanking = new JMenu();        
        JMenu jmnCapture = new JMenu();
        JMenu jmnHelp = new JMenu();        
        JMenuItem jmnSelectAll = new JMenuItem();
        JMenuItem jmnDeselectAll = new JMenuItem();
        JMenuItem jmnPreferences = new JMenuItem();
        JMenuItem jmnRankingOptions = new JMenuItem();       
        JMenuItem jmnAbout = new JMenuItem();
        JMenuItem jmnParallel = new JMenuItem();
        JMenuItem jmnRadViz = new JMenuItem();
        JSeparator jSeparator = new JSeparator();
                

        jmnFile.setText("File");
        jmnEdit.setText("Edit");        
        jmnCapture.setText("Capture");
        jmnHelp.setText("Help");
        

        jmnSelectAll.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jmnSelectAll.setText("Select All");
        jmnSelectAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmnSelectAllActionPerformed(evt);
            }
        });
        

        jmnDeselectAll.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jmnDeselectAll.setText("Deselect All");
        jmnDeselectAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmnDeselectAllActionPerformed(evt);
            }
        });
        
        
        jmnPreferences.setText("Preferences");
        jmnPreferences.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmnPreferencesActionPerformed(evt);
            }
        });
        
        
        jmnEdit.add(jmnSelectAll);
        jmnEdit.add(jmnDeselectAll);
        jmnEdit.add(jSeparator);
        jmnEdit.add(jmnPreferences);

        
        
        jmnRanking.setText("Ranking");

        jmnRankingOptions.setText("Ranking options");
        jmnRankingOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmnRankingActionPerformed(evt);
            }
        });
        jmnRanking.add(jmnRankingOptions);
        
        jmnAbout.setText("About");
        jmnAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmnAboutActionPerformed(evt);
            }
        });
        jmnHelp.add(jmnAbout);
        
        jmnParallel.setText("Parallel coordinates plot");
        jmnParallel.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jmnParallel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    LocalDateTime ldt = LocalDateTime.now();
                    String dateTime = ldt.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss-SS"));
            
                    BufferedImage bi = ScreenImage.createImage(PlotFrame.plotParPanel);
                    String path = PlotFrame.settings.getCapturesLocation() + File.separator;
                    ScreenImage.writeImage(bi, String.format(path + "parallel %s.png", dateTime));
                } catch (IOException ex) {
                }
               
            }
        });
        jmnRadViz.setText("RadViz plot");
        jmnRadViz.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jmnRadViz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    LocalDateTime ldt = LocalDateTime.now();
                    String dateTime = ldt.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss-SS"));
                    
                    BufferedImage bi = ScreenImage.createImage(PlotFrame.plotRadPanel);
                    String path = PlotFrame.settings.getCapturesLocation() + File.separator;
                    ScreenImage.writeImage(bi, String.format(path + "radviz %s.png", dateTime));
                } catch (IOException ex) {
                }
               
            }
        });
        jmnCapture.add(jmnParallel);
        jmnCapture.add(jmnRadViz);
        
       // jmb.add(jmnFile);
        jmb.add(jmnEdit);
        jmb.add(jmnRanking);
        jmb.add(jmnCapture);
        jmb.add(jmnHelp);

        setJMenuBar(jmb);

    }
    
    private View getView(int ID) {
        switch (ID) {
            case TOOLS_VIEW_ID:
                JPanel jp = new JPanel();                
                jp.setLayout(new FlowLayout(FlowLayout.LEFT));
                jp.add(jlblIconSelect);
                jp.add(jlblIconZoom);                
                View view = new View("", null, jp);      
                view.getViewProperties().getViewTitleBarProperties()
                        .setVisible(false);
                return view;
                
            case GENERAL_VIEW_ID:
                return new View("General", null, getScrollableComponent(generalInfoPanel));
            case FILTER_VIEW_ID:
                return new View("Filters", null, getScrollableComponent(filterListPanel));
            case GROUP_VIEW_ID:
                return new View("Groups", null, getScrollableComponent(groupListPanel));
            case SELECTION_VIEW_ID:
                return new View("Selections", null, getScrollableComponent(selectionsPanel));
            case CLUSTER_VIEW_ID:            
                return new View("Clusters", null, getScrollableComponent(clusterizePanel));
            case PLOT_PAR_VIEW_ID:
                return new View("Parallel coordinates", null, plotParPanel);
            case PLOT_RAD_VIEW_ID:
                return new View("Radviz", null, plotRadPanel);
        }              
        return null;
    }
    
    private JScrollPane getScrollableComponent(Component component) {
        JScrollPane jScrollPane = new JScrollPane(component);
        return jScrollPane;
    }
    
    
    
    
    private PlotPanel getPlotPanelFromName(String name) {
        switch(name) {
            case PLOT_RAD_NAME: 
                return plotRadPanel;
            default:
                return plotParPanel;
        }
    }
    
    private MouseMotionListener getPlotMouseMotionListener() {
        return new MouseMotionListener() {
                                   
            
            @Override
            public void mouseDragged(MouseEvent e) {                                                
                
                JPanel panel = (JPanel)e.getComponent();
                PlotPanel plotPanel = getPlotPanelFromName(panel.getName());
                
                if (zoomState != ZoomState.ZOOM_2) return;
                
                plotPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.MOVE_CURSOR));
                
                int move_x = e.getX() - draggingX;
                int move_y = e.getY() - draggingY;
                
                
                draggingX = e.getX();
                draggingY = e.getY();
                
                move_x = plotPanel.zoom_moveX - move_x;
                move_y = plotPanel.zoom_moveY - move_y;
                
                
                plotPanel.zoom_moveX = move_x;
                plotPanel.zoom_moveY = move_y;
                
                redraw();
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                JPanel panel = (JPanel)e.getComponent();
                PlotPanel plotPanel = getPlotPanelFromName(panel.getName());
                
                if (toolSelected != ToolSelected.ZOOM) {
                    plotPanel.update(e);
                } else if (zoomState == ZoomState.ZOOM_1) {
    
                    plotPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));                                        
                    redraw();
                    
    
                    int zoom_width = new Double(ZOOM_PROP_WIDTH * plotPanel.getWidth()).intValue();
                    int zoom_height = new Double(ZOOM_PROP_HEIGHT * plotPanel.getHeight()).intValue();
            
                    zoom_width = Math.max(zoom_width, ZOOM_MIN_WIDTH);
                    zoom_height = Math.max(zoom_height, ZOOM_MIN_HEIGHT);
                    
                    int posx = new Double(e.getX()).intValue();
                    int posy = new Double(e.getY()).intValue();
                    
                    Graphics2D g2 = (Graphics2D)plotPanel.getImg().getGraphics();                    
                    g2.setColor(Color.BLACK);
    
                    int x0 = posx - zoom_width / 2;
                    int y0 = posy - zoom_height / 2;
                    int w0 = zoom_width;
                    int h0 = zoom_height;

                    if (x0 < 0) x0 = 0;
                    if (y0 < 0) y0 = 0;
                    if (x0 + zoom_width > plotPanel.getWidth()) x0 = plotPanel.getWidth() - zoom_width;
                    if (y0 + zoom_height > plotPanel.getHeight()) y0 = plotPanel.getHeight()- zoom_height;
                    
                    
                    g2.drawRect(x0, y0, w0, h0);
                    g2.dispose();
                    
                    plotPanel.revalidate();
                    plotPanel.repaint();
                }
            }
        };
    }
    
    private MouseListener getPlotMouseListener() {
        return new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JPanel panel = (JPanel)e.getComponent();
                PlotPanel plotPanel = getPlotPanelFromName(panel.getName());
                
                if (toolSelected == ToolSelected.ZOOM) {
                                        
                    switch(zoomState) {
                        case ZOOM_1: 
                        {
                            int width = new Double(ZOOM_PROP_WIDTH * plotPanel.getWidth()).intValue();
                            int height = new Double(ZOOM_PROP_HEIGHT * plotPanel.getHeight()).intValue();
                       
                            
                            width = Math.max(width, ZOOM_MIN_WIDTH);
                            height = Math.max(height, ZOOM_MIN_HEIGHT);
                            
                            zoomState = ZoomState.ZOOM_2;
                            
                            int from_x = e.getX() - width/2;
                            int to_x = from_x + width;
                            int from_y = e.getY() - height/2;
                            int to_y = from_y + height;
                            
                            if (from_x < 0) from_x = 0;
                            if (from_y < 0) from_y = 0;
                            if (to_x > panel.getWidth()) to_x = panel.getWidth();
                            if (to_y > panel.getHeight()) to_y = panel.getHeight();
                            
                            plotPanel.zoom  = true;                            
                            plotPanel.zoom_moveX = from_x;
                            plotPanel.zoom_moveY = from_y;
                            plotPanel.zoom_width = width;
                            plotPanel.zoom_height = height;
                            
                            
                            redraw();
                            
                            break;
                        } 
                        case ZOOM_2:                        
                        {                            
                            
                            zoomState = ZoomState.ZOOM_1;
                            plotPanel.zoom  = false;
                            plotPanel.zoom_moveX = 0;
                            plotPanel.zoom_moveY = 0;
                            redraw();
                            break;
                        } 
                    }
                    
                } else {                                        
                    plotPanel.updateClick(e);                                        
                }                    
            }

            @Override
            public void mousePressed(MouseEvent e) {                
                draggingX = e.getX();
                draggingY = e.getY();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                JPanel panel = (JPanel)e.getComponent();
                PlotPanel plotPanel = getPlotPanelFromName(panel.getName());
                
                plotPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
                JPanel panel = (JPanel)e.getComponent();
                PlotPanel plotPanel = getPlotPanelFromName(panel.getName());
                
                solutionDraw.setHoveredSolutions(new ArrayList<>());
                redraw();
            }
        };
    }
    
    public void updateInfo() {
        generalInfoPanel.updateInfo();
    }
    
       
    private void changedTool() {
        if (toolSelected == ToolSelected.ZOOM) {            
            this.jlblIconZoom.setBorder(
                    BorderFactory.createLineBorder(Color.BLACK, 2)
            );
            this.jlblIconSelect.setBorder(
                    BorderFactory.createEmptyBorder()
            );
        } else if (toolSelected == ToolSelected.SELECT) {
            this.jlblIconSelect.setBorder(
                    BorderFactory.createLineBorder(Color.BLACK, 2)
            );
            this.jlblIconZoom.setBorder(
                    BorderFactory.createEmptyBorder()
            );
        }
    }
    
    private void jlblIconZoomMouseClicked(java.awt.event.MouseEvent evt) {                                          
        toolSelected = ToolSelected.ZOOM;
        changedTool();
    }                                         

    private void jlblIconSelectMouseClicked(java.awt.event.MouseEvent evt) {                                            
        toolSelected = ToolSelected.SELECT;
        changedTool();
    }                                           

    private void jmnRankingActionPerformed(java.awt.event.ActionEvent evt) {                                           
        rankingPanel.resetRank();
        rankingPanel.updateList();
        rankingPanel.setSize(rankingPanel.getPreferredSize());
        rankingPanel.setVisible(true);
        this.jDgRanking.setSize(new Dimension(new Double(rankingPanel.getPreferredSize().getWidth()).intValue() + 15, new Double(rankingPanel.getPreferredSize().getHeight()).intValue() + 40));
        this.jDgRanking.add(rankingPanel);
        this.jDgRanking.setModal(true);
        this.jDgRanking.setResizable(false);
        this.jDgRanking.setVisible(true);
    }                                          

    private void jmnPreferencesActionPerformed(java.awt.event.ActionEvent evt) {                                               
        preferencesPanel.setSize(preferencesPanel.getPreferredSize());
        preferencesPanel.initializeFields(settings);
        preferencesPanel.setVisible(true);
        preferencesPanel.setParent(jDgPreferences);
        this.jDgPreferences.setSize(new Dimension(new Double(preferencesPanel.getPreferredSize().getWidth()).intValue() + 15, new Double(preferencesPanel.getPreferredSize().getHeight()).intValue() + 40));
        this.jDgPreferences.add(preferencesPanel);
        this.jDgPreferences.setModal(true);
        this.jDgPreferences.setResizable(false);
        this.jDgPreferences.setVisible(true);
    }   
    
    private void jmnAboutActionPerformed(java.awt.event.ActionEvent evt) {                                               
        aboutPanelPanel.setSize(aboutPanelPanel.getPreferredSize());
        aboutPanelPanel.setVisible(true);
        aboutPanelPanel.setParent(jDgAbout);
        this.jDgAbout.setSize(new Dimension(new Double(aboutPanelPanel.getPreferredSize().getWidth()).intValue() + 15, new Double(aboutPanelPanel.getPreferredSize().getHeight()).intValue() + 40));
        this.jDgAbout.add(aboutPanelPanel);
        this.jDgAbout.setModal(true);
        this.jDgAbout.setResizable(false);
        this.jDgAbout.setVisible(true);
    }   

    private void jmnDeselectAllActionPerformed(java.awt.event.ActionEvent evt) {                                               
        selectAllTrigger(SolutionDrawManager.SelectionMode.FORCE_DESELECT);
    }                                              

    private void jmnSelectAllActionPerformed(java.awt.event.ActionEvent evt) {                                             
        getDevelLayaout();
        selectAllTrigger(SolutionDrawManager.SelectionMode.FORCE_SELECT);
    }                                            

    
    private void selectAllTrigger(SolutionDrawManager.SelectionMode selectionMode) {
        
        SolutionSet solutionSet = solutionDraw.getSolutionSet();
        
        for(DrawEntry drawEntry : solutionDraw.getDrawingSet()) {                        
            
            for (Integer solutionId : drawEntry.getSolutionIds()) {
                SelectionEntry selectionEntry = solutionDraw.createSelectionEntry(solutionSet.getSolutionById(solutionId));
                solutionDraw.toogleSelection(selectionEntry, selectionMode);
            }            
            
        }
        selectionsPanel.setClickedSolutions(solutionDraw.getClickedSolutions());        
        redraw();
    }
    
    
  
    public static void redraw() {
        plotParPanel.redraw();
        plotRadPanel.redraw();
    }
    
    
    private enum ToolSelected {
        SELECT, 
        ZOOM
    }
    
    private enum ZoomState {
        ZOOM_1,
        ZOOM_2
    }
    
    
    
    private void getDevelLayaout() {
        //DeveloperUtil.createWindowLayoutFrame("My Main RootWindow", rootWindow).setVisible(true);
    }         
}
