/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.filters;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.CustomMouseListener;
import py.una.pol.setviz.utils.ImageUtils;
import py.una.pol.setviz.utils.filters.FilterEntry;
import py.una.pol.setviz.utils.filters.Filters;
import py.una.pol.setviz.utils.math.Graph;
import py.una.pol.setviz.utils.solution.Group;
import py.una.pol.setviz.utils.solution.Solution;

/**
 *
 * @author acost
 */
public class FilterListPanel extends javax.swing.JPanel {

    /**
     * Creates new form SelectionsPanel
     */

    public static ArrayList<FilterEntry> filterEntries;    
    public static ArrayList<Integer> filteredSetSolutionIds;
    
    boolean automaticUpdate;
    boolean applyFilters;
    
    ImageIcon removeIcon;    
    ImageIcon editIcon;    
    
    HashMap<Integer, JCheckBox> checkboxes;
    Set<Integer> selected;
    
    Graph priorityGraph;
    
    boolean updateCheckbox;    
    
    
    FilterEntry actualFilterEntryEdit;
    HashMap<String, Integer> groupIdByName;
    HashMap<String, Integer> objectiveIdByNamePos;
    HashMap<String, Integer> objectiveIdByNameVal;
    HashMap<String, Integer> objectiveIdByNamePriority1;
    HashMap<String, Integer> objectiveIdByNamePriority2;

    
    
    Set<Integer> alreadyUsedGroups;
    
    public FilterListPanel() {
        initComponents();
        
        priorityGraph = new Graph();
        
        
        filterEntries = new ArrayList<>();           
        
        alreadyUsedGroups = new HashSet<>();
        
        actualFilterEntryEdit = null;
        updateCheckbox = true;
        
        selected = new HashSet<>();
        
        removeIcon = ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("delete-icon.png"), 
                this.jlblRemoveSelected.getPreferredSize().width, 
                this.jlblRemoveSelected.getPreferredSize().height);
        
        editIcon = ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("edit-icon.png"), 
                this.jlblRemoveSelected.getPreferredSize().width, 
                this.jlblRemoveSelected.getPreferredSize().height);
                        
        this.jlblRemoveSelected.setIcon(removeIcon);                
        this.jlblRemoveSelected.setToolTipText("Remove selected");
        makeButtonStyle(jlblRemoveSelected);
        
        
        this.jlblNew.setIcon(ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("new-icon.png"), 
                this.jlblNew.getPreferredSize().width, 
                this.jlblNew.getPreferredSize().height));
        this.jlblNew.setToolTipText("Create new group");
        makeButtonStyle(jlblNew);
                        
        JScrollBar vertical = this.jScrollPane1.getVerticalScrollBar();
	InputMap verticalMap = vertical.getInputMap( JComponent.WHEN_IN_FOCUSED_WINDOW );
	verticalMap.put( KeyStroke.getKeyStroke( "DOWN" ), "positive_" );        
	verticalMap.put( KeyStroke.getKeyStroke( "UP" ), "negative_" );
                
        Action actionListener1 = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                
                JScrollBar jScrollBar = (JScrollBar) actionEvent.getSource();               
                jScrollBar.setValue(jScrollBar.getValue() + 20);
                
            }
        };
        
        Action actionListener2 = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                
                JScrollBar jScrollBar = (JScrollBar) actionEvent.getSource();               
                jScrollBar.setValue(jScrollBar.getValue() - 20);
                
            }
        };
        
        ActionMap actionMap = new ActionMap();
        actionMap.put("positive_", actionListener1);
        actionMap.put("negative_", actionListener2);
        this.jScrollPane1.getVerticalScrollBar().setActionMap(actionMap);
    
        
        this.jtfEditShape.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                checkTextFieldShape();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkTextFieldShape();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                checkTextFieldShape();
            }
                        
        });
        
        
        jtfEditObjValue.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                checkTextFieldValue();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkTextFieldValue();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                checkTextFieldValue();
            }
        });
        
        
        
        updateList();
    }

    public boolean isAutomaticUpdate() {
        return automaticUpdate;
    }

    public void setAutomaticUpdate(boolean automaticUpdate) {
        this.automaticUpdate = automaticUpdate;
        this.jcbAutomaticUpdate.setSelected(automaticUpdate);
    }

    public boolean isApplyFilters() {
        return applyFilters;
    }

    public void setApplyFilters(boolean applyFilters) {
        this.applyFilters = applyFilters;
        this.jcbApplyFilters.setSelected(applyFilters);
    }
    
    
    
    
    private void makeButtonStyle(JLabel component) {
        component.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        component.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        component.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
        component.setBackground(new Color(210,210,210));
        component.setCursor(new Cursor(Cursor.HAND_CURSOR));
        component.setPreferredSize(new Dimension(16,16));
        component.setOpaque(true);        
    }            
    
    private void checkTextFieldValue() {
        boolean valid = true;
        
        if (this.jtfEditObjValue.getText().trim().isEmpty()) valid = false;
        else {
            try {
                Double value = Double.parseDouble(this.jtfEditObjValue.getText());                
                if (value < 0 || value > 1) valid = false;
            } catch(NumberFormatException e) {
                valid = false;
            }
            
        }
        
        if (valid) {
            jtfEditObjValue.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        } else {
            jtfEditObjValue.setBorder(BorderFactory.createLineBorder(Color.RED));
        }
        jbtnAcceptEdition.setEnabled(valid);
    }
    
    private void checkTextFieldShape() {
        boolean valid = true;

        if (jtfEditShape.getText().trim().isEmpty()) valid = false;
        else {
            String[] values = jtfEditShape.getText().split(",");
            if (values.length != PlotFrame.settings.getShownObjectives().size()) valid = false;
            else {
                Set<Integer> positions = new HashSet<>();
                for(int i = 0; i < values.length; i++) {
                   if (values[i].equalsIgnoreCase("*")) continue;
                   try {
                        Integer value = Integer.parseInt(values[i]);
                        if (positions.contains(value)) valid = false;
                        if (value < 1 || value > PlotFrame.settings.getShownObjectives().size()) valid = false;
                        positions.add(value);
                   } catch(NumberFormatException exception) {
                       valid = false;
                   }

                }
            }
        }

        if (valid) {
            jtfEditShape.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        } else {
            jtfEditShape.setBorder(BorderFactory.createLineBorder(Color.RED));
        }

        jbtnAcceptEdition.setEnabled(valid);
    }
    
    
    private void checkSelectedButtons() {        
       this.jlblRemoveSelected.setEnabled(selected.size() > 0);
    }

    private void removeInvalidGroups() {
        ArrayList<FilterEntry> newSet = new ArrayList<>();
        
        for(FilterEntry filterEntry : filterEntries) {
            if (filterEntry.getFilterType() == FilterEntry.FilterType.GROUP) {
                if (PlotFrame.groups.keySet().contains(filterEntry.getGroupId())) {
                    newSet.add(filterEntry);
                }
            } else {
                newSet.add(filterEntry);
            }                
        
        
        }
        
        filterEntries = newSet;
    }            
    
    private void updateAlreadyUsed() {
        
        alreadyUsedGroups = new HashSet<>();
        
        for(FilterEntry filterEntry : filterEntries) {
            switch(filterEntry.getFilterType()) {
                case GROUP:
                    alreadyUsedGroups.add(filterEntry.getGroupId());
                    break;
            }
        }
        
    }
    
    private void removeDuplicatedShapes() {
        ArrayList<FilterEntry> newSet = new ArrayList<>();
        
        for(int i = 0; i < filterEntries.size(); i++) {            
            FilterEntry fe1 = filterEntries.get(i);
            boolean add = true;
            
            if (fe1.getFilterType() == FilterEntry.FilterType.SHAPE) {
                for(int j = i+1; j < filterEntries.size(); j++) {
                    if (i != j) {
                        FilterEntry fe2 = filterEntries.get(j);
                        if (fe2.getFilterType() == FilterEntry.FilterType.SHAPE) {
                            if (fe1.getShape().equalsIgnoreCase(fe2.getShape())) {
                                add = false;
                            }
                        }
                    }
                }
            }
            
            
            if (add) {
                newSet.add(fe1);
            }
            
        }
        
        filterEntries = newSet;
    }            
    
    public final void updateList() {
        updateList(false);
    }
    
    public final void updateList(boolean calledForGroupChanges) {
            
        int sizeBefore = filterEntries.size();
        removeInvalidGroups();
        int sizeAfter = filterEntries.size();
        checkSelectedButtons();
        removeDuplicatedShapes();
        updateAlreadyUsed();
        
        checkboxes = new HashMap<>();
        
        ArrayList<JPanel> rows = new ArrayList<>();
        
        
        int index = 0;
        for (FilterEntry filterEntry : filterEntries) {                                    
            JPanel row = createRow(index % 2 == 0, index, filterEntry);
            index++;
            rows.add(row);
        }
        
        this.jpnList.removeAll();
        this.jpnList.revalidate();
        this.jpnList.repaint();
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.jpnList);
        this.jpnList.setLayout(layout);                       
        
        
        GroupLayout.ParallelGroup pG = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
        for (JPanel row : rows) {
            pG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
        }                            
                
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pG)
                .addContainerGap())
        );
        
        GroupLayout.SequentialGroup sG = layout.createSequentialGroup().addContainerGap();
        
        for (JPanel row : rows) {            
            sG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25);
        }
        
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sG)
        );
              
        
        
        
        if (calledForGroupChanges && sizeBefore == sizeAfter) return;        
        if (!applyFilters || !automaticUpdate) return;
        applyFilters();
        
    }
    
    
    public void applyFilters() {                                
        
        filteredSetSolutionIds = new ArrayList<>();        
        ArrayList<Solution> newSet;
        if (applyFilters) {
            newSet = Filters.applyFilters(PlotFrame.solutionDraw.getSolutionSet().getSolutions(), filterEntries);
        } else {
            newSet = Filters.applyFilters(PlotFrame.solutionDraw.getSolutionSet().getSolutions(), new ArrayList<>());
        }        
        for (Solution sol : newSet) {
            filteredSetSolutionIds.add(sol.getId());
        }
        
        PlotFrame.clusterizePanel.updateList();
        PlotFrame.selectionsPanel.updateList();
        PlotFrame.redraw();
    }
    
    
    private JPanel createRow(boolean color, int index, FilterEntry filterEntry) {
        
        JPanel jpnRow = new javax.swing.JPanel();

        JCheckBox jckSelect = new JCheckBox();
        JTextField jtfFilterStr = new JTextField();
        JLabel jlblEdit = new javax.swing.JLabel();       
        JLabel jlblRemove = new javax.swing.JLabel();       


        jckSelect.setSelected(selected.contains(index));        
        jckSelect.setText("");
        jckSelect.setName("ck_" + index);
        jckSelect.setPreferredSize(new Dimension(14, 16));
        jckSelect.setOpaque(false);
        jckSelect.addChangeListener((ChangeEvent e) -> {
            if (!updateCheckbox) return;
            
            JCheckBox jc = (JCheckBox)e.getSource();
            int id = Integer.parseInt(jc.getName().split("_")[1]);
            if (jc.isSelected()) {
                selected.add(id);
            } else {
                selected.remove(id);
            }
            
            checkSelectedButtons();
        });
        checkboxes.put(index, jckSelect);
        
        jtfFilterStr.setText(filterEntry.toString());
        jtfFilterStr.setToolTipText(filterEntry.toString());
        jtfFilterStr.setEditable(false);
        jtfFilterStr.setBorder(BorderFactory.createEmptyBorder());
        jtfFilterStr.setOpaque(false);
        
        jlblEdit.setPreferredSize(new Dimension(14, 14));
        jlblEdit.setIcon(editIcon);        
        jlblEdit.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jlblEdit.setToolTipText("Edit filter");
        jlblEdit.setName("edit_" + index);
        jlblEdit.addMouseListener(
            new CustomMouseListener().setMouseClicked((MouseEvent e) -> {
                int idx = Integer.parseInt(e.getComponent().getName().split("_")[1]);
                
                FilterEntry fe = filterEntries.get(idx);
                actualFilterEntryEdit = fe;
                initializeEdit();
                this.jDgEdit.setVisible(true);
            })
        );
        
        
                
        jlblRemove.setPreferredSize(new Dimension(14, 14));
        jlblRemove.setIcon(removeIcon);        
        jlblRemove.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jlblRemove.setToolTipText("Remove");
        jlblRemove.setName("rem_" + index);
        jlblRemove.addMouseListener(
            new CustomMouseListener().setMouseClicked((MouseEvent e) -> {
                int idx = Integer.parseInt(e.getComponent().getName().split("_")[1]);
        
                FilterEntry fe = filterEntries.get(idx);
                if (null != fe.getFilterType()) switch (fe.getFilterType()) {
                case GROUP:
                    alreadyUsedGroups.remove(fe.getGroupId());
                    break;
                 case OBJ_PRIORITY:
                    priorityGraph.removeDirectedEdge("" + fe.getObjectiveIndex(), 
                            "" + fe.getObjectiveIndexTwo());
                    break;    
                }
                
                                                
                selected.remove(idx);
                for(Integer i : selected) {
                    if(i > idx) {
                        selected.remove(i);
                        selected.add(i-1);
                    }
                }
                
                filterEntries.remove(idx);
                updateList();
            })
        );
        
        
        javax.swing.GroupLayout jpnRowLayout = new javax.swing.GroupLayout(jpnRow);
        jpnRow.setLayout(jpnRowLayout);
        jpnRowLayout.setHorizontalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnRowLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jckSelect, 18, 18, 18)                
                .addGap(20,20,20)
                .addComponent(jlblEdit, 20, 20, 20)
                .addGap(5,5,5)
                .addComponent(jlblRemove, 20, 20, 20)
                .addGap(20, 20, 20)
                .addComponent(jtfFilterStr, 250, 250, 250)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)                
                
                .addContainerGap())
        );
        jpnRowLayout.setVerticalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jckSelect, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 19, 19)
            .addComponent(jtfFilterStr, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            .addComponent(jlblEdit, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)                    
            .addComponent(jlblRemove, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
                            
        );
               
        if (color) jpnRow.setBackground(new java.awt.Color(230, 230, 230));        

        return jpnRow;
    }
    
    
    private void initializeEdit() {
        
        
        this.jcbFilterType.removeAllItems();
        this.jcbFilterType.addItem(FilterEntry.filterTypeToString(FilterEntry.FilterType.SHAPE));
        if (alreadyUsedGroups.size() < PlotFrame.groups.size() || (actualFilterEntryEdit != null && actualFilterEntryEdit.getFilterType() == FilterEntry.FilterType.GROUP)) {
            this.jcbFilterType.addItem(FilterEntry.filterTypeToString(FilterEntry.FilterType.GROUP));
        }
        this.jcbFilterType.addItem(FilterEntry.filterTypeToString(FilterEntry.FilterType.OBJ_POSITION));
        this.jcbFilterType.addItem(FilterEntry.filterTypeToString(FilterEntry.FilterType.OBJ_VALUE));
        this.jcbFilterType.addItem(FilterEntry.filterTypeToString(FilterEntry.FilterType.OBJ_PRIORITY));


        
        initializeEditGroup();
        initializeEditShape();
        initializeEditObjPosValPriority();
     
        
        this.jbtnAcceptEdition.setEnabled(false);
        
        if (actualFilterEntryEdit != null) {
            this.jcbFilterType.setSelectedItem(FilterEntry.filterTypeToString(actualFilterEntryEdit.getFilterType()));            
        }
        
    }
    
    
    
    private void initializeEditGroup() {
        
        
        this.jcbEditGroupName.removeAllItems();
        
        groupIdByName = new HashMap<>();
        for(Group group : PlotFrame.groups.values()) {
            if (!alreadyUsedGroups.contains(group.getGroupId())) {
                groupIdByName.put(group.getGroupName(), group.getGroupId());
                this.jcbEditGroupName.addItem(group.getGroupName());
            }
        }
        
        this.jcbEditGroupCondition.removeAllItems();
        this.jcbEditGroupCondition.addItem(FilterEntry.filterGroupConditionToString(FilterEntry.FilterGroupCondition.IN));
        this.jcbEditGroupCondition.addItem(FilterEntry.filterGroupConditionToString(FilterEntry.FilterGroupCondition.OR_IN));
        this.jcbEditGroupCondition.addItem(FilterEntry.filterGroupConditionToString(FilterEntry.FilterGroupCondition.NOT_IN));
        
        if (actualFilterEntryEdit != null && actualFilterEntryEdit.getFilterType() == FilterEntry.FilterType.GROUP) {                       
                        
            String groupName = PlotFrame.groups.get(actualFilterEntryEdit.getGroupId()).getGroupName();            
                    
            
            this.jcbEditGroupName.addItem(groupName);
            groupIdByName.put(groupName, actualFilterEntryEdit.getGroupId());
            
            this.jcbEditGroupName.setSelectedItem(groupName);
            this.jcbEditGroupCondition.setSelectedItem(
                    FilterEntry.filterGroupConditionToString(actualFilterEntryEdit.getFilterGroupCondition())
            );
        }
       
    }
            
    private void initializeEditShape() {
        this.jtfEditShape.setText("");
        if (actualFilterEntryEdit != null && actualFilterEntryEdit.getFilterType() == FilterEntry.FilterType.SHAPE) {
            this.jtfEditShape.setText(actualFilterEntryEdit.getShape());
        }
    }
    
 
    
    
    private void initializeEditObjPosValPriority() {
    
        objectiveIdByNamePos = new HashMap<>();
        objectiveIdByNameVal = new HashMap<>();
        objectiveIdByNamePriority1 = new HashMap<>();
        
        this.jcbEditObjPosObjective.removeAllItems();
        this.jcbEditObjValueObjective.removeAllItems();
        this.jcbEditObjPosValue.removeAllItems();
        this.jcbEditObjPriorityObjective1.removeAllItems();

        int realObjPos = 1;
        for(int i = 0; i < PlotFrame.settings.getObjectiveNames().size(); i++) {
            if (!PlotFrame.settings.getShownObjectives().contains(i)) continue;
            
            String objName = PlotFrame.settings.getObjectiveNames().get(i);
            
            objectiveIdByNamePos.put(objName, i);
            this.jcbEditObjPosObjective.addItem(objName);


            objectiveIdByNameVal.put(objName, i);
            this.jcbEditObjValueObjective.addItem(objName);
            
            objectiveIdByNamePriority1.put(objName, i);
            this.jcbEditObjPriorityObjective1.addItem(objName);
            
            this.jcbEditObjPosValue.addItem("" + realObjPos++);
        }                

        
        
        this.jcbEditObjPosPosition.removeAllItems();
        this.jcbEditObjPosPosition.addItem(FilterEntry.filterPositionTypeToString(FilterEntry.FilterPositionType.AT));
        this.jcbEditObjPosPosition.addItem(FilterEntry.filterPositionTypeToString(FilterEntry.FilterPositionType.NOT_AT));
        this.jcbEditObjPosPosition.addItem(FilterEntry.filterPositionTypeToString(FilterEntry.FilterPositionType.IN_FIRST));
        this.jcbEditObjPosPosition.addItem(FilterEntry.filterPositionTypeToString(FilterEntry.FilterPositionType.IN_LAST));
        
        this.jcbEditObjValueCondition.removeAllItems();
        this.jcbEditObjValueCondition.addItem(FilterEntry.filterValueConditionToString(FilterEntry.FilterValueCondition.EQ));
        this.jcbEditObjValueCondition.addItem(FilterEntry.filterValueConditionToString(FilterEntry.FilterValueCondition.NEQ));
        this.jcbEditObjValueCondition.addItem(FilterEntry.filterValueConditionToString(FilterEntry.FilterValueCondition.LT));
        this.jcbEditObjValueCondition.addItem(FilterEntry.filterValueConditionToString(FilterEntry.FilterValueCondition.GT));
        this.jcbEditObjValueCondition.addItem(FilterEntry.filterValueConditionToString(FilterEntry.FilterValueCondition.LET));
        this.jcbEditObjValueCondition.addItem(FilterEntry.filterValueConditionToString(FilterEntry.FilterValueCondition.GET));
        
        this.jtfEditObjValue.setText("");
        
        if (actualFilterEntryEdit != null) {
                                    
            if (actualFilterEntryEdit.getFilterType() == FilterEntry.FilterType.OBJ_POSITION) {
                
                String objname = PlotFrame.settings.getObjectiveNames().get(actualFilterEntryEdit.getObjectiveIndex());
                                
                this.jcbEditObjPosObjective.setSelectedItem(objname);
                this.jcbEditObjPosPosition.setSelectedItem(
                        FilterEntry.filterPositionTypeToString(actualFilterEntryEdit.getFilterPositionType()));
                this.jcbEditObjPosValue.setSelectedItem("" + actualFilterEntryEdit.getValue().intValue());
            }
            
            if (actualFilterEntryEdit.getFilterType() == FilterEntry.FilterType.OBJ_VALUE) {
                
                String objname = PlotFrame.settings.getObjectiveNames().get(actualFilterEntryEdit.getObjectiveIndex());                                
                                                
                this.jcbEditObjValueObjective.setSelectedItem(objname);
                this.jcbEditObjValueCondition.setSelectedItem(
                        FilterEntry.filterValueConditionToString(actualFilterEntryEdit.getFilterValueCondition()));
                this.jtfEditObjValue.setText("" + actualFilterEntryEdit.getValue());
            }
            
            if (actualFilterEntryEdit.getFilterType() == FilterEntry.FilterType.OBJ_PRIORITY) {
                
                String objname1 = PlotFrame.settings.getObjectiveNames().get(actualFilterEntryEdit.getObjectiveIndex());                                
                String objname2 = PlotFrame.settings.getObjectiveNames().get(actualFilterEntryEdit.getObjectiveIndexTwo());                                
                
                
                this.jcbEditObjPriorityObjective1.setSelectedItem(objname1);
                this.jcbEditObjPriorityObjective2.setSelectedItem(objname2);
                
            }
            
            
        }
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDgEdit = new javax.swing.JDialog();
        jpnEditPanel = new javax.swing.JPanel();
        jbtnAcceptEdition = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jcbFilterType = new javax.swing.JComboBox<>();
        jSeparator1 = new javax.swing.JSeparator();
        jpnEditShapePanel = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jtfEditShape = new javax.swing.JTextField();
        jpnEditGroupPanel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jcbEditGroupCondition = new javax.swing.JComboBox<>();
        jcbEditGroupName = new javax.swing.JComboBox<>();
        jpnEditObjPosPanel = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jcbEditObjPosPosition = new javax.swing.JComboBox<>();
        jcbEditObjPosObjective = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jcbEditObjPosValue = new javax.swing.JComboBox<>();
        jpnEditObjValuePanel = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jcbEditObjValueCondition = new javax.swing.JComboBox<>();
        jcbEditObjValueObjective = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        jtfEditObjValue = new javax.swing.JTextField();
        jpnEditObjPriorityPanel = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jcbEditObjPriorityObjective1 = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();
        jcbEditObjPriorityObjective2 = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        jcbSelectAll = new javax.swing.JCheckBox();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jpnList = new javax.swing.JPanel();
        jlblNew = new javax.swing.JLabel();
        jlblRemoveSelected = new javax.swing.JLabel();
        jcbApplyFilters = new javax.swing.JCheckBox();
        jcbAutomaticUpdate = new javax.swing.JCheckBox();

        jDgEdit.setMinimumSize(new java.awt.Dimension(343, 287));
        jDgEdit.setModal(true);
        jDgEdit.setResizable(false);

        jpnEditPanel.setPreferredSize(new java.awt.Dimension(313, 160));

        javax.swing.GroupLayout jpnEditPanelLayout = new javax.swing.GroupLayout(jpnEditPanel);
        jpnEditPanel.setLayout(jpnEditPanelLayout);
        jpnEditPanelLayout.setHorizontalGroup(
            jpnEditPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 323, Short.MAX_VALUE)
        );
        jpnEditPanelLayout.setVerticalGroup(
            jpnEditPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 138, Short.MAX_VALUE)
        );

        jbtnAcceptEdition.setText("Accept");
        jbtnAcceptEdition.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnAcceptEditionActionPerformed(evt);
            }
        });

        jPanel2.setMinimumSize(new java.awt.Dimension(313, 30));
        jPanel2.setPreferredSize(new java.awt.Dimension(313, 30));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Filter type:");
        jLabel1.setMaximumSize(new java.awt.Dimension(36, 16));
        jLabel1.setMinimumSize(new java.awt.Dimension(36, 16));
        jLabel1.setPreferredSize(new java.awt.Dimension(36, 16));

        jcbFilterType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jcbFilterType.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcbFilterTypeItemStateChanged(evt);
            }
        });
        jcbFilterType.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jcbFilterTypeMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jcbFilterType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                .addComponent(jcbFilterType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jDgEditLayout = new javax.swing.GroupLayout(jDgEdit.getContentPane());
        jDgEdit.getContentPane().setLayout(jDgEditLayout);
        jDgEditLayout.setHorizontalGroup(
            jDgEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgEditLayout.createSequentialGroup()
                .addGroup(jDgEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDgEditLayout.createSequentialGroup()
                        .addGap(137, 268, Short.MAX_VALUE)
                        .addComponent(jbtnAcceptEdition))
                    .addGroup(jDgEditLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jDgEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jpnEditPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jDgEditLayout.setVerticalGroup(
            jDgEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgEditLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jpnEditPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbtnAcceptEdition)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jpnEditShapePanel.setMinimumSize(new java.awt.Dimension(313, 160));
        jpnEditShapePanel.setPreferredSize(new java.awt.Dimension(313, 160));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Shape:");

        javax.swing.GroupLayout jpnEditShapePanelLayout = new javax.swing.GroupLayout(jpnEditShapePanel);
        jpnEditShapePanel.setLayout(jpnEditShapePanelLayout);
        jpnEditShapePanelLayout.setHorizontalGroup(
            jpnEditShapePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnEditShapePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jtfEditShape)
                .addContainerGap())
        );
        jpnEditShapePanelLayout.setVerticalGroup(
            jpnEditShapePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnEditShapePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnEditShapePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jtfEditShape, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(129, Short.MAX_VALUE))
        );

        jpnEditGroupPanel.setMinimumSize(new java.awt.Dimension(313, 160));
        jpnEditGroupPanel.setPreferredSize(new java.awt.Dimension(313, 160));

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Group: name");

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Condition:");

        jcbEditGroupCondition.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jcbEditGroupName.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout jpnEditGroupPanelLayout = new javax.swing.GroupLayout(jpnEditGroupPanel);
        jpnEditGroupPanel.setLayout(jpnEditGroupPanelLayout);
        jpnEditGroupPanelLayout.setHorizontalGroup(
            jpnEditGroupPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnEditGroupPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnEditGroupPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnEditGroupPanelLayout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcbEditGroupName, 0, 174, Short.MAX_VALUE))
                    .addGroup(jpnEditGroupPanelLayout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcbEditGroupCondition, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jpnEditGroupPanelLayout.setVerticalGroup(
            jpnEditGroupPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnEditGroupPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnEditGroupPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jcbEditGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jpnEditGroupPanelLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnEditGroupPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jcbEditGroupCondition)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(103, Short.MAX_VALUE))
        );

        jpnEditObjPosPanel.setMinimumSize(new java.awt.Dimension(313, 160));

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Objective:");

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Position:");

        jcbEditObjPosPosition.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jcbEditObjPosObjective.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Value:");

        jcbEditObjPosValue.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout jpnEditObjPosPanelLayout = new javax.swing.GroupLayout(jpnEditObjPosPanel);
        jpnEditObjPosPanel.setLayout(jpnEditObjPosPanelLayout);
        jpnEditObjPosPanelLayout.setHorizontalGroup(
            jpnEditObjPosPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnEditObjPosPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnEditObjPosPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnEditObjPosPanelLayout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcbEditObjPosObjective, 0, 174, Short.MAX_VALUE))
                    .addGroup(jpnEditObjPosPanelLayout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcbEditObjPosPosition, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jpnEditObjPosPanelLayout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcbEditObjPosValue, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jpnEditObjPosPanelLayout.setVerticalGroup(
            jpnEditObjPosPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnEditObjPosPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnEditObjPosPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jcbEditObjPosObjective, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jpnEditObjPosPanelLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnEditObjPosPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbEditObjPosPosition, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnEditObjPosPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jcbEditObjPosValue)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(76, Short.MAX_VALUE))
        );

        jpnEditObjValuePanel.setMinimumSize(new java.awt.Dimension(313, 160));

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Objective:");

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Condition:");

        jcbEditObjValueCondition.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jcbEditObjValueObjective.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Value:");

        javax.swing.GroupLayout jpnEditObjValuePanelLayout = new javax.swing.GroupLayout(jpnEditObjValuePanel);
        jpnEditObjValuePanel.setLayout(jpnEditObjValuePanelLayout);
        jpnEditObjValuePanelLayout.setHorizontalGroup(
            jpnEditObjValuePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnEditObjValuePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnEditObjValuePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnEditObjValuePanelLayout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcbEditObjValueObjective, 0, 174, Short.MAX_VALUE))
                    .addGroup(jpnEditObjValuePanelLayout.createSequentialGroup()
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcbEditObjValueCondition, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jpnEditObjValuePanelLayout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jtfEditObjValue)))
                .addContainerGap())
        );
        jpnEditObjValuePanelLayout.setVerticalGroup(
            jpnEditObjValuePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnEditObjValuePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnEditObjValuePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jcbEditObjValueObjective, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jpnEditObjValuePanelLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnEditObjValuePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbEditObjValueCondition, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnEditObjValuePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfEditObjValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(76, Short.MAX_VALUE))
        );

        jpnEditObjPriorityPanel.setMinimumSize(new java.awt.Dimension(313, 160));

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Objective:");

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel13.setText("before");

        jcbEditObjPriorityObjective1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jcbEditObjPriorityObjective1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcbEditObjPriorityObjective1ItemStateChanged(evt);
            }
        });

        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("Objective:");

        jcbEditObjPriorityObjective2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout jpnEditObjPriorityPanelLayout = new javax.swing.GroupLayout(jpnEditObjPriorityPanel);
        jpnEditObjPriorityPanel.setLayout(jpnEditObjPriorityPanelLayout);
        jpnEditObjPriorityPanelLayout.setHorizontalGroup(
            jpnEditObjPriorityPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnEditObjPriorityPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnEditObjPriorityPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnEditObjPriorityPanelLayout.createSequentialGroup()
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcbEditObjPriorityObjective1, 0, 174, Short.MAX_VALUE))
                    .addGroup(jpnEditObjPriorityPanelLayout.createSequentialGroup()
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jpnEditObjPriorityPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jcbEditObjPriorityObjective2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jpnEditObjPriorityPanelLayout.setVerticalGroup(
            jpnEditObjPriorityPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnEditObjPriorityPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnEditObjPriorityPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jcbEditObjPriorityObjective1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jpnEditObjPriorityPanelLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnEditObjPriorityPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jcbEditObjPriorityObjective2)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(76, Short.MAX_VALUE))
        );

        setPreferredSize(new java.awt.Dimension(840, 194));

        jcbSelectAll.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jcbSelectAllMouseClicked(evt);
            }
        });

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Filter");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jcbSelectAll)
                .addGap(85, 85, 85)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jcbSelectAll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jpnListLayout = new javax.swing.GroupLayout(jpnList);
        jpnList.setLayout(jpnListLayout);
        jpnListLayout.setHorizontalGroup(
            jpnListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 818, Short.MAX_VALUE)
        );
        jpnListLayout.setVerticalGroup(
            jpnListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(jpnList);

        jlblNew.setPreferredSize(new java.awt.Dimension(14, 14));
        jlblNew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlblNewMouseClicked(evt);
            }
        });

        jlblRemoveSelected.setEnabled(false);
        jlblRemoveSelected.setPreferredSize(new java.awt.Dimension(14, 14));
        jlblRemoveSelected.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlblRemoveSelectedMouseClicked(evt);
            }
        });

        jcbApplyFilters.setText("Apply filters");
        jcbApplyFilters.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcbApplyFiltersItemStateChanged(evt);
            }
        });

        jcbAutomaticUpdate.setText("Automatic Update");
        jcbAutomaticUpdate.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcbAutomaticUpdateItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jlblNew, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlblRemoveSelected, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcbAutomaticUpdate)
                        .addGap(18, 18, 18)
                        .addComponent(jcbApplyFilters)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jlblNew, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jlblRemoveSelected, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jcbApplyFilters)
                        .addComponent(jcbAutomaticUpdate)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jcbSelectAllMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jcbSelectAllMouseClicked
        
        updateCheckbox = false;
        JCheckBox source = (JCheckBox)evt.getSource();
        for(Integer id : checkboxes.keySet()) {
            JCheckBox ck = checkboxes.get(id);
            ck.setSelected(source.isSelected());
            if (source.isSelected()) selected.add(id);
            else selected.remove(id);
        }
        
        checkSelectedButtons();
        updateCheckbox = true;
                
    }//GEN-LAST:event_jcbSelectAllMouseClicked

    private void jlblNewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlblNewMouseClicked
        actualFilterEntryEdit = null;
        initializeEdit();
        this.jDgEdit.setVisible(true);
    }//GEN-LAST:event_jlblNewMouseClicked
    
    private void jlblRemoveSelectedMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlblRemoveSelectedMouseClicked
        ArrayList<FilterEntry> newSet = new ArrayList<>();
        
        for(int i = 0; i < filterEntries.size(); i++) {
            if (selected.contains(i)) {
                selected.remove(i);
                
                FilterEntry fe = filterEntries.get(i);
                if (null != fe.getFilterType()) switch (fe.getFilterType()) {
                case GROUP:
                    alreadyUsedGroups.remove(fe.getGroupId());
                    break;
                case OBJ_PRIORITY:
                    priorityGraph.removeDirectedEdge("" + fe.getObjectiveIndex(), 
                            "" + fe.getObjectiveIndexTwo());
                    break;
                }
                
            } else {
                newSet.add(filterEntries.get(i));
            }
        }
        
        filterEntries = newSet;
        updateList();
    }//GEN-LAST:event_jlblRemoveSelectedMouseClicked

    private void jbtnAcceptEditionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnAcceptEditionActionPerformed
        
        FilterEntry.FilterType filterType = FilterEntry.getFilterTypeFromStr((String)this.jcbFilterType.getSelectedItem());
        boolean addNew = false;
        
        if (actualFilterEntryEdit == null) {
            actualFilterEntryEdit = new FilterEntry();
            addNew = true;
        }
        
        actualFilterEntryEdit.setFilterType(filterType);
        switch(filterType) {
            case GROUP:
                actualFilterEntryEdit.setGroupId(groupIdByName.get((String)this.jcbEditGroupName.getSelectedItem()));                
                actualFilterEntryEdit.setFilterGroupCondition(
                        FilterEntry.getFilterGroupConditionFromStr(
                                (String)this.jcbEditGroupCondition.getSelectedItem()));                
                break;
            case OBJ_POSITION:
                actualFilterEntryEdit.setObjectiveIndex(
                        this.objectiveIdByNamePos.get((String)this.jcbEditObjPosObjective.getSelectedItem()));
                actualFilterEntryEdit.setFilterPositionType(
                        FilterEntry.getFilterPositionTypeFromStr((
                                String)this.jcbEditObjPosPosition.getSelectedItem()));
                actualFilterEntryEdit.setValue(Double.parseDouble((String)this.jcbEditObjPosValue.getSelectedItem()));
                break;
            case OBJ_VALUE:
                actualFilterEntryEdit.setObjectiveIndex(
                        this.objectiveIdByNameVal.get((String)this.jcbEditObjValueObjective.getSelectedItem()));
                actualFilterEntryEdit.setFilterValueCondition(
                        FilterEntry.getFilterValueConditionFromStr(
                                (String)this.jcbEditObjValueCondition.getSelectedItem()));
                actualFilterEntryEdit.setValue(Double.parseDouble(this.jtfEditObjValue.getText()));
                break;
            case SHAPE:
                actualFilterEntryEdit.setShape(this.jtfEditShape.getText());
                break;
            case OBJ_PRIORITY:
                
                if (!addNew) {
                    filterEntries.remove(actualFilterEntryEdit);
                    priorityGraph.removeDirectedEdge("" + actualFilterEntryEdit.getObjectiveIndex(), 
                        "" + actualFilterEntryEdit.getObjectiveIndexTwo());
                
                }
                
                
                actualFilterEntryEdit.setObjectiveIndex(
                        this.objectiveIdByNamePriority1.get((String)this.jcbEditObjPriorityObjective1.getSelectedItem()));
                actualFilterEntryEdit.setObjectiveIndexTwo(
                        this.objectiveIdByNamePriority2.get((String)this.jcbEditObjPriorityObjective2.getSelectedItem()));
                
                priorityGraph.addNode("" + actualFilterEntryEdit.getObjectiveIndex());
                priorityGraph.addNode("" + actualFilterEntryEdit.getObjectiveIndexTwo());
                
                addNew = priorityGraph.addDirectedEdge("" + actualFilterEntryEdit.getObjectiveIndex(), 
                        "" + actualFilterEntryEdit.getObjectiveIndexTwo());
                                
                if (addNew) {
                    if (priorityGraph.hasCycle()) {
                        priorityGraph.removeDirectedEdge("" + actualFilterEntryEdit.getObjectiveIndex(), 
                        "" + actualFilterEntryEdit.getObjectiveIndexTwo());
                         
                        JOptionPane.showMessageDialog(PlotFrame.plotFrame, "You can't add conflictive relationships!", "Error", JOptionPane.ERROR_MESSAGE);
                        addNew = false;
                    }
                }
                
                break;
        }
        
        if (addNew) {            
            filterEntries.add(actualFilterEntryEdit);            
        }
                
        updateList();
        this.jDgEdit.dispose();
    }//GEN-LAST:event_jbtnAcceptEditionActionPerformed

    
    
    
        
    
    private void jcbFilterTypeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jcbFilterTypeMouseClicked
        
        
        
    }//GEN-LAST:event_jcbFilterTypeMouseClicked

    private void jcbFilterTypeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcbFilterTypeItemStateChanged
        
        if (evt.getStateChange() != ItemEvent.SELECTED) return;
        
        FilterEntry.FilterType filterType = FilterEntry.getFilterTypeFromStr((String)evt.getItem());
        JPanel component = null;
        
        switch(filterType) {
            
            case GROUP:             
                this.jbtnAcceptEdition.setEnabled(true);
                component = this.jpnEditGroupPanel;                
                break;
            case SHAPE:                
                checkTextFieldShape();
                component = this.jpnEditShapePanel;                
                break;
            case OBJ_POSITION:                
                this.jbtnAcceptEdition.setEnabled(true);
                component = this.jpnEditObjPosPanel;
                break;
            case OBJ_VALUE:                             
                checkTextFieldValue();
                component = this.jpnEditObjValuePanel;
                break;
            case OBJ_PRIORITY:                             
                this.jbtnAcceptEdition.setEnabled(true);
                component = this.jpnEditObjPriorityPanel;
                break;
            
        }        
        
        if (component != null) {
            this.jpnEditPanel.removeAll();
            this.jpnEditPanel.revalidate();
            this.jpnEditPanel.repaint();
            component.setSize(this.jpnEditPanel.getPreferredSize());
            component.setVisible(true);
            this.jpnEditPanel.add(component);                
            this.jpnEditPanel.revalidate();
            this.jpnEditPanel.repaint();
        }
        
        
    }//GEN-LAST:event_jcbFilterTypeItemStateChanged

    private void jcbApplyFiltersItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcbApplyFiltersItemStateChanged
        
        this.applyFilters = this.jcbApplyFilters.isSelected();
        applyFilters();
    }//GEN-LAST:event_jcbApplyFiltersItemStateChanged

    private void jcbAutomaticUpdateItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcbAutomaticUpdateItemStateChanged
        this.automaticUpdate = this.jcbAutomaticUpdate.isSelected();
        if (applyFilters && automaticUpdate) applyFilters();
    }//GEN-LAST:event_jcbAutomaticUpdateItemStateChanged

    private void jcbEditObjPriorityObjective1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcbEditObjPriorityObjective1ItemStateChanged

        
        this.objectiveIdByNamePriority2 = new HashMap<>();
        this.jcbEditObjPriorityObjective2.removeAllItems();

        String obj1 = (String)this.jcbEditObjPriorityObjective1.getSelectedItem();
        
         for(int i = 0; i < PlotFrame.settings.getObjectiveNames().size(); i++) {
            if (!PlotFrame.settings.getShownObjectives().contains(i)) continue;
            
            String objName = PlotFrame.settings.getObjectiveNames().get(i);
           
            if (objName.equalsIgnoreCase(obj1)) continue;
            
            
            objectiveIdByNamePriority2.put(objName, i);
            this.jcbEditObjPriorityObjective2.addItem(objName);
            
        }                

        
    }//GEN-LAST:event_jcbEditObjPriorityObjective1ItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDialog jDgEdit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JButton jbtnAcceptEdition;
    private javax.swing.JCheckBox jcbApplyFilters;
    private javax.swing.JCheckBox jcbAutomaticUpdate;
    private javax.swing.JComboBox<String> jcbEditGroupCondition;
    private javax.swing.JComboBox<String> jcbEditGroupName;
    private javax.swing.JComboBox<String> jcbEditObjPosObjective;
    private javax.swing.JComboBox<String> jcbEditObjPosPosition;
    private javax.swing.JComboBox<String> jcbEditObjPosValue;
    private javax.swing.JComboBox<String> jcbEditObjPriorityObjective1;
    private javax.swing.JComboBox<String> jcbEditObjPriorityObjective2;
    private javax.swing.JComboBox<String> jcbEditObjValueCondition;
    private javax.swing.JComboBox<String> jcbEditObjValueObjective;
    private javax.swing.JComboBox<String> jcbFilterType;
    private javax.swing.JCheckBox jcbSelectAll;
    private javax.swing.JLabel jlblNew;
    private javax.swing.JLabel jlblRemoveSelected;
    private javax.swing.JPanel jpnEditGroupPanel;
    private javax.swing.JPanel jpnEditObjPosPanel;
    private javax.swing.JPanel jpnEditObjPriorityPanel;
    private javax.swing.JPanel jpnEditObjValuePanel;
    private javax.swing.JPanel jpnEditPanel;
    private javax.swing.JPanel jpnEditShapePanel;
    private javax.swing.JPanel jpnList;
    private javax.swing.JTextField jtfEditObjValue;
    private javax.swing.JTextField jtfEditShape;
    // End of variables declaration//GEN-END:variables
}

