/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.draw;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import py.una.pol.setviz.gui.GeneralInfoPanel;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.DrawEntry;
import py.una.pol.setviz.utils.SelectionEntry;
import py.una.pol.setviz.utils.solution.Solution;

/**
 *
 * @author acost
 */
public class SolutionDrawParallelCoordinates extends SolutionDraw {
    
    public static final int OFFSET_X_LEFT = 50;
    public static final int OFFSET_X_RIGHT = 100;
    public static final int OFFSET_Y_UP = 40;
    public static final int OFFSET_Y_DOWN = 50;
    public static final double AXES_INC = 0.1;
    public static final int AXES_NUMBER_SPACE_X = 5;
    public static final int AXES_LEGEND_SPACE_Y = 20;    
    
    Dimension size;
    int space_x;
    int from;
    int to;
    int length;
    
    
    public SolutionDrawParallelCoordinates(SolutionDrawManager solutionDrawManager) {
        super(solutionDrawManager);
    }

    
    public void drawAxes(Graphics2D g2) {
                
       ArrayList<Integer> currentAxesOrder = PlotFrame.settings.getCurrentAxesOrder();
       ArrayList<String> names = PlotFrame.settings.getObjectiveNames();
       
       GeneralInfoPanel.GeneralInfo info = PlotFrame.generalInfoPanel.getGeneralInfo();
       String infoStr = String.format("Original set: %s - Filtered: %s - Drawing: %s", info.originalSizeStr, info.filteredSizeStr, info.drawingSizeStr);
        
       g2.setColor(Color.BLACK);
       g2.setFont(new Font("SansSerif", Font.PLAIN, 12));
       g2.drawString(infoStr, OFFSET_X_LEFT, OFFSET_Y_UP - 20);
       
       g2.setColor(Color.BLUE);       
       for (int i = 0; i < currentAxesOrder.size(); i++) {
           g2.setFont(new Font("SansSerif", Font.BOLD, 14));
           int x = dimensionToX(i);
           
           g2.drawLine(x, OFFSET_Y_UP, x, OFFSET_Y_UP + length);
           g2.drawLine(x+1, OFFSET_Y_UP, x+1, OFFSET_Y_UP + length);
           
           double eps = 0.0005;
           double val = 0;
           while(val <= 1 + eps) {
               
               int y = valueToY(val);

               g2.drawString(String.format("%.2f", val), x + AXES_NUMBER_SPACE_X
                       , y + 4);
               
               g2.drawLine(x, y, x + 4, y);
               g2.drawLine(x, y+1, x + 4, y+1);
               
               val += AXES_INC;
           }

           String legend = names.get(currentAxesOrder.get(i));
           if (legend.length() > 25) {
               legend = legend.substring(0,22) + "...";                 
           }
           g2.setFont(new Font("SansSerif", Font.BOLD, 10));
           g2.drawString(legend, x - ((legend.length() * 8) / 2), OFFSET_Y_UP + length + AXES_LEGEND_SPACE_Y);
           
       }
       
        
    }
    
    
    private int dimensionToX(int dimension) {                        
        return OFFSET_X_LEFT + dimension * space_x;
    }
    
    
    private int valueToY(double value) {               
       return to - new Double(value * (to - from)).intValue();
       
    }
    
    @Override
    public void updateSize(Dimension size) {
        this.size = size;
        length = size.height - (OFFSET_Y_UP + OFFSET_Y_DOWN);
        from = OFFSET_Y_UP;
        to = OFFSET_Y_UP + length;               
        space_x = (size.width - OFFSET_X_LEFT - OFFSET_X_RIGHT) / (PlotFrame.settings.getCurrentAxesOrder().size()-1);                        
    }
    
    @Override
    public void drawSolutions(Graphics2D g2) {
        super.drawSolutions(g2);        
        drawAxes(g2);
    }
    
    
    @Override
    protected void drawSingleSolution(Graphics2D g2, Solution sol, SolutionWeight solutionWeight) {
        drawSingleSolution(g2, sol, solutionWeight, false, 0);
    }
    
    @Override
    protected void drawSingleSolution(Graphics2D g2, Solution sol, SolutionWeight solutionWeight, boolean legend, int index) {
       
        g2.setFont(new Font("SansSerif", Font.PLAIN, 10));
        
        ArrayList<Integer> currentAxesOrder = PlotFrame.settings.getCurrentAxesOrder();
        for(int i = 0; i < currentAxesOrder.size() - 1; i++) {           

            int x1 = dimensionToX(i);                
            int x2 = dimensionToX(i+1);

            Double val_1 = sol.getValueAt(currentAxesOrder.get(i));
            Double val_2 = sol.getValueAt(currentAxesOrder.get(i+1));

            int y1 = valueToY(val_1);
            int y2 = valueToY(val_2);
    
        
            if (solutionWeight == SolutionWeight.TRIPLE) {
                g2.drawLine(x1, y1-1, x2, y2-1);
                g2.drawLine(x1, y1+1, x2, y2+1);
            }
                        
            if (solutionWeight == SolutionWeight.DOUBLE) {
                g2.drawLine(x1, y1+1, x2, y2+1);
            }
            
            if (solutionWeight == SolutionWeight.QUINTUPLE) {
                Color c = g2.getColor();
                g2.setColor(Color.BLACK);
                g2.drawLine(x1, y1+1, x2, y2+1);
                g2.drawLine(x1, y1-2, x2, y2-2);
                g2.setColor(c);
                g2.drawLine(x1, y1+2, x2, y2+2);
                g2.drawLine(x1, y1-1, x2, y2-1);
                
            }
            
            g2.drawLine(x1, y1, x2, y2);
                    
            
        }
        
        if (legend) {
            g2.drawString(sol.prettyString(), OFFSET_X_LEFT, OFFSET_Y_UP + length + (index +2) * 20);
        }
        
        
        
    }
    
    

    @Override
    public void update(double x, double y, boolean zoom) {
        
        solutionDrawManager.hoveredSolutions = new ArrayList<>();        
        ArrayList<Integer> currentAxesOrder = PlotFrame.settings.getCurrentAxesOrder();

        int x0 = new Double(x).intValue();
        int y0 = new Double(y).intValue();                
        
        if (x0 < dimensionToX(0) || x0 > dimensionToX(currentAxesOrder.size()-1)) return;
        
        int leftAxis = -1;                        
        
        for (int i = 0; i < currentAxesOrder.size(); i++) {
            if (dimensionToX(i) < x0) {
                leftAxis = i;                
            }
        }
        
        if (leftAxis == -1) return;
        
        int xleftAxis = dimensionToX(leftAxis);
        int xrightAxis = dimensionToX(leftAxis + 1);
        
        Set<Integer> idsSet = new HashSet<>();

        
        boolean onlyRepresentatives = solutionDrawManager.isDrawOnlyRepresentatives();
        
        ArrayList<Solution> representatives = new ArrayList<Solution>();
        int rep = 0;
        
        for(DrawEntry drawEntry : solutionDrawManager.drawingSet) {
            if (onlyRepresentatives) {
                representatives.add(drawEntry.getRepresentative());
                idsSet.add(rep++);
            } else {
                idsSet.addAll(drawEntry.getSolutionIds());
            }
        }       
        
        for(Integer solutionId : idsSet) {  
            Solution sol;
            if (onlyRepresentatives) {
                 sol = representatives.get(solutionId);
            } else {
                 sol = solutionDrawManager.solutionSet.getSolutionById(solutionId);
            }
            
            int value_left = valueToY(sol.getValueAt(currentAxesOrder.get(leftAxis)));
            int value_right = valueToY(sol.getValueAt(currentAxesOrder.get(leftAxis + 1)));
            
            if ((value_left >= y0 && value_right <= y0) ||
                    (value_left <= y0 && value_right >= y0)) {                
                if (inLine(xleftAxis, value_left, xrightAxis, value_right, x0, y0, zoom)) {
                                        
                    solutionDrawManager.hoveredSolutions.add(solutionDrawManager.createSelectionEntry(sol));
                    
                }
                
            }
        
        }        
        
        
        PlotFrame.plotFrame.updateInfo();
    
        
    }

    @Override
    public void updateClick(double x, double y, boolean zoom) {
        
        int x0 = new Double(x).intValue();
        int y0 = new Double(y).intValue();
        
        if (x0 < dimensionToX(0) || x0 > dimensionToX(solutionDrawManager.solutionSet.getDimensions()-1)) return;
        
        ArrayList<Integer> currentAxesOrder = PlotFrame.settings.getCurrentAxesOrder();
        
        int leftAxis = -1;                        
        
        for (int i = 0; i < currentAxesOrder.size(); i++) {
            if (dimensionToX(i) < x0) {
                leftAxis = i;                
            }
        }
        
        if (leftAxis == -1) return;
        
        int xleftAxis = dimensionToX(leftAxis);
        int xrightAxis = dimensionToX(leftAxis + 1);
        
        
        
        for(SelectionEntry selectionEntry : solutionDrawManager.hoveredSolutions) {
            
            
            Solution sol = selectionEntry.getSolution();
            
            int value_left = valueToY(sol.getValueAt(currentAxesOrder.get(leftAxis)));
            int value_right = valueToY(sol.getValueAt(currentAxesOrder.get(leftAxis + 1)));
            
            if ((value_left >= y0 && value_right <= y0) ||
                    (value_left <= y0 && value_right >= y0)) {                
                if (inLine(xleftAxis, value_left, xrightAxis, value_right, x0, y0, zoom)) {
                    
                    solutionDrawManager.toogleSelection(selectionEntry);
                    
                }
                
            }
        }
        
        if (PlotFrame.selectionsPanel != null) {
            PlotFrame.selectionsPanel.setClickedSolutions(solutionDrawManager.clickedSolutions);
        }
        
    }

    
    
    
    private boolean inLine(int Ax, int Ay, int Bx, int By, int Cx, int Cy, boolean zoom) {
        
        double distanceAC = distance(Ax, Ay, Cx, Cy);
        double distanceBC = distance(Bx, By, Cx, Cy);
        double distanceAB = distance(Ax, Ay, Bx, By);
        
        double eps;
        if (!zoom) { eps = 1.005; }
        else { eps = 0.005; }
            
        return Math.abs((distanceAC + distanceBC) - distanceAB) <= eps;
        
    }
    
    private double distance(int Ax, int Ay, int Bx, int By) {
        return Math.sqrt((Ax - Bx)*(Ax - Bx) + (Ay - By)*(Ay - By));
    }        
    
    
}
