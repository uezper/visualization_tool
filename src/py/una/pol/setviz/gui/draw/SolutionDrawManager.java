/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.draw;

import java.util.ArrayList;
import java.util.HashMap;
import py.una.pol.setviz.utils.DrawEntry;
import py.una.pol.setviz.utils.solution.cluster.ShapeCluster;
import py.una.pol.setviz.utils.SelectionEntry;
import py.una.pol.setviz.utils.solution.Solution;
import py.una.pol.setviz.utils.solution.SolutionExtraInformation;
import py.una.pol.setviz.utils.solution.SolutionSet;

/**
 *
 * @author acost
 */
public class SolutionDrawManager {
    
    SolutionSet solutionSet;
    ArrayList<DrawEntry> drawingSet;
    ArrayList<SelectionEntry> hoveredSolutions;   
    HashMap<Integer, SelectionEntry> clickedSolutions;   
    ArrayList<ArrayList<Integer>> rankedSolutions;
       
    boolean drawOnlyRepresentatives;
    
    
    public SolutionDrawManager(SolutionSet solutionSet) {    
        
        this.solutionSet = solutionSet;        
        this.hoveredSolutions = new ArrayList<>();
        this.clickedSolutions = new HashMap<>();                
        this.drawOnlyRepresentatives = false;        
        this.drawingSet = new ArrayList<>();        
        this.rankedSolutions = new ArrayList<>();
        
    }

    public boolean isDrawOnlyRepresentatives() {
        return drawOnlyRepresentatives;
    }

    public void setDrawOnlyRepresentatives(boolean drawOnlyRepresentatives) {
        this.drawOnlyRepresentatives = drawOnlyRepresentatives;
    }

    public ArrayList<DrawEntry> getDrawingSet() {
        return drawingSet;
    }

    public void setDrawingSet(ArrayList<DrawEntry> drawingSet) {
        this.drawingSet = drawingSet;
    }
    
    
    public int getOriginalSize() {
        return this.solutionSet.getSize();
    }
    
    public SolutionSet getSolutionSet() {
        return solutionSet;
    }

    public HashMap<Integer, SelectionEntry> getClickedSolutions() {
        return clickedSolutions;
    }

    public void setClickedSolutions(HashMap<Integer, SelectionEntry> clickedSolutions) {
        this.clickedSolutions = clickedSolutions;        
    }

    public ArrayList<SelectionEntry> getHoveredSolutions() {
        return hoveredSolutions;
    }

    public void setHoveredSolutions(ArrayList<SelectionEntry> hoveredSolutions) {
        this.hoveredSolutions = hoveredSolutions;
    }

    public ArrayList<ArrayList<Integer>> getRankedSolutions() {
        return rankedSolutions;
    }

    public void setRankedSolutions(ArrayList<ArrayList<Integer>> rankedSolutions) {
        this.rankedSolutions = rankedSolutions;
    }
    
    public SelectionEntry createSelectionEntry(Solution sol) {
        SelectionEntry selectionEntry = new SelectionEntry();
        
        Boolean representative = (Boolean)sol.getExtraInformation(SolutionExtraInformation.SOLUTION_IS_REPRESENTATIVE);

        if (representative != null && representative && drawOnlyRepresentatives) {
            selectionEntry.setSelectionType(SelectionEntry.SelectionType.SET);
            selectionEntry.setCluster((ShapeCluster)sol.getExtraInformation(SolutionExtraInformation.SOLUTION_CLUSTER_SHAPE));
            selectionEntry.setSize(selectionEntry.getCluster().ids.size());
        } else {
            selectionEntry.setSelectionType(SelectionEntry.SelectionType.SOLUTION);
        }

        selectionEntry.setSolution(sol);                                                            
        return selectionEntry;
        
    }
    
    
    public void toogleSelection(SelectionEntry selectionEntry) {
        toogleSelection(selectionEntry, SelectionMode.NONE);
    }
    
    public void toogleSelection(SelectionEntry selectionEntry, SelectionMode selectionMode) {
        
        Solution sol = selectionEntry.getSolution();
        
        Boolean selected = (Boolean)sol.getExtraInformation(SolutionExtraInformation.SOLUTION_SELECTED);
        Boolean rep = selectionEntry.getSelectionType() == SelectionEntry.SelectionType.SET;

        if (selectionMode!= SelectionMode.FORCE_DESELECT && (selected == null || selected == false || selectionMode == SelectionMode.FORCE_SELECT)) {                        
            clickedSolutions.put(sol.getId(), selectionEntry);                        
            sol.putExtraInformation(SolutionExtraInformation.SOLUTION_SELECTED, true);

            if (rep) {
                ShapeCluster c = selectionEntry.getCluster();
                if (c != null) {
                    for (Integer id : c.ids) {
                        Solution s = solutionSet.getSolutionById(id);
                        
                        s.putExtraInformation(SolutionExtraInformation.SOLUTION_SELECTED, true);

                        SelectionEntry newSelectionEntry = new SelectionEntry();
                        newSelectionEntry.setColor(selectionEntry.getColor());
                        newSelectionEntry.setSelectionType(SelectionEntry.SelectionType.SOLUTION);
                        newSelectionEntry.setSolution(s);                    

                        clickedSolutions.put(s.getId(), newSelectionEntry);                        


                    }
                } 

            }  else {
                updateSelectedRepresentatives(sol, true);
            }
        } else {
            clickedSolutions.remove(sol.getId());
            sol.putExtraInformation(SolutionExtraInformation.SOLUTION_SELECTED, false);

            if (rep) {
                ShapeCluster c = selectionEntry.getCluster();
                if (c != null) {
                    for (Integer id : c.ids) {
                        Solution s = solutionSet.getSolutionById(id);
                        s.putExtraInformation(SolutionExtraInformation.SOLUTION_SELECTED, false);
                        clickedSolutions.remove(s.getId());
                    }
                }
            } else {
                updateSelectedRepresentatives(sol, false);
            }
        }
        
    }
    
 
    private void updateSelectedRepresentatives(Solution sol, boolean select) {
        
        
        for(SelectionEntry selectionEntry : clickedSolutions.values()) {
            if (selectionEntry.getSelectionType() == SelectionEntry.SelectionType.SET) {
                if (selectionEntry.inSelection(sol)) {
                    if (select) selectionEntry.setSize(selectionEntry.getSize() + 1);
                    else selectionEntry.setSize(selectionEntry.getSize() - 1);
                }
            }
        }
    }
    

    public enum SelectionMode {
        NONE,
        FORCE_SELECT,
        FORCE_DESELECT
    }
    
    
}
