/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.draw;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.DrawEntry;
import py.una.pol.setviz.utils.SelectionEntry;
import py.una.pol.setviz.utils.solution.Solution;

/**
 *
 * @author acost
 */
public abstract class SolutionDraw {
    
    SolutionDrawManager solutionDrawManager;
    
    public SolutionDraw(SolutionDrawManager solutionDrawManager) {
        this.solutionDrawManager = solutionDrawManager;
    }
    
    public abstract void updateSize(Dimension size);
    
    protected abstract void drawSingleSolution(Graphics2D g2, Solution sol, SolutionWeight solutionWeight, boolean legend, int index);
    protected abstract void drawSingleSolution(Graphics2D g2, Solution sol, SolutionWeight solutionWeight);    
    
    public void drawSolutions(Graphics2D g2) {
        
        
        // draw all original solutions in background
        if (PlotFrame.settings.isShowOriginalSetInBackground()) {
            for (Solution sol: solutionDrawManager.solutionSet.getSolutions()) {
                g2.setColor(Color.LIGHT_GRAY);
                drawSingleSolution(g2, sol, SolutionWeight.SINGLE);
            }
        }
        
        
        // obtain ranked solutions
        Map<Integer, Integer> randkedSolutionsRank = new HashMap<>();
        List<Integer> rankedSolutionIds = new ArrayList<>();
        for (int i = 0; i < solutionDrawManager.rankedSolutions.size(); i++) {
            List<Integer> rank = solutionDrawManager.rankedSolutions.get(i);
            for (int j = 0; j < rank.size(); j++) {
                randkedSolutionsRank.put(rank.get(j), i);
            }
        }
        
        // draw solutions
        for(DrawEntry drawEntry : solutionDrawManager.drawingSet) {            
            g2.setColor(drawEntry.getColor());
            
            if (solutionDrawManager.isDrawOnlyRepresentatives()) {
                drawSingleSolution(g2, drawEntry.getRepresentative(), SolutionWeight.SINGLE);
            } else {
                drawEntry.getSolutionIds().forEach((solutionId) -> {
                    
                    if (randkedSolutionsRank.containsKey(solutionId)) {
                        rankedSolutionIds.add(solutionId);
                    }
                    
                    drawSingleSolution(g2, solutionDrawManager.solutionSet.getSolutionById(solutionId), SolutionWeight.SINGLE);                                     
                });
            }
            
        }
        
        // draw ranked solutions        
        for (int i = 0; i < rankedSolutionIds.size(); i++) {
            if (solutionDrawManager.isDrawOnlyRepresentatives()) {
                // do nothing
            } else {                
                Integer solutionId = rankedSolutionIds.get(i);
                g2.setColor(ColorPalette.getRankColor(randkedSolutionsRank.get(solutionId), solutionDrawManager.rankedSolutions.size()));                                
                drawSingleSolution(g2, solutionDrawManager.solutionSet.getSolutionById(solutionId), SolutionWeight.SINGLE);                
            }
        }
        
        ArrayList<Integer> removeKeys = new ArrayList<>();
        Set<Integer> keys = solutionDrawManager.clickedSolutions.keySet();
        // draw selected solutions
        for(Integer key : keys) {                        
            
            SelectionEntry selectionEntry = solutionDrawManager.clickedSolutions.get(key);
            
            g2.setColor(selectionEntry.getColor());            
            
            if (solutionDrawManager.drawOnlyRepresentatives && selectionEntry.getSelectionType() == SelectionEntry.SelectionType.SOLUTION) continue;
            if (!solutionDrawManager.drawOnlyRepresentatives && selectionEntry.getSelectionType() != SelectionEntry.SelectionType.SOLUTION) continue;
            
            if (selectionEntry.getSelectionType() == SelectionEntry.SelectionType.SET &&
                    selectionEntry.getSize() == 0) {
                removeKeys.add(key);
                continue;
            }                        
            
            Solution sol = selectionEntry.getSolution();                                                
            drawSingleSolution(g2, selectionEntry.getSolution(), SolutionWeight.TRIPLE);            

       }
        
       for (Integer key : removeKeys) {
           solutionDrawManager.clickedSolutions.remove(key);
       }
                
        // draw hovered solutions
        g2.setColor(ColorPalette.hoverColor);
        for(int k = 0; k < solutionDrawManager.hoveredSolutions.size(); k++) {            
            SelectionEntry selectionEntry = solutionDrawManager.hoveredSolutions.get(k);
            
            Solution sol = selectionEntry.getSolution();                                    
            drawSingleSolution(g2, sol, SolutionWeight.TRIPLE, true, k);                        
        }
        
        
        if (removeKeys.size() > 0 && PlotFrame.selectionsPanel != null) {
            PlotFrame.selectionsPanel.setClickedSolutions(solutionDrawManager.clickedSolutions);
        }
        
        
                
    }
    public abstract void update(double x, double y, boolean zoom);
    public abstract void updateClick(double x, double y, boolean zoom);
    
    public enum SolutionWeight {
        SINGLE,
        DOUBLE,
        TRIPLE,
        QUINTUPLE
    }
    
}
