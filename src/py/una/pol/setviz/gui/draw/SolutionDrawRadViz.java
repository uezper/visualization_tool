/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.draw;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import py.una.pol.setviz.gui.GeneralInfoPanel;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.utils.DrawEntry;
import py.una.pol.setviz.utils.SelectionEntry;
import py.una.pol.setviz.utils.solution.Solution;

/**
 *
 * @author acost
 */
public class SolutionDrawRadViz extends SolutionDraw {

    public static int OFFSET = 35;
    public static int SOL_MIN_RADIUS = 3;    
    public static double SOL_RADIUS_PROP = 1/80f;    
    
    Dimension size;
    int radius;
    int center_x;
    int center_y;
    
    public SolutionDrawRadViz(SolutionDrawManager solutionDrawManager) {
        super(solutionDrawManager);
    }

    
    @Override
    public void updateSize(Dimension size) {
        this.size = size;
        radius = new Double((Math.min(size.getHeight(), size.getWidth()) - 2 * OFFSET) / 2).intValue();
        center_x = new Double(size.getWidth() / 2).intValue();
        center_y = new Double(size.getHeight() / 2).intValue();                
    }

    
    
    @Override
    public void drawSolutions(Graphics2D g2) {
        super.drawSolutions(g2);
        
        drawAxes(g2);
    }

    @Override
    public void update(double x, double y, boolean zoom) {
        
        solutionDrawManager.hoveredSolutions = new ArrayList<>();        
        ArrayList<Integer> currentAxesOrder = PlotFrame.settings.getCurrentAxesOrder();

        int x0 = new Double(x).intValue();
        int y0 = new Double(y).intValue();                
        
        double eps = 0.001;
        
        // Checks if the mouse is inside the circle        
        double distMouse = distance(x0, y0, center_x, center_y);
        if (distMouse > radius + eps) return;
        
        
        Set<Integer> idsSet = new HashSet<>();

        int sol_radius = new Double(SOL_RADIUS_PROP * radius).intValue();
        sol_radius = Math.max(sol_radius, SOL_MIN_RADIUS);
        
        boolean onlyRepresentatives = solutionDrawManager.isDrawOnlyRepresentatives();
        ArrayList<Solution> representatives = new ArrayList<Solution>();
        int rep = 0;
        
        for(DrawEntry drawEntry : solutionDrawManager.drawingSet) {
            if (onlyRepresentatives) {
                representatives.add(drawEntry.getRepresentative());
                idsSet.add(rep++);
            } else {
                idsSet.addAll(drawEntry.getSolutionIds());
            }
        }       
        
        for(Integer solutionId : idsSet) {            
             Solution sol;
            if (onlyRepresentatives) {
                 sol = representatives.get(solutionId);
            } else {
                 sol = solutionDrawManager.solutionSet.getSolutionById(solutionId);
            }
            
            List<Integer> pos = getSolutionPos(sol);
            
            int x_pos = pos.get(0);
            int y_pos = pos.get(1);
            
            if (distance(x_pos, y_pos, x0, y0) <= sol_radius + eps) {                                        
                solutionDrawManager.hoveredSolutions.add(solutionDrawManager.createSelectionEntry(sol));                    
            }
                
        
        }        
        
        PlotFrame.plotFrame.updateInfo();
    
        
    }

    @Override
    public void updateClick(double x, double y, boolean zoom) {
        
        int x0 = new Double(x).intValue();
        int y0 = new Double(y).intValue();                
        
        double eps = 0.001;
        
        int sol_radius = new Double(SOL_RADIUS_PROP * radius).intValue();
        sol_radius = Math.max(sol_radius, SOL_MIN_RADIUS);
        
        
        // Checks if the mouse is inside the circle        
        double distMouse = distance(x0, y0, center_x, center_y);
        if (distMouse > radius + eps) return;
                      
        for(SelectionEntry selectionEntry : solutionDrawManager.hoveredSolutions) {
                        
            Solution sol = selectionEntry.getSolution();
            
            List<Integer> pos = getSolutionPos(sol);
            
            int x_pos = pos.get(0);
            int y_pos = pos.get(1);
            
            if (distance(x_pos, y_pos, x0, y0) <= sol_radius + eps) {                                        
                solutionDrawManager.toogleSelection(selectionEntry);
            }
                            
        }
        
        if (PlotFrame.selectionsPanel != null) {
            PlotFrame.selectionsPanel.setClickedSolutions(solutionDrawManager.clickedSolutions);
        }
        
        
        
    }

    private void drawAxes(Graphics2D g2) {
        
        g2.setColor(Color.black);
        g2.drawArc(center_x - radius, center_y - radius, 2 * radius, 2 * radius, 0, 360);        
        
        ArrayList<String> names = PlotFrame.settings.getObjectiveNames();
        ArrayList<Integer> axesOrder = PlotFrame.settings.getCurrentAxesOrder();
        
        GeneralInfoPanel.GeneralInfo info = PlotFrame.generalInfoPanel.getGeneralInfo();        
        String infoStr = String.format("Original set: %s - Filtered: %s - Drawing: %s", info.originalSizeStr, info.filteredSizeStr, info.drawingSizeStr);
        
        g2.setFont(new Font("SansSerif", Font.PLAIN, 12));
        g2.setColor(Color.BLACK);
        g2.drawString(infoStr, 15, 15);
        
        g2.setFont(new Font("SansSerif", Font.BOLD, 14));
        
        Double offset = 5.0;
        Double angleSpace = (2 * Math.PI) / PlotFrame.settings.getShownObjectives().size();
        for(int i = 0; i < PlotFrame.settings.getShownObjectives().size(); i++) {
            String name = names.get(axesOrder.get(i));
            
            if (name.length() > 25) {
               name = name.substring(0,22) + "...";                 
           }
            
            Double angle = angleSpace * i;
            int x = 0;
            int y = 0;
            
            double siny = Math.sin(angle);
            x = new Double((radius + offset) * Math.cos(angle)).intValue();
            y = new Double((radius + offset) * siny).intValue();
            
            if (angle > (1/2.0) * Math.PI && angle < (3/2.0) * Math.PI) { // left
                x -= 8 * name.length();
                                
            }
            
            if (angle > 0 && angle < Math.PI) { // down
                y += 14;   
            }
            
            
            x += center_x;
            y += center_y;
            
            g2.drawString(name, x, y);
            //g2.drawRect(x, y - 14, 8 * name.length(), 14);
            
        }
        
    }
    
    private List<Integer> getSolutionPos(Solution sol) {
        
        ArrayList<Integer> axesOrder = PlotFrame.settings.getCurrentAxesOrder();        
        int N = PlotFrame.settings.getShownObjectives().size();
                
        double angleSpace = (2 * Math.PI) / N;
        
        double eps = 0.000001;
        double xNum = 0.0;       
        double xDen = eps;
        double yNum = 0.0;
        double yDen = eps;
        
        for(int i = 0; i < N; i++) {
                        
            Double value = sol.getValueAt(axesOrder.get(i));
            
            if (PlotFrame.isMinProblem) {
                value = 1 - value;
            }
            
            
            xNum += value * Math.cos(angleSpace * i);
            xDen += value;
            yNum += value * Math.sin(angleSpace * i);
            yDen += value;            
        }        
        
        int x = new Double((xNum / xDen) * radius).intValue();
        int y = new Double((yNum / yDen) * radius).intValue();
        
        x += center_x;
        y += center_y;
        
        List<Integer> ret = new ArrayList<>();
        ret.add(x);
        ret.add(y);
        
        return ret;
    }
    
    
    @Override
    protected void drawSingleSolution(Graphics2D g2, Solution sol, SolutionWeight solutionWeight, boolean legend, int index) {
        
        List<Integer> ret = getSolutionPos(sol);
        
        int x = ret.get(0);
        int y = ret.get(1);
        
        int sol_radius = new Double(SOL_RADIUS_PROP * radius).intValue();
        sol_radius = Math.max(sol_radius, SOL_MIN_RADIUS);
        
        Color temp = g2.getColor();        
        g2.fillArc(x - sol_radius, y - sol_radius, 2 * sol_radius, 2 * sol_radius, 0, 360);
        g2.setColor(Color.BLACK);
        g2.drawArc(x - sol_radius, y - sol_radius, 2 * sol_radius, 2 * sol_radius, 0, 360);
        g2.setColor(temp);
    }

    @Override
    protected void drawSingleSolution(Graphics2D g2, Solution sol, SolutionWeight solutionWeight) {
        drawSingleSolution(g2, sol, solutionWeight, false, 0);
    }
    
    
    
    private double distance(int Ax, int Ay, int Bx, int By) {
        return Math.sqrt((Ax - Bx)*(Ax - Bx) + (Ay - By)*(Ay - By));
    }        
    
}
