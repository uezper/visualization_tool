/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.draw;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;



/**
 *
 * @author acost
 */
public class ColorPalette {
   
    private static HashMap<String, Color> shapeColors;
    public static ArrayList<Color> mainColors;    
    public static Color hoverColor;
    
    public static void createColor(int n) {
           
        hoverColor = Color.BLACK;
        shapeColors = new HashMap<>();
        mainColors = new ArrayList<>();
        
        Random random = new Random();
        float hue, saturation, luminance;
        Color color;
        
        for (int i = 0; i < n; i++) {          
            hue = random.nextFloat();
            saturation = (random.nextInt(4000) + 6000) / 10000f;
            luminance = (random.nextInt(3000) + 5000) / 10000f;
            color = Color.getHSBColor(hue, saturation, luminance);            
            if (i < n) {
            mainColors.add(color);            
            } 
        }
        
        
    }
    
    private static void verify() {
        if (mainColors == null) {
            createColor(1000);
        }
    }

    public static void setColor(int index, Color color) {
        verify();
        mainColors.set(index, color);
    }
    
    public static Color indexToColor(int index) {
        verify();    
        Color color = mainColors.get(index % mainColors.size());        
        return color;
    }
    
    public static void setShapeColor(String shape, Color color) {
        verify();
        shapeColors.put(shape, color);
    }
    
    public static Color getShapeColor(String shape) {
        verify();
        Color color = shapeColors.get(shape);
        if (color == null) {
            color = indexToColor(new Random().nextInt(mainColors.size()));
            shapeColors.put(shape, color);
        }
        return color;
    }
    
    
    public static Color getRankColor(Integer n, Integer maxN) {
        
        Color firstColor = new Color(91, 47, 47);
        Color secondColor = Color.RED;
                
        double inc = 1.0 / maxN;
        
        double p = inc * n;
        int newR = new Double(firstColor.getRed() * p + secondColor.getRed() * (1 - p)).intValue();
        int newG = new Double(firstColor.getGreen() * p + secondColor.getGreen() * (1 - p)).intValue();
        int newB = new Double(firstColor.getBlue() * p + secondColor.getBlue() * (1 - p)).intValue();
        
        Color ret = new Color(newR, newG, newB);
        return ret;
    }
    
}
