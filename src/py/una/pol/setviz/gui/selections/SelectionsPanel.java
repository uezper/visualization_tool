/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.selections;


import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.gui.utils.SelectFolderFileUtil;
import py.una.pol.setviz.utils.CustomMouseListener;
import py.una.pol.setviz.utils.DrawEntry;
import py.una.pol.setviz.utils.ExportSolutionsToCsvUtil1;
import py.una.pol.setviz.utils.ImageUtils;
import py.una.pol.setviz.utils.SelectionEntry;
import py.una.pol.setviz.utils.solution.Group;

/**
 *
 * @author acost
 */
public class SelectionsPanel extends javax.swing.JPanel {

    /**
     * Creates new form SelectionsPanel
     */
    
    ImageIcon removeIcon;
    HashMap<Integer, SelectionEntry> clickedSolutions;
    HashMap<Integer, JPanel> panelRows;
    
    MouseListener hoverListener;
    Color lastColor;
    
    ArrayList<Group> existingGroups;
    
    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    public SelectionsPanel() {
        initComponents();
                
    
        clickedSolutions = new HashMap<>();
        removeIcon = ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("delete-icon.png"), 
                this.jlblDeleteAll.getPreferredSize().width, 
                this.jlblDeleteAll.getPreferredSize().height);
        
        this.jlblDeleteAll.setIcon(removeIcon);                
        this.jlblDeleteAll.setToolTipText("Remove all");
        makeButtonStyle(jlblDeleteAll);

        
        this.jlblAddToGroup.setIcon(ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("add-to-group.png"), 
                this.jlblAddToGroup.getPreferredSize().width, 
                this.jlblAddToGroup.getPreferredSize().height));                
        this.jlblAddToGroup.setToolTipText("Add to group");
        makeButtonStyle(jlblAddToGroup);

        JScrollBar vertical = this.jScrollPane1.getVerticalScrollBar();
	InputMap verticalMap = vertical.getInputMap( JComponent.WHEN_IN_FOCUSED_WINDOW );
	verticalMap.put( KeyStroke.getKeyStroke( "DOWN" ), "positive_" );        
	verticalMap.put( KeyStroke.getKeyStroke( "UP" ), "negative_" );
        
        
        Action actionListener1 = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                
                JScrollBar jScrollBar = (JScrollBar) actionEvent.getSource();               
                jScrollBar.setValue(jScrollBar.getValue() + 20);
                
            }
        };
        
        Action actionListener2 = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                
                JScrollBar jScrollBar = (JScrollBar) actionEvent.getSource();               
                jScrollBar.setValue(jScrollBar.getValue() - 20);
                
            }
        };
        
        ActionMap actionMap = new ActionMap();
        actionMap.put("positive_", actionListener1);
        actionMap.put("negative_", actionListener2);
        this.jScrollPane1.getVerticalScrollBar().setActionMap(actionMap);
        
        hoverListener = new CustomMouseListener()
                .setMouseEntered((e) -> {hoverEntered(e);})
                .setMouseExited((e) -> {hoverExited(e);});
        
        
        
        this.jtfGroupName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                check();
            }
            
            private void check() {
                boolean valid = PlotFrame.groupListPanel.checkGroupName(jtfGroupName.getText());
                if (valid) {
                    jtfGroupName.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                } else {
                    jtfGroupName.setBorder(BorderFactory.createLineBorder(Color.RED));
                }                    
                jbtnAdd.setEnabled(valid);
            }
            
        });
        
        
        
        updateList();
    }

    
    private void makeButtonStyle(JLabel component) {
        component.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        component.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        component.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
        component.setBackground(new Color(210,210,210));
        component.setCursor(new Cursor(Cursor.HAND_CURSOR));
        component.setPreferredSize(new Dimension(16,16));
        component.setOpaque(true);        
    }
    
    
    private void hoverEntered(MouseEvent e) {        
        int id = Integer.parseInt(e.getComponent().getName().split("_")[1]);
        
        SelectionEntry selectionEntry = clickedSolutions.getOrDefault(id, null);
        if (selectionEntry != null) {            
            JPanel panel = panelRows.get(id);
            lastColor = panel.getBackground();
            panel.setBackground(new Color(210, 210, 210));
            PlotFrame.solutionDraw.getHoveredSolutions().add(selectionEntry);
            PlotFrame.redraw();
        }
    }
    
    private void hoverExited(MouseEvent e) {        
        int id = Integer.parseInt(e.getComponent().getName().split("_")[1]);
        
        
        SelectionEntry selectionEntry = clickedSolutions.getOrDefault(id, null);
        if (selectionEntry != null) {
            JPanel panel = panelRows.get(id);
            panel.setBackground(lastColor);
            PlotFrame.solutionDraw.setHoveredSolutions(new ArrayList<>());
            PlotFrame.redraw();
        }
    }
    
    public HashMap<Integer, SelectionEntry> getClickedSolutions() {
        return clickedSolutions;
    }

    public void setClickedSolutions(HashMap<Integer, SelectionEntry> clickedSolutions) {
        this.clickedSolutions = clickedSolutions;
        checkAllButtons();
        updateList();
    }

    
    private void checkAllButtons() {
        
        this.jlblDeleteAll.setEnabled(clickedSolutions.size() > 0);
        this.jlblAddToGroup.setEnabled(clickedSolutions.size() > 0);
        this.jbtnExport.setEnabled(clickedSolutions.size() > 0 && !PlotFrame.solutionDraw.isDrawOnlyRepresentatives());
        
    }
    
    
    private void checkSelectionsAreValid() {
        
        ArrayList<Integer> removeIds = new ArrayList<>();
        for (SelectionEntry selectionEntry : clickedSolutions.values()) {
            boolean found = false;
            for(DrawEntry drawEntry : PlotFrame.solutionDraw.getDrawingSet()) {
                if (found) continue;
                for(Integer solId : drawEntry.getSolutionIds()) {
                    if (selectionEntry.getSelectionType() == SelectionEntry.SelectionType.SOLUTION) {
                        if (solId == selectionEntry.getSolution().getId()) found = true;
                    }
                }                
            }
            if (!found) removeIds.add(selectionEntry.getSolution().getId());
        }
        
        for(Integer id : removeIds) {
            clickedSolutions.remove(id);
        }
    }
    
    public void updateList() {
        
        
        checkSelectionsAreValid();
        
        int ammount_solutions = 0;
        
        this.panelRows = new HashMap<>();
        
        ArrayList<JPanel> rows = new ArrayList<>();
                
        
        Set<Integer> keys = clickedSolutions.keySet();
        
        
        int i = 0;
        for (Integer key : keys) {            
            SelectionEntry selectionEntry = clickedSolutions.get(key);
            
            if (PlotFrame.solutionDraw.isDrawOnlyRepresentatives() && selectionEntry.getSelectionType() == SelectionEntry.SelectionType.SOLUTION) continue;
            if (!PlotFrame.solutionDraw.isDrawOnlyRepresentatives() && selectionEntry.getSelectionType() != SelectionEntry.SelectionType.SOLUTION) continue;
            
            ammount_solutions += selectionEntry.getSize();
            
            JPanel row = createRow(i++ % 2 == 0, selectionEntry);
            rows.add(row);
            panelRows.put(key, row);
        }
        
        this.jpnList.removeAll();
        this.jpnList.revalidate();
        this.jpnList.repaint();
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.jpnList);
        this.jpnList.setLayout(layout);                       
        
        
        GroupLayout.ParallelGroup pG = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
        for (JPanel row : rows) {
            pG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
        }                            
                
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pG)
                .addContainerGap())
        );
        
        GroupLayout.SequentialGroup sG = layout.createSequentialGroup().addContainerGap();
        
        for (JPanel row : rows) {            
            sG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25);
        }
        
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sG)
        );
        
        
        this.jlblSelectionsSize.setText("" + ammount_solutions);
    }
    
    
    
    
    
    private JPanel createRow(boolean color, SelectionEntry selectionEntry) {
        
        JPanel jpnRow = new javax.swing.JPanel();
        JPanel jpnColor = new javax.swing.JPanel();
        JTextField jtfReal = new JTextField();
        JTextField jtfSolution = new JTextField();
        JTextField jtfShape = new JTextField();
        JLabel jlblRemove = new javax.swing.JLabel();       
        
        jpnColor.setBackground(selectionEntry.getColor());
        jpnColor.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jpnColor.setName("color_" + selectionEntry.getSolution().getId());
        
        jpnColor.addMouseListener(new CustomMouseListener()
                .setMouseClicked((e) -> { changeColorEvent(e); }));
                
        if (selectionEntry.getSelectionType() == SelectionEntry.SelectionType.SOLUTION) {
            jtfSolution.setText(selectionEntry.getSolution().prettyString(false));            
            jtfSolution.setToolTipText(selectionEntry.getSolution().prettyString(false));
            jtfReal.setVisible(true);
        } else {
            int ammount = selectionEntry.getSize();
            jtfSolution.setText(ammount+ " solutions");
            jtfSolution.setToolTipText(ammount+ " solutions");
            jtfReal.setVisible(false);
        }            
        
        jtfSolution.setEditable(false);
        jtfSolution.setBorder(BorderFactory.createEmptyBorder());
        jtfSolution.setOpaque(false);
        jtfSolution.setName("sol_" + selectionEntry.getSolution().getId());
        jtfSolution.addMouseListener(hoverListener);
        
        jtfReal.setText(selectionEntry.getSolution().realValuesString());
        jtfReal.setToolTipText(selectionEntry.getSolution().realValuesString());
        jtfReal.setEditable(false);
        jtfReal.setBorder(BorderFactory.createEmptyBorder());
        jtfReal.setOpaque(false);
        jtfReal.setName("sol_" + selectionEntry.getSolution().getId());
        jtfReal.addMouseListener(hoverListener);
        
        
        jtfShape.setText(selectionEntry.getSolution().shapeString());
        jtfShape.setToolTipText(selectionEntry.getSolution().shapeString());
        jtfShape.setEditable(false);
        jtfShape.setBorder(BorderFactory.createEmptyBorder());
        jtfShape.setOpaque(false);
        jtfShape.setName("shape_" + selectionEntry.getSolution().getId());
        jtfShape.addMouseListener(hoverListener);
        
        jlblRemove.setPreferredSize(new Dimension(14, 14));
        jlblRemove.setIcon(removeIcon);        
        jlblRemove.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jlblRemove.setName("rem_" + selectionEntry.getSolution().getId());
        jlblRemove.addMouseListener(new CustomMouseListener()
                .setMouseClicked((e) -> {removeSelectionEvent(e);}));
        
        
        javax.swing.GroupLayout jpnRowLayout = new javax.swing.GroupLayout(jpnRow);
        jpnRow.setLayout(jpnRowLayout);
        jpnRowLayout.setHorizontalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnRowLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jlblRemove, 20, 20, 20)
                .addGap(20, 20, 20)
                .addComponent(jpnColor, 60, 60, 60)
                .addGap(15, 15, 15)
                .addComponent(jtfShape, 150, 150, 150)
                .addGap(15, 15, 15)
                .addComponent(jtfSolution, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(jtfReal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jpnRowLayout.setVerticalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpnColor, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 19, 19)
            .addComponent(jtfSolution, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            .addComponent(jtfReal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            .addComponent(jtfShape, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            .addComponent(jlblRemove, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            
                
                
        );

        
        jpnRow.setName("pnsol_" + selectionEntry.getSolution().getId());        
        jpnRow.addMouseListener(hoverListener);
        if (color) jpnRow.setBackground(new java.awt.Color(230, 230, 230));        
        
        
        
        return jpnRow;
    }
    
    
    
    
    private void changeColorEvent(MouseEvent e) {
        JPanel panel = (JPanel)e.getComponent();
        int id = Integer.parseInt(e.getComponent().getName().split("_")[1]);

        SelectionEntry selectionEntry = clickedSolutions.getOrDefault(id, null);
        if (selectionEntry != null) {

            JColorChooser tcc = new JColorChooser(selectionEntry.getColor());
            tcc.getSelectionModel().addChangeListener((ChangeEvent e1) -> {
                selectionEntry.setColor(tcc.getColor());
                panel.setBackground(selectionEntry.getColor());                                                                                          
                PlotFrame.redraw();
                updateList();
            });

            JFrame jf = new JFrame();                   
            jf.add(tcc);
            tcc.setPreferredSize(new Dimension(442, 384));
            jf.setPreferredSize(new Dimension(442, 384));
            jf.setSize(new Dimension(442, 384));
            jf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            jf.setVisible(true);
        }
    }
    
    private void removeSelectionEvent(MouseEvent e) {
        int id = Integer.parseInt(e.getComponent().getName().split("_")[1]);        
                
        SelectionEntry selectionEntry = clickedSolutions.getOrDefault(id, null);
        if (selectionEntry != null) {

            PlotFrame.solutionDraw.toogleSelection(selectionEntry);                                        
            PlotFrame.solutionDraw.setHoveredSolutions(new ArrayList<>());
            PlotFrame.redraw();
            setClickedSolutions(PlotFrame.solutionDraw.getClickedSolutions());
        }
        

    }
    


    private void initializeAddToGroupDialog() {
        
        existingGroups = new ArrayList<>();
        
        this.jtfGroupName.setText("");
        this.jcbGroupName.removeAllItems();
        
        if (PlotFrame.groups.size() > 0) {           
            
            for(Group group : PlotFrame.groups.values()) {
                existingGroups.add(group);
                this.jcbGroupName.addItem(group.getGroupName());
            }
                       
            
            this.jcbGroupName.setSelectedIndex(0);
            this.jcbGroupName.setEnabled(true);            
            this.jrbAddToExistingGroup.setEnabled(true);            
            this.jbtnAdd.setEnabled(true);
            this.jrbAddToExistingGroup.setSelected(true);              
        } else {
            this.jcbGroupName.setEnabled(false);
            this.jrbAddToExistingGroup.setEnabled(false);
            this.jrbCreateNew.setSelected(true);
            this.jtfGroupName.setEnabled(true);
            this.jtfGroupName.requestFocus();
            this.jbtnAdd.setEnabled(false);
        }
        
        
    }

    
    private void changeAddToGroupSelection() {
        
        if (this.jrbAddToExistingGroup.isSelected()) {
            this.jcbGroupName.setEnabled(true);
            this.jtfGroupName.setEnabled(false);
            this.jbtnAdd.setEnabled(true);            
        } else {
           this.jtfGroupName.setEnabled(true);
           this.jcbGroupName.setEnabled(false);
           this.jtfGroupName.requestFocus();
        }
    }
    
    private void addToExistingGroup() {
        
        Set<Integer> solution_ids = new HashSet<>();            
        for(SelectionEntry selectionEntry : clickedSolutions.values()) {
            if (selectionEntry.getSelectionType() == SelectionEntry.SelectionType.SOLUTION) {
                solution_ids.add(selectionEntry.getSolution().getId());
            } else {
                solution_ids.addAll(selectionEntry.getCluster().ids);
            }
        }
        
        
        if (this.jrbCreateNew.isSelected()) {
            Group newGroup = new Group();
            newGroup.setGroupName(this.jtfGroupName.getText());
            newGroup.getIds().addAll(solution_ids);
            PlotFrame.groups.put(newGroup.getGroupId(), newGroup);
            PlotFrame.groupListPanel.updateList();
            this.jDgAddToGroup.dispose();
        } else {
            Group group = existingGroups.get(this.jcbGroupName.getSelectedIndex());
            group.getIds().addAll(solution_ids);
            PlotFrame.groupListPanel.updateList();
            this.jDgAddToGroup.dispose();
        }
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDgAddToGroup = new javax.swing.JDialog();
        jrbAddToExistingGroup = new javax.swing.JRadioButton();
        jcbGroupName = new javax.swing.JComboBox<>();
        jrbCreateNew = new javax.swing.JRadioButton();
        jtfGroupName = new javax.swing.JTextField();
        jbtnAdd = new javax.swing.JButton();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jbtnExport = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jpnList = new javax.swing.JPanel();
        jlblDeleteAll = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jlblSelectionsSize = new javax.swing.JLabel();
        jlblNew = new javax.swing.JLabel();
        jlblAddToGroup = new javax.swing.JLabel();

        jDgAddToGroup.setMinimumSize(new java.awt.Dimension(278, 157));
        jDgAddToGroup.setModal(true);
        jDgAddToGroup.setResizable(false);

        jrbAddToExistingGroup.setSelected(true);
        jrbAddToExistingGroup.setText("Existing:");
        jrbAddToExistingGroup.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jrbAddToExistingGroup.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jrbAddToExistingGroupMouseClicked(evt);
            }
        });
        jrbAddToExistingGroup.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jrbAddToExistingGroupPropertyChange(evt);
            }
        });

        jcbGroupName.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jcbGroupName.setPreferredSize(new java.awt.Dimension(140, 20));

        jrbCreateNew.setText("Create new:");
        jrbCreateNew.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jrbCreateNew.setPreferredSize(new java.awt.Dimension(97, 23));
        jrbCreateNew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jrbCreateNewMouseClicked(evt);
            }
        });
        jrbCreateNew.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jrbCreateNewPropertyChange(evt);
            }
        });

        jtfGroupName.setText("jTextField1");
        jtfGroupName.setPreferredSize(new java.awt.Dimension(140, 20));
        jtfGroupName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtfGroupNameKeyReleased(evt);
            }
        });

        jbtnAdd.setText("Add");
        jbtnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnAddActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDgAddToGroupLayout = new javax.swing.GroupLayout(jDgAddToGroup.getContentPane());
        jDgAddToGroup.getContentPane().setLayout(jDgAddToGroupLayout);
        jDgAddToGroupLayout.setHorizontalGroup(
            jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgAddToGroupLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jDgAddToGroupLayout.createSequentialGroup()
                        .addComponent(jbtnAdd)
                        .addGap(12, 12, 12))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDgAddToGroupLayout.createSequentialGroup()
                        .addGroup(jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDgAddToGroupLayout.createSequentialGroup()
                                .addComponent(jrbAddToExistingGroup, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jcbGroupName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDgAddToGroupLayout.createSequentialGroup()
                                .addComponent(jrbCreateNew, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jtfGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        jDgAddToGroupLayout.setVerticalGroup(
            jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgAddToGroupLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jrbAddToExistingGroup)
                    .addComponent(jcbGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jrbCreateNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jbtnAdd)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        buttonGroup1.add(this.jrbAddToExistingGroup);
        buttonGroup1.add(this.jrbCreateNew);

        setPreferredSize(new java.awt.Dimension(840, 194));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Color");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Normalized");

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Shape");

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Real Values");

        jbtnExport.setText("Export");
        jbtnExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnExportActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtnExport)
                .addGap(21, 21, 21))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtnExport))
        );

        javax.swing.GroupLayout jpnListLayout = new javax.swing.GroupLayout(jpnList);
        jpnList.setLayout(jpnListLayout);
        jpnListLayout.setHorizontalGroup(
            jpnListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 866, Short.MAX_VALUE)
        );
        jpnListLayout.setVerticalGroup(
            jpnListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 109, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(jpnList);

        jlblDeleteAll.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jlblDeleteAll.setEnabled(false);
        jlblDeleteAll.setPreferredSize(new java.awt.Dimension(16, 16));
        jlblDeleteAll.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlblDeleteAllMouseClicked(evt);
            }
        });

        jLabel1.setText("Selections:");

        jlblSelectionsSize.setText("0");

        jlblNew.setPreferredSize(new java.awt.Dimension(14, 14));

        jlblAddToGroup.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jlblAddToGroup.setEnabled(false);
        jlblAddToGroup.setPreferredSize(new java.awt.Dimension(16, 16));
        jlblAddToGroup.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlblAddToGroupMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jlblDeleteAll, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlblAddToGroup, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlblSelectionsSize, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 816, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jlblNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jlblSelectionsSize, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jlblDeleteAll, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlblAddToGroup, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jlblNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jlblDeleteAllMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlblDeleteAllMouseClicked
        
        ArrayList<SelectionEntry> toRemove = new ArrayList<>();
        for(Integer key : clickedSolutions.keySet()) {                        
            SelectionEntry selectionEntry = clickedSolutions.get(key);
            if (selectionEntry.getSelectionType() == SelectionEntry.SelectionType.SOLUTION && PlotFrame.solutionDraw.isDrawOnlyRepresentatives()) continue;
            if (selectionEntry.getSelectionType() != SelectionEntry.SelectionType.SOLUTION && !PlotFrame.solutionDraw.isDrawOnlyRepresentatives()) continue;
            toRemove.add(selectionEntry);
        }
        
        for(SelectionEntry selectionEntry : toRemove) {
            PlotFrame.solutionDraw.toogleSelection(selectionEntry);
        }
        
        PlotFrame.redraw();
        setClickedSolutions(PlotFrame.solutionDraw.getClickedSolutions());
        
        checkAllButtons();
    }//GEN-LAST:event_jlblDeleteAllMouseClicked

    
    
    private void jlblAddToGroupMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlblAddToGroupMouseClicked
        if (!this.jlblAddToGroup.isEnabled()) return;
        initializeAddToGroupDialog();
        this.jDgAddToGroup.setVisible(true);
    }//GEN-LAST:event_jlblAddToGroupMouseClicked

    private void jtfGroupNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtfGroupNameKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (this.jbtnAdd.isEnabled()) addToExistingGroup();
        }
    }//GEN-LAST:event_jtfGroupNameKeyReleased

    private void jrbAddToExistingGroupPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jrbAddToExistingGroupPropertyChange
        changeAddToGroupSelection();
    }//GEN-LAST:event_jrbAddToExistingGroupPropertyChange

    private void jrbCreateNewPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jrbCreateNewPropertyChange
        changeAddToGroupSelection();
    }//GEN-LAST:event_jrbCreateNewPropertyChange

    private void jrbCreateNewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jrbCreateNewMouseClicked
        changeAddToGroupSelection();
    }//GEN-LAST:event_jrbCreateNewMouseClicked

    private void jrbAddToExistingGroupMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jrbAddToExistingGroupMouseClicked
        changeAddToGroupSelection();
    }//GEN-LAST:event_jrbAddToExistingGroupMouseClicked

    private void jbtnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnAddActionPerformed
        addToExistingGroup();
    }//GEN-LAST:event_jbtnAddActionPerformed

    private void jbtnExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnExportActionPerformed
         
        List<Integer> solutionIds = new ArrayList<>();
        Set<Integer> keys = clickedSolutions.keySet();        
        
        int i = 0;
        for (Integer key : keys) {            
            SelectionEntry selectionEntry = clickedSolutions.get(key);
            if (selectionEntry.getSelectionType() == SelectionEntry.SelectionType.SOLUTION) {
                solutionIds.add(selectionEntry.getSolution().getId());
            }
        }    
        
        if (solutionIds.isEmpty()) { return; }
        
            SelectFolderFileUtil.select(
                    EnumSet.of(SelectFolderFileUtil.SelectionOption.SELECT_FILE), 
                    new SelectFolderFileUtil.SelectAfter() {
                @Override
                public void excecute(String fullPath) {
                    
                    if (fullPath != null) {
                                                
                        ExportSolutionsToCsvUtil1.export(solutionIds, fullPath);
                        
                        JOptionPane.showMessageDialog(PlotFrame.plotFrame,
                        "Successfully exported to csv format");    
                        
                    }
                    
                    
                }
            }, "export.csv", null);
            
            
        
        
        
    }//GEN-LAST:event_jbtnExportActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JDialog jDgAddToGroup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton jbtnAdd;
    private javax.swing.JButton jbtnExport;
    private javax.swing.JComboBox<String> jcbGroupName;
    private javax.swing.JLabel jlblAddToGroup;
    private javax.swing.JLabel jlblDeleteAll;
    private javax.swing.JLabel jlblNew;
    private javax.swing.JLabel jlblSelectionsSize;
    private javax.swing.JPanel jpnList;
    private javax.swing.JRadioButton jrbAddToExistingGroup;
    private javax.swing.JRadioButton jrbCreateNew;
    private javax.swing.JTextField jtfGroupName;
    // End of variables declaration//GEN-END:variables
}
