/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import py.una.pol.setviz.gui.draw.SolutionDraw;
import py.una.pol.setviz.gui.draw.SolutionDrawManager;
import py.una.pol.setviz.gui.draw.SolutionDrawParallelCoordinates;
import py.una.pol.setviz.gui.draw.SolutionDrawRadViz;

/**
 *
 * @author acost
 */
public class PlotPanel extends JPanel {

    private BufferedImage img;
    private Graphics2D g2;
    private final SolutionDrawManager solutionDrawManager;       
    private final SolutionDraw solutionDraw;
    
    double zoom_scaleX;
    double zoom_scaleY;

    int zoom_moveX;
    int zoom_moveY;

    int zoom_width;
    int zoom_height;

    boolean zoom;

   public PlotPanel(SolutionDrawManager solutionDrawManager, SolutionDrawMode solutionDrawMode) {
       img = new BufferedImage(PlotFrame.PANEL_WIDTH, PlotFrame.PANEL_HEIGHT, BufferedImage.TYPE_INT_RGB);
       g2 = (Graphics2D) img.getGraphics();       
       this.solutionDrawManager = solutionDrawManager;       
       this.zoom = false;
       
       switch(solutionDrawMode) {
           case RADVIZ:
                solutionDraw = new SolutionDrawRadViz(solutionDrawManager);
                break;
            default:
                solutionDraw = new SolutionDrawParallelCoordinates(solutionDrawManager);               
       }
       
       
   }
    
   public void redraw() {
       Dimension actualSize = this.getSize();
       
       img = new BufferedImage(new Double(actualSize.getWidth()).intValue(), new Double(actualSize.getHeight()).intValue(), BufferedImage.TYPE_INT_RGB);
       g2 = (Graphics2D) img.getGraphics();
       draw();
             
   }

    public BufferedImage getImg() {
        return img;
    }
   
   
   
   public void draw() {
       Dimension actualSize = this.getSize();
       
       g2.setColor(Color.WHITE);
       g2.fillRect(0, 0, actualSize.width, actualSize.height);              
              
       solutionDraw.updateSize(actualSize);       
       solutionDraw.drawSolutions(g2);
       
       revalidate();
       repaint();
   }
   
   public void update(MouseEvent e) {
          
       double x = e.getX() / zoom_scaleX + zoom_moveX;
       double y = e.getY() / zoom_scaleY + zoom_moveY;
           
       solutionDraw.update(x, y, zoom);
       PlotFrame.redraw();
       
   }

   public void updateClick(MouseEvent e) {
          
       double x = e.getX() / zoom_scaleX + zoom_moveX;
       double y = e.getY() / zoom_scaleY + zoom_moveY;
           
       solutionDraw.updateClick(x, y, zoom);
       PlotFrame.redraw();
       
   }
   

    public SolutionDrawManager getSolutionDraw() {
        return solutionDrawManager;
    }
   

   public void refreshZoomValues() {
       
       if (!zoom) {
           zoom_width = img.getWidth();
           zoom_height = img.getHeight();
       }
                     
       if (zoom_moveX < 0) zoom_moveX = 0;
       if (zoom_moveY < 0) zoom_moveY = 0;                
       if (zoom_moveX > img.getWidth() - zoom_width) zoom_moveX = img.getWidth() - zoom_width;
       if (zoom_moveY > img.getHeight() - zoom_height) zoom_moveY = img.getHeight()- zoom_height;
       
       zoom_scaleX = (1.0 * this.getWidth()) / zoom_width;
       zoom_scaleY = (1.0 * this.getHeight()) / zoom_height;
       
       
   }
    
   
   @Override
   public void paintComponent(Graphics g) { 
       
       refreshZoomValues();
      
       BufferedImage newImg = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_ARGB);
       Graphics2D g2 = (Graphics2D)newImg.getGraphics();
       
       AffineTransform afft = new AffineTransform();
       afft.scale(zoom_scaleX, zoom_scaleY);
       afft.translate(-1.0 * zoom_moveX, -1.0 * zoom_moveY);       
       g2.setTransform(afft);
       
       g2.drawImage(img, 0, 0, null);              
       g2.dispose();
       
       g.drawImage(newImg, 0, 0, null);
       
      
   }
   
       
    public enum SolutionDrawMode {
        PARALLEL,
        RADVIZ
    }
    
   
   
}
