/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.tmp;

import javax.swing.JLabel;
import net.infonode.docking.View;
import py.una.pol.setviz.gui.language.LanguageKeys;
import py.una.pol.setviz.gui.language.LanguageManager;

/**
 *
 * @author acost
 */
public class ProjectExplorerView {
    
        
    public static View createView(LanguageManager languageManager) {
        View projectView = new View(languageManager.getMessage(LanguageKeys.PROJECT_VIEW_TITLE), null, new JLabel("Aun no hay nada aqui :("));
        return projectView;
    }
    
}
