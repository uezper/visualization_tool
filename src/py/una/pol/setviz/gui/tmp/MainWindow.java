/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.tmp;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Locale;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import net.infonode.docking.DockingWindow;
import net.infonode.docking.RootWindow;
import net.infonode.docking.SplitWindow;
import net.infonode.docking.TabWindow;
import net.infonode.docking.View;
import net.infonode.docking.theme.DockingWindowsTheme;
import net.infonode.docking.theme.GradientDockingTheme;
import net.infonode.docking.util.DeveloperUtil;
import net.infonode.docking.util.DockingUtil;
import net.infonode.docking.util.ViewMap;
import static py.una.pol.setviz.gui.PlotFrame.PANEL_HEIGHT;
import static py.una.pol.setviz.gui.PlotFrame.PANEL_WIDTH;
import py.una.pol.setviz.gui.PlotPanel;
import py.una.pol.setviz.gui.clusters.ClusterizePanel;
import py.una.pol.setviz.gui.draw.SolutionDrawManager;
import py.una.pol.setviz.gui.filters.FilterListPanel;
import py.una.pol.setviz.gui.groups.GroupListPanel;
import py.una.pol.setviz.gui.language.LanguageKeys;
import py.una.pol.setviz.gui.language.LanguageManager;
import py.una.pol.setviz.gui.preferences.PreferencesPanel;
import py.una.pol.setviz.gui.ranking.RankingPanel;
import py.una.pol.setviz.gui.selections.SelectionsPanel;
import py.una.pol.setviz.Settings;
import py.una.pol.setviz.utils.solution.SolutionSet;

/**
 *
 * @author Uriel Pereira
 */
public class MainWindow extends JFrame {
    
    // VIEWS IDS
    private static final int PROJECT_VIEW_ID = 0;
    private static final int FILTER_VIEW_ID = 1;
    private static final int GENERAL_VIEW_ID = 2;
    private static final int GROUP_VIEW_ID = 3;
    private static final int CLUSTER_VIEW_ID = 4;
    private static final int SELECTION_VIEW_ID = 5;       
    private static final int PLOT_PAR_VIEW_ID = 6;       
    private static final int PLOT_RAD_VIEW_ID = 7;       
    private static final int[] VIEWS_IDS = {
        PROJECT_VIEW_ID, 
        FILTER_VIEW_ID, 
        GENERAL_VIEW_ID,
        GROUP_VIEW_ID, 
        CLUSTER_VIEW_ID, 
        SELECTION_VIEW_ID,
        PLOT_PAR_VIEW_ID,
        PLOT_RAD_VIEW_ID};
    
    
    // PANELS    
    public static SelectionsPanel selectionsPanel;
    public static SolutionDrawManager solutionDraw;
    public static GroupListPanel groupListPanel;
    public static PreferencesPanel preferencesPanel;
    public static FilterListPanel filterListPanel;
    public static ClusterizePanel clusterizePanel;
    public static RankingPanel rankingPanel;
    public static PlotPanel plotParFrame;
    public static PlotPanel plotRadFrame;    
        
    // STATIC VARIABLES
    public static boolean isMinProblem;    
    public static Settings settings;
    
    public static Dimension screenSize;
    public static Dimension windowSize;
    public static LanguageManager languageManager;
    
    RootWindow rootWindow;
    
    public MainWindow(SolutionSet solutionSet, boolean isMinProblemParam) {
        
        languageManager = new LanguageManager(new Locale("es"));             
        
        // INIT PANELS
        solutionDraw = new SolutionDrawManager(solutionSet);        
        isMinProblem = isMinProblemParam;
        
//        plotRadFrame = new PlotPanel(solutionDraw);
        plotRadFrame.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
        plotRadFrame.setSize(PANEL_WIDTH, PANEL_HEIGHT);        
        
        
        settings = new Settings(solutionSet);                        
        solutionSet.recalculateShapes(isMinProblem);        
        
        selectionsPanel = new SelectionsPanel();                        
        groupListPanel = new GroupListPanel();                
        preferencesPanel = new PreferencesPanel();
        filterListPanel = new FilterListPanel();               
        clusterizePanel = new ClusterizePanel();
        rankingPanel = new RankingPanel();
        
        filterListPanel.applyFilters();                
        
        initViews();                
    }        
    
    
    
    private void initViews() {
     
        // GET AND DEFINE SCREEN AND WINDOW SIZES        
        screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        windowSize = new Dimension(new Double(screenSize.getWidth() - 50F).intValue(), 
                new Double(screenSize.getHeight() - 50F).intValue());
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
        setPreferredSize(windowSize);
        setTitle(languageManager.getMessage(LanguageKeys.MAIN_WINDOW_TITLE));
        
        // MENU        
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu(languageManager.getMessage(LanguageKeys.MAIN_WINDOW_MENUBAR_FILE_DESC));
        menu.getAccessibleContext().setAccessibleDescription("Do nothing yet!");
        menuBar.add(menu);
        setJMenuBar(menuBar);
      
        // OTHER STUFF        
        ViewMap viewMap = new ViewMap();                          
        
        View[] views = new View[VIEWS_IDS.length];
        
        for (int i = 0; i < views.length; i++) {
            views[VIEWS_IDS[i]] = getView(VIEWS_IDS[i]);
            viewMap.addView(VIEWS_IDS[i], views[VIEWS_IDS[i]]);
        }
        
        rootWindow = DockingUtil.createRootWindow(viewMap, true);        

        rootWindow.setWindow(new SplitWindow(true, 0.22f,
                  new SplitWindow(false, 0.3f,
                                  views[GENERAL_VIEW_ID],
                                  new SplitWindow(false, 0.5f,
                                                  views[SELECTION_VIEW_ID],
                                                  new TabWindow(new DockingWindow[]{views[CLUSTER_VIEW_ID], views[GROUP_VIEW_ID]}))),                                  
                  views[PLOT_RAD_VIEW_ID]));
        
        
        DockingWindowsTheme theme = new GradientDockingTheme();
        rootWindow.getRootWindowProperties().addSuperObject(theme.getRootWindowProperties());
        
        
        add(rootWindow);
        
        
        
        pack();       
    }   
    
    
    private View getView(int ID) {
        switch (ID) {
            case GENERAL_VIEW_ID:
                return new View("No title", null, new JLabel("General Panel"));
            case FILTER_VIEW_ID:
                return new View("No title", null, filterListPanel);
            case GROUP_VIEW_ID:
                return new View("No title", null, groupListPanel);
            case SELECTION_VIEW_ID:
                return new View("No title", null, selectionsPanel);
            case CLUSTER_VIEW_ID:            
                return new View("No title", null, clusterizePanel);
            case PLOT_PAR_VIEW_ID:
            case PLOT_RAD_VIEW_ID:
                return new View("No title", null, plotRadFrame);
        }              
        return null;
    }
    
    private View getNewView() {
        return new View("lalala", null, new JLabel("This is a new view"));
    }        
    
    private void getDevelLayaout() {
        DeveloperUtil.createWindowLayoutFrame("My Main RootWindow", rootWindow).setVisible(true);
    }         
    
}
