/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz.gui.clusters;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import py.una.pol.setviz.gui.PlotFrame;
import py.una.pol.setviz.gui.draw.ColorPalette;
import py.una.pol.setviz.gui.utils.SelectFolderFileUtil;
import py.una.pol.setviz.utils.CustomMouseListener;
import py.una.pol.setviz.utils.DrawEntry;
import py.una.pol.setviz.utils.ExportShapeToCsvUtil;
import py.una.pol.setviz.utils.ImageUtils;
import py.una.pol.setviz.utils.SelectionEntry;
import py.una.pol.setviz.utils.solution.Group;
import py.una.pol.setviz.utils.solution.cluster.GroupCluster;
import py.una.pol.setviz.utils.solution.cluster.KmeansCluster;
import py.una.pol.setviz.utils.solution.cluster.ShapeCluster;

/**
 *
 * @author acost
 */
public class ClusterListPanel extends javax.swing.JPanel {

    /**
     * Creates new form SelectionsPanel
     */   
    
    ClusterizePanel.ClusterType clusterType;
    
    ArrayList<ShapeCluster> shapeClusters;
    ArrayList<KmeansCluster> kmeansClusters;
    ArrayList<GroupCluster> groupsClusters;
    
    HashMap<Integer, JCheckBox> checkboxes;
    HashMap<Integer, JPanel> panels;
    HashMap<Integer, Color> colors;
    List<Integer> selected;           
    
    
    private int selectedIndex;
    Color lastColor;
    MouseListener hoverListener;
       
    ImageIcon addToGroupIcon;
    
    boolean updateCheckbox;
    ArrayList<Group> existingGroups;
    
    public ClusterListPanel(ClusterizePanel.ClusterType clusterType) {
        initComponents();
        
        
        
        shapeClusters = new ArrayList<>();
        kmeansClusters = new ArrayList<>();
        groupsClusters = new ArrayList<>();
    
        
        addToGroupIcon = ImageUtils.getScaledImageIcon(
                ImageUtils.getImageIcon("add-to-group.png"), 
                18, 
                18);
        
        
        
        updateCheckbox = true;        
        selected = new ArrayList<>();
        this.clusterType = clusterType;
        
        if (this.clusterType == ClusterizePanel.ClusterType.SHAPE) {
            this.jlblClusterName.setText("Shape");
            this.jbtnExport.setVisible(false);
            this.jbtnExport.setVisible(true);
        } else if (this.clusterType == ClusterizePanel.ClusterType.KMEANS) {
            this.jlblClusterName.setText("Cluster");
            this.jbtnExport.setVisible(false);
        } else {
            this.jlblClusterName.setText("Group");
            this.jbtnExport.setVisible(false);
        }
        
        JScrollBar vertical = this.jScrollPane1.getVerticalScrollBar();
	InputMap verticalMap = vertical.getInputMap( JComponent.WHEN_IN_FOCUSED_WINDOW );
	verticalMap.put( KeyStroke.getKeyStroke( "DOWN" ), "positive_" );        
	verticalMap.put( KeyStroke.getKeyStroke( "UP" ), "negative_" );
        
        
        Action actionListener1 = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                
                JScrollBar jScrollBar = (JScrollBar) actionEvent.getSource();               
                jScrollBar.setValue(jScrollBar.getValue() + 20);
                
            }
        };
        
        Action actionListener2 = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                
                JScrollBar jScrollBar = (JScrollBar) actionEvent.getSource();               
                jScrollBar.setValue(jScrollBar.getValue() - 20);
                
            }
        };
        
        ActionMap actionMap = new ActionMap();
        actionMap.put("positive_", actionListener1);
        actionMap.put("negative_", actionListener2);
        this.jScrollPane1.getVerticalScrollBar().setActionMap(actionMap);
                
        
                this.jtfGroupName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                check();
            }
            
            private void check() {
                boolean valid = PlotFrame.groupListPanel.checkGroupName(jtfGroupName.getText());
                if (valid) {
                    jtfGroupName.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                } else {
                    jtfGroupName.setBorder(BorderFactory.createLineBorder(Color.RED));
                }                    
                jbtnAdd.setEnabled(valid);
            }
            
        });
                
                
        hoverListener = new CustomMouseListener()
                .setMouseEntered((e) -> {hoverEntered(e);})
                .setMouseExited((e) -> {hoverExited(e);});
        

        
        updateList();        
       
    }
    
    

    public ArrayList<KmeansCluster> getKmeansClusters() {
        return kmeansClusters;
    }

    public void setKmeansClusters(ArrayList<KmeansCluster> kmeansClusters) {
        this.kmeansClusters = kmeansClusters;
    }
    
    

    public ArrayList<ShapeCluster> getShapeClusters() {
        return shapeClusters;
    }

    public void setShapeClusters(ArrayList<ShapeCluster> clusters) {
        this.shapeClusters = clusters;
    }

    public ArrayList<GroupCluster> getGroupsClusters() {
        return groupsClusters;
    }

    public void setGroupsClusters(ArrayList<GroupCluster> groupsClusters) {
        this.groupsClusters = groupsClusters;
    }

    
    

    
    private void hoverEntered(MouseEvent e) {        
        int idx = Integer.parseInt(e.getComponent().getName().split("_")[1]);
        
        ArrayList<SelectionEntry> hovered = new ArrayList<>();
        
        if (clusterType == ClusterizePanel.ClusterType.SHAPE) {
            if (PlotFrame.solutionDraw.isDrawOnlyRepresentatives()) {
                SelectionEntry selectionEntry = new SelectionEntry();
                selectionEntry.setSelectionType(SelectionEntry.SelectionType.SET);
                selectionEntry.setSolution(shapeClusters.get(idx).representative);

                hovered.add(selectionEntry);
            } else {
                for(Integer solutionId : shapeClusters.get(idx).ids) {
                    SelectionEntry selectionEntry = new SelectionEntry();
                    selectionEntry.setSelectionType(SelectionEntry.SelectionType.SOLUTION);
                    selectionEntry.setSolution(PlotFrame.solutionDraw.getSolutionSet().getSolutionById(solutionId));

                    hovered.add(selectionEntry);
                }
            }
         
            
        } else if (clusterType == ClusterizePanel.ClusterType.KMEANS)  {
            if (PlotFrame.solutionDraw.isDrawOnlyRepresentatives()) {
                SelectionEntry selectionEntry = new SelectionEntry();
                selectionEntry.setSelectionType(SelectionEntry.SelectionType.SOLUTION);
                selectionEntry.setSolution(kmeansClusters.get(idx).getRepresentative());

                hovered.add(selectionEntry);
            } else {
                for(Integer solutionId : kmeansClusters.get(idx).getSolutionIds()) {
                    SelectionEntry selectionEntry = new SelectionEntry();
                    selectionEntry.setSelectionType(SelectionEntry.SelectionType.SOLUTION);
                    selectionEntry.setSolution(PlotFrame.solutionDraw.getSolutionSet().getSolutionById(solutionId));

                    hovered.add(selectionEntry);
                }
            }
            
        } else {
            for(Integer solutionId : groupsClusters.get(idx).ids) {
               SelectionEntry selectionEntry = new SelectionEntry();
               selectionEntry.setSelectionType(SelectionEntry.SelectionType.SOLUTION);
               selectionEntry.setSolution(PlotFrame.solutionDraw.getSolutionSet().getSolutionById(solutionId));

               hovered.add(selectionEntry);
            }
        }
        
        
        
        
        JPanel panel = panels.get(idx);        
        lastColor = panel.getBackground();
        panel.setBackground(new Color(170, 170, 170));
        PlotFrame.solutionDraw.getHoveredSolutions().addAll(hovered);
        PlotFrame.solutionDraw.setRankedSolutions(new ArrayList<>());
        PlotFrame.redraw();
    }
    
    private void hoverExited(MouseEvent e) {        
        int idx = Integer.parseInt(e.getComponent().getName().split("_")[1]);
        
        JPanel panel = panels.get(idx);
        panel.setBackground(lastColor);
        PlotFrame.solutionDraw.setHoveredSolutions(new ArrayList<>());
        PlotFrame.solutionDraw.setRankedSolutions(new ArrayList<>());
        PlotFrame.redraw();
        
    }
    
    
    
    public final void updateList() {
                            
        checkboxes = new HashMap<>();
        panels = new HashMap<>();
        colors = new HashMap<>();
        selected = new ArrayList<>();
        
        ArrayList<JPanel> rows = new ArrayList<>();
        
        ArrayList<Object> clusters;
        if (clusterType == ClusterizePanel.ClusterType.SHAPE) {
            clusters = new ArrayList<>(shapeClusters);
        } else if (clusterType == ClusterizePanel.ClusterType.KMEANS) {
            clusters = new ArrayList<>(kmeansClusters);            
        } else {
            clusters = new ArrayList<>(groupsClusters);            
        }
        
        int index = 0;
        for (Object cluster : clusters) {                        
            JPanel row = createRow(index % 2 == 0, index, cluster);            
            panels.put(index, row);
            index++;
            rows.add(row);
        }
        
        this.jpnList.removeAll();
        this.jpnList.revalidate();
        this.jpnList.repaint();
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this.jpnList);
        this.jpnList.setLayout(layout);                       
        
        
        GroupLayout.ParallelGroup pG = layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
        for (JPanel row : rows) {
            pG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
        }                            
                
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pG)
                .addContainerGap())
        );
        
        GroupLayout.SequentialGroup sG = layout.createSequentialGroup().addContainerGap();
        
        for (JPanel row : rows) {            
            sG.addComponent(row, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25);
        }
        
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sG)
        );
              
                
        this.jcbSelectAll.setSelected(true);
        selectDeselectAll();
    }
    
    
    
    
    
    private JPanel createRow(boolean color, int index, Object clusterObject) {
        
        boolean isShapeCluster = clusterObject instanceof ShapeCluster;
        boolean isKmeansCluster = clusterObject instanceof KmeansCluster;
        
        ShapeCluster shapeCluster = null;
        KmeansCluster kmeansCluster = null;
        GroupCluster groupCluster = null;
        
        if (isShapeCluster) {
            shapeCluster = (ShapeCluster) clusterObject;
        } else if (isKmeansCluster) {
            kmeansCluster = (KmeansCluster) clusterObject;            
        } else {
            groupCluster = (GroupCluster) clusterObject;
        }
        
        JPanel jpnRow = new javax.swing.JPanel();

        JCheckBox jckSelect = new JCheckBox();
        JTextField jtfCluster = new JTextField();
        JTextField jtfSolutions = new JTextField();
        JPanel jpnColor = new javax.swing.JPanel();
        JLabel jlblAddToGroup = new JLabel();
        
        
        if (isShapeCluster) {
            jpnColor.setBackground(ColorPalette.getShapeColor(shapeCluster.representative.shapeString()));
        } else {
            jpnColor.setBackground(ColorPalette.indexToColor(index));
        }
        
        jpnColor.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jpnColor.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jpnColor.setName("color_" + index);
        
        jpnColor.addMouseListener(new CustomMouseListener()
                .setMouseClicked((e) -> { changeColorEvent(e); }));
         
        
        
        jckSelect.setSelected(selected.contains(index));        
        jckSelect.setText("");       
        jckSelect.setName("ck_" + index);
        jckSelect.setPreferredSize(new Dimension(14, 16));
        jckSelect.setOpaque(false);
        jckSelect.addChangeListener((ChangeEvent e) -> {
            if (!updateCheckbox) return;
            
            JCheckBox jc = (JCheckBox)e.getSource();
            int id = Integer.parseInt(jc.getName().split("_")[1]);
            if (jc.isSelected()) {
                if (!selected.contains(id)) {
                    selected.add(id);                   
                }                
                panels.get(id).setBackground(new Color(210, 210, 210));
            } else {
                selected.remove(Integer.valueOf(id));
                panels.get(id).setBackground(colors.get(id));
            }
            updateSelected();
            
        });
        checkboxes.put(index, jckSelect);
        
        if (isShapeCluster) {
            jtfCluster.setText(shapeCluster.representative.shapeString());
            jtfCluster.setToolTipText(shapeCluster.representative.shapeString());
        } else if (isKmeansCluster) {
            jtfCluster.setText("Cluster " + (index + 1));
            jtfCluster.setToolTipText("Cluster " + (index + 1));
        } else {
            jtfCluster.setText(groupCluster.getName());
            jtfCluster.setToolTipText(groupCluster.getName());
        }
        jtfCluster.setName("cluster_" + index);
        jtfSolutions.setName("sol_" + index);
        
        
        
        jlblAddToGroup.setToolTipText("Add to group");
        jlblAddToGroup.setPreferredSize(new Dimension(14, 14));
        jlblAddToGroup.setIcon(addToGroupIcon);        
        jlblAddToGroup.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jlblAddToGroup.setName("atg_" + index);
        jlblAddToGroup.addMouseListener(new CustomMouseListener()
                .setMouseClicked((e) -> {
                    int idx = Integer.parseInt(e.getComponent().getName().split("_")[1]);                 
                    selectedIndex = idx;
                                        
                    initializeAddToGroupDialog();                    
                    
                    this.jDgAddToGroup.setVisible(true);
                }));
        
        
        
        
        
        jtfCluster.setEditable(false);
        jtfCluster.setBorder(BorderFactory.createEmptyBorder());
        jtfCluster.setOpaque(false);
        jtfCluster.addMouseListener(hoverListener);
        
        if (isShapeCluster) {
            jtfSolutions.setText(""+shapeCluster.ids.size());
            jtfSolutions.setToolTipText(""+shapeCluster.ids.size());
        } else if (isKmeansCluster) {            
            jtfSolutions.setText(""+kmeansCluster.getSize());
            jtfSolutions.setToolTipText(""+kmeansCluster.getSize());
        } else {
            jtfSolutions.setText(""+groupCluster.ids.size());
            jtfSolutions.setToolTipText(""+groupCluster.ids.size());
        }
        jtfSolutions.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jtfSolutions.setEditable(false);
        jtfSolutions.setBorder(BorderFactory.createEmptyBorder());
        jtfSolutions.setOpaque(false);
        jtfSolutions.setName("sol_" + index);
        jtfSolutions.addMouseListener(hoverListener);
        
        javax.swing.GroupLayout jpnRowLayout = new javax.swing.GroupLayout(jpnRow);
        jpnRow.setLayout(jpnRowLayout);
        jpnRowLayout.setHorizontalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnRowLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jckSelect, 18, 18, 18)                                
                .addGap(20, 20, 20)
                .addComponent(jpnColor, 19, 19, 19)
                .addGap(5, 5, 5)
                .addComponent(jlblAddToGroup, 18, 18, 18)    
                .addGap(20, 20, 20)
                .addComponent(jtfCluster, 65, 65, 177)
                .addGap(18, 18, 18)
                .addComponent(jtfSolutions, 65, 65, 65)                 
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)                                
                .addContainerGap())
        );
        jpnRowLayout.setVerticalGroup(
            jpnRowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpnColor, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 19, 19)                    
            .addComponent(jckSelect, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 19, 19)
            .addComponent(jtfCluster, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            .addComponent(jtfSolutions, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
            .addComponent(jlblAddToGroup, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, 25)
                            
        );
        
        jpnRow.setName("panel_" + index);
        jpnRow.addMouseListener(hoverListener);
        
        if (color) jpnRow.setBackground(new java.awt.Color(230, 230, 230));        
        colors.put(index, jpnRow.getBackground());
        
        return jpnRow;
    }
    
    
    
    private void changeColorEvent(MouseEvent e) {
        JPanel panel = (JPanel)e.getComponent();
        int id = Integer.parseInt(e.getComponent().getName().split("_")[1]);

        JColorChooser tcc;
        
        if (clusterType == ClusterizePanel.ClusterType.SHAPE) {
            ShapeCluster shapeCluster = shapeClusters.get(id);
        
            tcc = new JColorChooser(ColorPalette.getShapeColor(shapeCluster.representative.shapeString()));
            tcc.getSelectionModel().addChangeListener((ChangeEvent e1) -> {
                ColorPalette.setShapeColor(shapeCluster.representative.shapeString(), tcc.getColor());            
                panel.setBackground(tcc.getColor());                                                                                          
                updateSelected();
                PlotFrame.redraw();            
            });
        } else {
        
            tcc = new JColorChooser(ColorPalette.indexToColor(id));
            tcc.getSelectionModel().addChangeListener((ChangeEvent e1) -> {
                ColorPalette.setColor(id, tcc.getColor());            
                panel.setBackground(tcc.getColor());                                                                                          
                updateSelected();
                PlotFrame.redraw();            
            });
        }
        

        JFrame jf = new JFrame();                   
        jf.add(tcc);
        tcc.setPreferredSize(new Dimension(442, 384));
        jf.setPreferredSize(new Dimension(442, 384));
        jf.setSize(new Dimension(442, 384));
        jf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jf.setVisible(true);

    }
    
    
    
    
    
    
    private void updateSelected() {
        
        ArrayList<DrawEntry> drawEntries = new ArrayList<>();
            
        for(Integer id : selected) {
            
            
            if (clusterType == ClusterizePanel.ClusterType.SHAPE) {
                ShapeCluster cluster = shapeClusters.get(id);
                DrawEntry drawEntry = new DrawEntry();
                drawEntry.setColor(ColorPalette.getShapeColor(cluster.representative.shapeString()));
                drawEntry.setSolutionIds(new HashSet<Integer>(cluster.ids));    
                drawEntry.setRepresentative(cluster.representative);
                drawEntries.add(drawEntry);                
            } else if (clusterType == ClusterizePanel.ClusterType.KMEANS) {
                KmeansCluster cluster = kmeansClusters.get(id);
                DrawEntry drawEntry = new DrawEntry();
                drawEntry.setColor(ColorPalette.indexToColor(id));
                drawEntry.setSolutionIds(new HashSet<Integer>(cluster.getSolutionIds()));
                drawEntry.setRepresentative(cluster.getRepresentative());
                drawEntries.add(drawEntry);
            } else {
                GroupCluster cluster = groupsClusters.get(id);
                DrawEntry drawEntry = new DrawEntry();
                drawEntry.setColor(ColorPalette.indexToColor(id));
                drawEntry.setSolutionIds(new HashSet<Integer>(cluster.ids));                
                drawEntries.add(drawEntry);
                PlotFrame.solutionDraw.setDrawOnlyRepresentatives(false);
            }
            
        }
                
        PlotFrame.solutionDraw.setRankedSolutions(new ArrayList<>());
        PlotFrame.solutionDraw.setDrawingSet(drawEntries);
        PlotFrame.selectionsPanel.updateList();
        PlotFrame.redraw();
    }
    
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDgAddToGroup = new javax.swing.JDialog();
        jrbAddToExistingGroup = new javax.swing.JRadioButton();
        jcbGroupName = new javax.swing.JComboBox<>();
        jrbCreateNew = new javax.swing.JRadioButton();
        jtfGroupName = new javax.swing.JTextField();
        jbtnAdd = new javax.swing.JButton();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jlblClusterName = new javax.swing.JLabel();
        jlblSolutionsTitle = new javax.swing.JLabel();
        jcbSelectAll = new javax.swing.JCheckBox();
        jbtnExport = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jpnList = new javax.swing.JPanel();

        jDgAddToGroup.setMinimumSize(new java.awt.Dimension(278, 157));
        jDgAddToGroup.setModal(true);
        jDgAddToGroup.setResizable(false);

        jrbAddToExistingGroup.setSelected(true);
        jrbAddToExistingGroup.setText("Existing:");
        jrbAddToExistingGroup.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jrbAddToExistingGroup.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jrbAddToExistingGroupMouseClicked(evt);
            }
        });
        jrbAddToExistingGroup.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jrbAddToExistingGroupPropertyChange(evt);
            }
        });

        jcbGroupName.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jcbGroupName.setPreferredSize(new java.awt.Dimension(140, 20));

        jrbCreateNew.setText("Create new:");
        jrbCreateNew.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jrbCreateNew.setPreferredSize(new java.awt.Dimension(97, 23));
        jrbCreateNew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jrbCreateNewMouseClicked(evt);
            }
        });
        jrbCreateNew.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jrbCreateNewPropertyChange(evt);
            }
        });

        jtfGroupName.setText("jTextField1");
        jtfGroupName.setPreferredSize(new java.awt.Dimension(140, 20));
        jtfGroupName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtfGroupNameKeyReleased(evt);
            }
        });

        jbtnAdd.setText("Add");
        jbtnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnAddActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDgAddToGroupLayout = new javax.swing.GroupLayout(jDgAddToGroup.getContentPane());
        jDgAddToGroup.getContentPane().setLayout(jDgAddToGroupLayout);
        jDgAddToGroupLayout.setHorizontalGroup(
            jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgAddToGroupLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jDgAddToGroupLayout.createSequentialGroup()
                        .addComponent(jbtnAdd)
                        .addGap(12, 12, 12))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDgAddToGroupLayout.createSequentialGroup()
                        .addGroup(jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDgAddToGroupLayout.createSequentialGroup()
                                .addComponent(jrbAddToExistingGroup, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jcbGroupName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jDgAddToGroupLayout.createSequentialGroup()
                                .addComponent(jrbCreateNew, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jtfGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        jDgAddToGroupLayout.setVerticalGroup(
            jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDgAddToGroupLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jrbAddToExistingGroup)
                    .addComponent(jcbGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jDgAddToGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jrbCreateNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jbtnAdd)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        buttonGroup1.add(this.jrbAddToExistingGroup);
        buttonGroup1.add(this.jrbCreateNew);

        setMinimumSize(new java.awt.Dimension(600, 158));
        setPreferredSize(new java.awt.Dimension(861, 158));

        jlblClusterName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlblClusterName.setText("Shape");
        jlblClusterName.setPreferredSize(new java.awt.Dimension(177, 16));

        jlblSolutionsTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlblSolutionsTitle.setText("Solutions");
        jlblSolutionsTitle.setMaximumSize(new java.awt.Dimension(65, 16));
        jlblSolutionsTitle.setMinimumSize(new java.awt.Dimension(65, 16));
        jlblSolutionsTitle.setPreferredSize(new java.awt.Dimension(65, 16));

        jcbSelectAll.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcbSelectAllItemStateChanged(evt);
            }
        });

        jbtnExport.setText("Export");
        jbtnExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnExportActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jcbSelectAll)
                .addGap(82, 82, 82)
                .addComponent(jlblClusterName, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jlblSolutionsTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbtnExport)
                .addGap(22, 22, 22))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jlblClusterName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jcbSelectAll)
            .addComponent(jlblSolutionsTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jbtnExport)
        );

        javax.swing.GroupLayout jpnListLayout = new javax.swing.GroupLayout(jpnList);
        jpnList.setLayout(jpnListLayout);
        jpnListLayout.setHorizontalGroup(
            jpnListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 818, Short.MAX_VALUE)
        );
        jpnListLayout.setVerticalGroup(
            jpnListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 149, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(jpnList);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 837, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void selectDeselectAll() {    
        selected = new ArrayList<>();
        updateCheckbox = false;
        JCheckBox source = this.jcbSelectAll;           
        for(Integer id : checkboxes.keySet()) {
            JCheckBox ck = checkboxes.get(id);
            ck.setSelected(source.isSelected());            
            if (source.isSelected()) {
                selected.add(id);
                panels.get(id).setBackground(new Color(210, 210, 210));                
            }
            else {                
                selected.remove(Integer.valueOf(id));
                panels.get(id).setBackground(colors.get(id));
            }
        }
        
        updateCheckbox = true;        
        updateSelected();
    }


    private void changeAddToGroupSelection() {
        if (this.jrbAddToExistingGroup.isSelected()) {
            this.jcbGroupName.setEnabled(true);
            this.jtfGroupName.setEnabled(false);
            this.jbtnAdd.setEnabled(true);            
        } else {
           this.jtfGroupName.setEnabled(true);
           this.jcbGroupName.setEnabled(false);
           this.jtfGroupName.requestFocus();
        }
    }
    
    
    private void addToExistingGroup() {
        
        Set<Integer> solution_ids = new HashSet<>();            
        
        if (clusterType == ClusterizePanel.ClusterType.SHAPE) {            
            solution_ids.addAll(shapeClusters.get(selectedIndex).ids);            
        } else if (clusterType == ClusterizePanel.ClusterType.KMEANS) {
            solution_ids.addAll(kmeansClusters.get(selectedIndex).getSolutionIds());            
        } else {
            solution_ids.addAll(groupsClusters.get(selectedIndex).ids);            
        }
                
        
        if (this.jrbCreateNew.isSelected()) {
            Group newGroup = new Group();
            newGroup.setGroupName(this.jtfGroupName.getText());
            newGroup.getIds().addAll(solution_ids);
            PlotFrame.groups.put(newGroup.getGroupId(), newGroup);
            PlotFrame.groupListPanel.updateList();
            this.jDgAddToGroup.dispose();
        } else {
            Group group = existingGroups.get(this.jcbGroupName.getSelectedIndex());
            group.getIds().addAll(solution_ids);
            PlotFrame.groupListPanel.updateList();
            this.jDgAddToGroup.dispose();
        }
    }
    
    
    private void jcbSelectAllItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcbSelectAllItemStateChanged
        selectDeselectAll();
    }//GEN-LAST:event_jcbSelectAllItemStateChanged

    private void jrbAddToExistingGroupMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jrbAddToExistingGroupMouseClicked
        changeAddToGroupSelection();
    }//GEN-LAST:event_jrbAddToExistingGroupMouseClicked

    private void jrbAddToExistingGroupPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jrbAddToExistingGroupPropertyChange
        changeAddToGroupSelection();
    }//GEN-LAST:event_jrbAddToExistingGroupPropertyChange

    private void jrbCreateNewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jrbCreateNewMouseClicked
        changeAddToGroupSelection();
    }//GEN-LAST:event_jrbCreateNewMouseClicked

    private void jrbCreateNewPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jrbCreateNewPropertyChange
        changeAddToGroupSelection();
    }//GEN-LAST:event_jrbCreateNewPropertyChange

    private void jtfGroupNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtfGroupNameKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (this.jbtnAdd.isEnabled()) addToExistingGroup();
        }
    }//GEN-LAST:event_jtfGroupNameKeyReleased

    private void jbtnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnAddActionPerformed
        addToExistingGroup();
    }//GEN-LAST:event_jbtnAddActionPerformed

    private void jbtnExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnExportActionPerformed
        
        if (clusterType == ClusterizePanel.ClusterType.SHAPE) {            
        
            List<ShapeCluster> clusters = new ArrayList<>();
            for (Integer sIndex : selected) {
                clusters.add(shapeClusters.get(sIndex));
            }

            if (clusters.isEmpty()) {
                return;
            }
                                                
            
            
            SelectFolderFileUtil.select(
                    EnumSet.of(SelectFolderFileUtil.SelectionOption.SELECT_FILE), 
                    new SelectFolderFileUtil.SelectAfter() {
                @Override
                public void excecute(String fullPath) {
                    
                    if (fullPath != null) {
                                                
                        ExportShapeToCsvUtil.export(clusters, fullPath);
                        
                        JOptionPane.showMessageDialog(PlotFrame.plotFrame,
                        "Successfully exported to csv format");    
                        
                    }
                    
                    
                }
            }, "export.csv", null);
            
            
        } 
        
    }//GEN-LAST:event_jbtnExportActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JDialog jDgAddToGroup;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton jbtnAdd;
    private javax.swing.JButton jbtnExport;
    private javax.swing.JComboBox<String> jcbGroupName;
    private javax.swing.JCheckBox jcbSelectAll;
    private javax.swing.JLabel jlblClusterName;
    private javax.swing.JLabel jlblSolutionsTitle;
    private javax.swing.JPanel jpnList;
    private javax.swing.JRadioButton jrbAddToExistingGroup;
    private javax.swing.JRadioButton jrbCreateNew;
    private javax.swing.JTextField jtfGroupName;
    // End of variables declaration//GEN-END:variables

    private void initializeAddToGroupDialog() {
        
        existingGroups = new ArrayList<>();
        
        this.jtfGroupName.setText("");
        this.jcbGroupName.removeAllItems();
        
        if (PlotFrame.groups.size() > 0) {           
            
            for(Group group : PlotFrame.groups.values()) {
                existingGroups.add(group);
                this.jcbGroupName.addItem(group.getGroupName());
            }
                       
            
            this.jcbGroupName.setSelectedIndex(0);
            this.jcbGroupName.setEnabled(true);            
            this.jrbAddToExistingGroup.setEnabled(true);            
            this.jbtnAdd.setEnabled(true);
            this.jrbAddToExistingGroup.setSelected(true);              
        } else {
            this.jcbGroupName.setEnabled(false);
            this.jrbAddToExistingGroup.setEnabled(false);
            this.jrbCreateNew.setSelected(true);
            this.jtfGroupName.setEnabled(true);
            this.jtfGroupName.requestFocus();
            this.jbtnAdd.setEnabled(false);
        }
        if (clusterType == ClusterizePanel.ClusterType.SHAPE) {
            this.jtfGroupName.setText(shapeClusters.get(selectedIndex).representative.shapeString());
        } else {
            this.jtfGroupName.setText("Cluster " + (selectedIndex + 1));
        }
        
      
    }
}

