/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.setviz;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import py.una.pol.setviz.gui.preferences.PreferencesOrderGuiHelper;
import py.una.pol.setviz.utils.solution.SolutionSet;
import py.una.pol.setviz.utils.solution.preferences.SolutionPreference;

/**
 *
 * @author acost
 */
public class Settings {
    
    private Set<Integer> shownObjectives;  
    private ArrayList<ArrayList<Integer>> axesOrder;
    private boolean showOriginalSetInBackground;
    private ArrayList<String> objectiveNames;    
    private AxesOrderType currentAxesOrderType;
    private ArrayList<Integer> currentAxesOrder;
    private List<PreferencesOrderGuiHelper.Row> preferenceRows;
    private SolutionPreference solutionPreference;
    private String capturesLocation;
    
    private final SolutionSet solutionSet;
    
    public Settings(SolutionSet solutionSet) {
        
        this.shownObjectives = new HashSet<>();
        this.solutionSet = solutionSet;
        this.currentAxesOrderType = AxesOrderType.DEFAULT;
        this.showOriginalSetInBackground = false;
        this.axesOrder = new ArrayList<>(AxesOrderType.values().length);                
        this.objectiveNames = new ArrayList<>();
        objectiveNames.addAll(solutionSet.getNames());
        
        ArrayList<Integer> defaultAxesOrder = new ArrayList<>();
        for(int i = 0; i < solutionSet.getDimensions(); i++) {
            defaultAxesOrder.add(i);
            shownObjectives.add(i);
        }        
       
        
        this.axesOrder.add(AxesOrderType.DEFAULT.ordinal(), defaultAxesOrder);
        this.axesOrder.add(AxesOrderType.AUTOMATIC.ordinal(), solutionSet.getCorrelationAdjusting(shownObjectives));
        this.axesOrder.add(AxesOrderType.CUSTOM.ordinal(), defaultAxesOrder);
        this.currentAxesOrder = defaultAxesOrder;
        
        this.capturesLocation = System.getProperty("user.dir");
    }

    public String getCapturesLocation() {
        return capturesLocation;
    }

    public void setCapturesLocation(String capturesLocation) {
        this.capturesLocation = capturesLocation;
    }

    public ArrayList<String> getDefaultObjectiveNames() {
        return solutionSet.getNames();
    } 
    
    public Set<Integer> getShownObjectives() {
        Set<Integer> setCopy = new HashSet<>();
        setCopy.addAll(shownObjectives);
        
        return setCopy;
    }

    public void setShownObjectives(Set<Integer> shownObjectives) {
        this.shownObjectives = shownObjectives;              
        
        this.currentAxesOrder = new ArrayList<>();
        switch(this.currentAxesOrderType) {
            case DEFAULT:
                for (Integer i : this.axesOrder.get(AxesOrderType.DEFAULT.ordinal())) {
                    if (shownObjectives.contains(i)) this.currentAxesOrder.add(i);
                }
                break;
            case AUTOMATIC:
                this.currentAxesOrder = solutionSet.getCorrelationAdjusting(shownObjectives);
                break;
            case PREFERENCES:
                this.currentAxesOrder = new ArrayList<>(PreferencesOrderGuiHelper.getOrderFromPreferences(shownObjectives));
                break;
            case CUSTOM:
                for (Integer i : this.axesOrder.get(AxesOrderType.CUSTOM.ordinal())) {
                    if (shownObjectives.contains(i)) this.currentAxesOrder.add(i);
                }
                break;
        }
    }
    
    
    public ArrayList<Integer> getCustomAxesOrder() {
        return this.axesOrder.get(AxesOrderType.CUSTOM.ordinal());
    }

    public void setCustomAxesOrder(ArrayList<Integer> customOrder) {
        this.axesOrder.set(AxesOrderType.CUSTOM.ordinal(), customOrder);
    }
    
    public ArrayList<Integer> getCurrentAxesOrder() {
        return this.currentAxesOrder;
    }

    public boolean isShowOriginalSetInBackground() {
        return showOriginalSetInBackground;
    }

    public void setShowOriginalSetInBackground(boolean showOriginalSetInBackground) {
        this.showOriginalSetInBackground = showOriginalSetInBackground;
    }

    public ArrayList<String> getObjectiveNames() {
        return objectiveNames;
    }

    public void setObjectiveNames(ArrayList<String> objectiveNames) {
        this.objectiveNames = objectiveNames;
    }

    public AxesOrderType getCurrentAxesOrderType() {
        return currentAxesOrderType;
    }

    public void setCurrentAxesOrderType(AxesOrderType currentAxesOrderType) {
        this.currentAxesOrderType = currentAxesOrderType;
    }

    public List<PreferencesOrderGuiHelper.Row> getPreferenceRows() {
        return preferenceRows;
    }

    public void setPreferenceRows(List<PreferencesOrderGuiHelper.Row> preferenceRows) {
        this.preferenceRows = preferenceRows;
    }

    public SolutionPreference getSolutionPreference() {
        return solutionPreference;
    }

    public void setSolutionPreference(SolutionPreference solutionPreference) {
        this.solutionPreference = solutionPreference;
    }

    
    
    
    
    public enum AxesOrderType {
        DEFAULT,
        AUTOMATIC,
        CUSTOM,
        PREFERENCES
    }
    
}
